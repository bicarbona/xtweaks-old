/* global confirm, redux, redux_change */
/*global redux_change, redux*/

(function( $ ) {
    "use strict";

    redux.field_objects = redux.field_objects || {};
    redux.field_objects.gmaps = redux.field_objects.gmaps || {};

    $( document ).ready(
        function() {
            //redux.field_objects.gmaps.init();
        }
    );

    redux.field_objects.gmaps.init = function( selector ) {
    	console.log('init');

    	//House keeping
        if ( !selector ) {
            selector = $( document ).find( '.redux-container-gmaps' );
        }

        var parent = selector;

        if ( !selector.hasClass( 'redux-field-container' ) ) {
            parent = selector.parents( '.redux-field-container:first' );
        }

        if ( parent.hasClass( 'redux-field-init' ) ) {
            parent.removeClass( 'redux-field-init' );
        } else {
            return;
        }
        this.initValues();
        this.initGMaps();
    };

    redux.field_objects.gmaps.initValues = function(){   
    	console.log('initValues'); 	
    	//Extract redux gmaps object values
    	var jsonobj = $.parseJSON( $( "#loca-gmaps-object-json" ).html());
    	if(jsonobj['field']){
    		redux.field_objects.gmaps.field = jsonobj['field'];
    	} else {
    		redux.field_objects.gmaps.field = {};
    	}
    	if(jsonobj['value']){
    		redux.field_objects.gmaps.value = jsonobj['value'];
    	} else {
    		redux.field_objects.gmaps.value = {};
    	}
    }

    redux.field_objects.gmaps.initGMaps = function(){
    	console.log('initGMaps');

    	var fieldname = '';
    	if(this.field){
    		fieldname = this.field['name'] + this.field['name_suffix'];
    	}

		this.el			= $(".loca-item-map-container");
		this.st_el		= $(".loca-item-streetview-container");
		this.streetView = $("[name='"+fieldname+"[street_visible]']").is(":checked");

		this.st_el.height(350);

		// This Item get Location
		this.latLng = $("input[name='"+fieldname+"[lat]']").val() != "" && $("input[name='"+fieldname+"[lng]']").val() != "" ?
			new google.maps.LatLng($("input[name='"+fieldname+"[lat]']").val(), $("input[name='"+fieldname+"[lng]']").val()) :
			new google.maps.LatLng('27.9879017', '86.92531409999992');

		// Initialize Map Options
		this.map_options = {
			map:{ options:{ zoom:16, center: this.latLng } }
			, marker:{
				latLng		: this.latLng
				, options:{
					draggable	: true
				}
				, events:{
					position_changed: function( m )
					{
						$('input[name="'+fieldname+'[lat]"]').val( m.getPosition().lat() );
						$('input[name="'+fieldname+'[lng]"]').val( m.getPosition().lng() );
						$('input[name="'+fieldname+'[street_lat]"]').val( m.getPosition().lat() );
						$('input[name="'+fieldname+'[street_lng]"]').val( m.getPosition().lng() );

						if( $("[name='"+fieldname+"[street_visible]']").is(":checked") )
						{

							$(this).gmap3({
								get:{
									name:'streetviewpanorama'
									, callback: function( streetView )
									{
										if( typeof streetView != 'undefined' )
										{
											streetView.setPosition( m.getPosition() );
											streetView.setVisible();
										}
									}
								}
							});
						}
					}

				}
			}, 
			streetviewpanorama:{
				options:{
					container: this.st_el
					, opts:{
						position: new google.maps.LatLng( $('input[name="'+fieldname+'[street_lat]"]').val(), $('input[name="'+fieldname+'[street_lng]"]').val() )
						, pov:{
							heading: parseFloat( $('input[name="'+fieldname+'[street_heading]"]').val() )
							, pitch: parseFloat( $('input[name="'+fieldname+'[street_pitch]"]').val() )
							, zoom: parseFloat( $('input[name="'+fieldname+'[street_zoom]"]').val() )
						}
						, addressControl	: false
						, clickToGo			: true
						, panControl		: true
						, linksControl		: true
						, mode				: 'webgl'	//required for firefox fix
					}
				}
				, events:{
					pov_changed:function( pano ){
						$('input[name="'+fieldname+'[street_heading]"]').val( parseFloat( pano.pov.heading ) );
						$('input[name="'+fieldname+'[street_pitch]"]').val( parseFloat( pano.pov.pitch ) );
						$('input[name="'+fieldname+'[street_zoom]"]').val( parseFloat( pano.pov.zoom ) );
					}
					, position_changed: function( pano ){
						$('input[name="'+fieldname+'[street_lat]"]').val( parseFloat( pano.getPosition().lat() ) );
						$('input[name="'+fieldname+'[street_lng]"]').val( parseFloat(  pano.getPosition().lng() ) );
					}
				}
			}
		};

		this.el.css("height", 300).gmap3( this.map_options );
		this.map = this.el.gmap3('get');

		if( !this.streetView && this.el.length > 0 ){
			this.map.getStreetView().setVisible( false );
		}

		var redux_gmaps_obj = this;
		$( document )
			.on("keydown", ".loca_txt_find_address", function(e){
				if(e.keyCode == 13){
					e.preventDefault();
					return false;
				}
			})
			.on("keyup", ".loca_txt_find_address", function(e){
				e.preventDefault();

				if(e.keyCode == 13){
					$(".loca_btn_find_address").trigger("click");
				}
				return false;
			})
			.on("click", ".loca_btn_find_address", function(e){

				var _addr = $(".loca_txt_find_address").val();
				$(".loca-item-map-container").gmap3({
					getlatlng:{
						address:_addr,
						callback:function(r){
							if(!r){
								alert('<?php echo $alerts["address_search_fail"];?>');
								return false;
							}
							var _find = r[0].geometry.location;
							$("input[name='"+fieldname+"[lat]']").val(_find.lat());
							$("input[name='"+fieldname+"[lng]']").val(_find.lng());
							$(".loca-item-map-container").gmap3({
								get:{
									name:"marker",
									callback:function(m){
										m.setPosition(_find);
										$(".loca-item-map-container").gmap3({map:{options:{center:_find}}});
									}
								}
							});
						}
					}
				});
			})
			.on( 'click', '[name="'+fieldname+'[street_visible]"]', function(e){

				if( $(this).is(":checked") )
				{
					redux_gmaps_obj.st_el.removeClass('hidden');
					redux_gmaps_obj.map.getStreetView().setVisible( true );
				}else{
					redux_gmaps_obj.st_el.addClass('hidden');
					redux_gmaps_obj.map.getStreetView().setVisible( false );
				}
			});
    }

})( jQuery );