<?php
    /**
     * The template for the panel header area.
     * Override this template by specifying the path where it is stored (templates_path) in your Redux config.
     *
     * @author      Redux Framework
     * @package     ReduxFramework/Templates
     * @version:    3.5.4.18
     */

    $tip_title = __( 'Developer Mode Enabled', 'redux-framework' );

    if ( $this->parent->dev_mode_forced ) {
        $is_debug     = false;
        $is_localhost = false;

        $debug_bit = '';
        if ( Redux_Helpers::isWpDebug() ) {
            $is_debug  = true;
            $debug_bit = __( 'WP_DEBUG is enabled', 'redux-framework' );
        }

        $localhost_bit = '';
        if ( Redux_Helpers::isLocalHost() ) {
            $is_localhost  = true;
            $localhost_bit = __( 'you are working in a localhost environment', 'redux-framework' );
        }

        $conjunction_bit = '';
        if ( $is_localhost && $is_debug ) {
            $conjunction_bit = ' ' . __( 'and', 'redux-framework' ) . ' ';
        }

        $tip_msg = __( 'This has been automatically enabled because', 'redux-framework' ) . ' ' . $debug_bit . $conjunction_bit . $localhost_bit . '.';
    } else {
        $tip_msg = __( 'If you are not a developer, your theme/plugin author shipped with developer mode enabled. Contact them directly to fix it.', 'redux-framework' );
    }
?>
<div id="redux-header2">
suliii
    <?php if ( ! empty( $this->parent->args['display_name'] ) ) { ?>
        <div class="display_header">

            <?php if ( isset( $this->parent->args['dev_mode'] ) && $this->parent->args['dev_mode'] ) { ?>
                <div class="redux-dev-mode-notice-container redux-dev-qtip"
                     qtip-title="<?php echo esc_attr( $tip_title ); ?>"
                     qtip-content="<?php echo esc_attr( $tip_msg ); ?>">
                    <span
                        class="redux-dev-mode-notice"><?php _e( 'Developer Mode Enabled', 'redux-framework' ); ?></span>
                </div>
            <?php } elseif (isset($this->parent->args['forced_dev_mode_off']) && $this->parent->args['forced_dev_mode_off'] == true ) { ?>
                <?php $tip_title    = 'The "forced_dev_mode_off" argument has been set to true.'; ?>
                <?php $tip_msg      = 'Support options are not available while this argument is enabled.  You will also need to switch this argument to false before deploying your project.  If you are a user of this product and you are seeing this message, please contact the author of this theme/plugin.'; ?>
                <div class="redux-dev-mode-notice-container redux-dev-qtip" 
                     qtip-title="<?php echo esc_attr( $tip_title ); ?>"
                     qtip-content="<?php echo esc_attr( $tip_msg ); ?>">
                    <span
                        class="redux-dev-mode-notice" style="background-color: #FF001D;"><?php _e( 'FORCED DEV MODE OFF ENABLED', 'redux-framework' ); ?></span>
                </div>
            
            <?php } ?>

            <h2><?php echo wp_kses_post( $this->parent->args['display_name'] ); ?></h2>

            <?php if ( ! empty( $this->parent->args['display_version'] ) ) { ?>
                <span><?php echo wp_kses_post( $this->parent->args['display_version'] ); ?></span>
            <?php } ?>

        </div>
    <?php } ?>

    <div class="clear"></div>
</div>

<?php
// ETIENNE ROLE
    // Stickybar
    echo '<div id="redux-sticky">';
    echo '<div id="info_bar">';

    $active_role = isset($_REQUEST['role']) ? $_REQUEST['role'] : '';
    ?>
    <script>
        var ut_changeRole = function(role) {
            var url = '<?php echo admin_url('admin.php?page=ultimate-tweaker');?>';
            if(role) {
                url += '&role=' + role;
            }

            var currentSection = jQuery('.redux-group-menu .active [data-section-id]').data('section-id');
            if(currentSection) {
                url += '&section=' + currentSection;
            }

            window.location.href = url;
        }
    </script>
    <?php
    echo '<div class="role_select" style="float: left;">';
//              echo '<span class="role_label">Settings: </span>';
    echo '<select onchange="ut_changeRole(this.value)">';

    $roles = array(""=>'All roles and visitors');

    global $wp_roles;
    if ( ! isset( $wp_roles ) ) {
        $wp_roles = new WP_Roles();
    }

    foreach($wp_roles->get_names() as $role=>$role_name) {
        $roles[$role] = sprintf("Only for %ss", $role_name);
    }

    foreach($roles as $role=>$role_name) {
        $isSelected = $active_role == $role ? 'selected' : '';
        echo "<option value='{$role}' {$isSelected}>" . $role_name .'</option>';
    }

    echo '</select>';
    echo '<a class="ut_role_manager_a" href="javasctipt:void(0)" style="margin-left: 10px;vertical-align: middle;">Role Manager</a>';
    echo '</div>';

    $expanded = ( $this->args['open_expanded'] ) ? ' expanded' : '';
    $hide_expand = $this->args['hide_expand'] ? ' style="display: none;"' : '';
    
    echo '<a href="javascript:void(0);" class="expand_options' . $expanded . '"' . $hide_expand . '>' . __( 'Expand', 'redux-framework' ) . '</a>';
    
    echo '<div class="redux-action_bar">';
    submit_button( __( 'Save Changes', 'redux-framework' ), 'primary', 'redux_save', false );

    // if ( false === $this->args['hide_reset'] ) {
        echo '&nbsp;';
        submit_button( __( 'Reset Role', 'redux-framework' ), 'secondary', 'resetrole', false );
        echo '&nbsp;';
        submit_button( __( 'Reset All', 'redux-framework' ), 'secondary', 'resetall', false );
    // }

    echo '</div>';

    echo '<div class="redux-ajax-loading" alt="' . __( 'Working...', 'redux-framework' ) . '">&nbsp;</div>';
    echo '<div class="clear"></div>';
    echo '</div>';