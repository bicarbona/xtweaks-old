<?php
/**
*   Plugin Name: Xtweaks
*   Plugin URI:  https://bitbucket.org/bicarbona/xtweaks
*   Description: Xtweaks - admin css, plugins etc...
*   Version:     0.6.9
*   Author:      Bicarbona
*   Author URI:
*   License:     GPLv2+
*   Text Domain: xtweaks
*   Domain Path: /languages
*   Bitbucket Plugin URI: https://bitbucket.org/bicarbona/xtweaks
*   Bitbucket Branch: master
**/


define('XTW_PLUGIN_PATH',  plugin_dir_path( __FILE__ ) );



/**
*
*   Add link to settings
*   in plugin listing
*
**/

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'xtweaks_action_links' );

function xtweaks_action_links( $links ) {
   $links[] = '<a href="'. esc_url( get_admin_url(null, 'admin.php?page=Tweaks') ) .'">Settings</a>';
   // $links[] = '<a href="'. esc_url( get_admin_url(null, 'update-core.php') ) .'">Updates</a>';
  $links[] = '<a href="'. esc_url( get_admin_url(null, 'update-core.php?force-check=1') ) .'">Updates Check</a>';

   // http://p4u/wp-admin/update-core.php?force-check=1
   return $links;
}


/**
*
*   Vypnutie / zapnutie LESS
*   url => ?lessoff
*
*/

if(isset($_GET['lessoff'])) {

    define('INC_LESS', false);

} else {

    define('INC_LESS', true);

}


/**
*
*   Debug Mode [actual not work]
*
**/

if(isset($_GET['debug'])){
    include_once 'lib/debug.php';
}


if ( ! defined( 'XTW_SLUG' ) ) {
    define( 'XTW_SLUG', trim( dirname( plugin_basename( __FILE__ ) ), '/' ) );
}

define( 'XTW_URL', plugins_url() . '/' . XTW_SLUG );


/**
*
*   Redux config
*
**/

if ( file_exists( dirname( __FILE__ ) . '/sections/redux-config-main-tweaks.php' ) ) {

    // require_once( dirname( __FILE__ ) . '/sections/sample-config.php' );
    require_once( dirname( __FILE__ ) . '/sections/redux-config-main-tweaks.php' );

}


/**
*
*   Load Custom Redux Admin panels
*   Header
**/

function filter_redux_panel_header(){
    $dir = plugin_dir_path( __FILE__ );
        $path  = $dir .'redux/template/panel/header.tpl.php';
    return $path;
}

add_filter('redux/redux_tweaks/panel/template/header.tpl.php', 'filter_redux_panel_header');


/**
*
*   Call options
*
**/

$options = get_option('redux_tweaks');


/**
*
*   Load Tweaks Modules
*
**/

foreach ( glob(__DIR__ . '/tweaks/*.php') as $my_theme_filename ) {
    // Exclude files whose names contain -sample
    if (!strpos($my_theme_filename, '-sample') ) {
      include_once $my_theme_filename;
    }
}

/**
*
*   Options to LESS
*
**/

if( INC_LESS == TRUE ){ // Turn OFF -> url => ?lessoff

    require_once( dirname( __FILE__ ) . '/option-to-less.php');

}
/**
*
*
**/

function xtw_get_tags() {
  $return_arr = array();
  if ( ! empty( $_GET[ 'post' ] ) ) {
    $ctname       = '';
    $thispostmeta = get_post_meta( $_GET[ 'post' ] );
    $ctname       = ( ! empty( $thispostmeta[ '_content_general_other-tax' ][ 0 ] ) ? $thispostmeta[ '_content_general_other-tax' ][ 0 ] : null );
    if ( $ctname ) {
      $args       = array( 'hide_empty' => false );
      $custom_tax = get_terms( $ctname, $args );
      foreach ( $custom_tax as $ct ) {
        $return_arr[ $ct->slug ] = $ct->name;
      }
    }
  }

  return $return_arr;
}

// var_dump($_GET[ 'post' ]);

function xtw_get_tags_2() {
  $return_arr = array();
  // if ( ! empty( $_GET[ 'post' ] ) ) {
    $ctname       = '';

    /**
    *$_GET[ 'post' ] aktualne ID tohto Query - >
    */
    $thispostmeta = get_post_meta( $_GET[ 'post' ] );

    $tax_name       = ( ! empty( $thispostmeta[ '_content_general_other-tax' ][ 0 ] ) ? $thispostmeta[ '_content_general_other-tax' ][ 0 ] : null );

    $tax_name  = 'tragac';

    if ( $tax_name ) {
      $args       = array( 'hide_empty' => false );
      $custom_tax = get_terms( $tax_name, $args );
      foreach ( $custom_tax as $ct ) {
        $return_arr[ $ct->slug ] = $ct->name;
      }
    }
  // }

  return $return_arr;
}



?>
