<?php 

global $post;

// print_r($post);
// echo $publishview = publish_view_enqueue();
$publishview = true;

/**

LESS

**/

if (INC_LESS == true){

if (class_exists('WPLessPlugin')){
    $less = WPLessPlugin::getInstance();

    $lessConfig = WPLessPlugin::getInstance()->getConfiguration();

// compiles in the active theme, in a ‘compiled-css’ subfolder
//  $lessConfig->setUploadDir(get_stylesheet_directory() . '/compiled-css');
//  $lessConfig->setUploadUrl(get_stylesheet_directory_uri() . '/compiled-css');

    // if debug Compress css 
    if (!defined('WP_DEBUG') || !WP_DEBUG) {
        $less->getCompiler()->setFormatter('compressed');
    }

// Methods:
// •   compile($string) — Compile a string
// •   compileFile($inFile, [$outFile]) — Compile a file to another or return it
// •   checkedCompile($inFile, $outFile) — Compile a file only if it’s newer
// •   cachedCompile($cacheOrFile, [$force]) — Conditionally compile while tracking imports
// •   setFormatter($formatterName) — Change how CSS output looks
// •   setPreserveComments($keepComments) — Change if comments are kept in output
// •   registerFunction($name, $callable) — Add a custom function
// •   unregisterFunction($name) — Remove a registered function
// •   setVariables($vars) — Set a variable from PHP
// •   unsetVariable($name) — Remove a PHP variable
// •   setImportDir($dirs) — Set the search path for imports
// •   addImportDir($dir) — Append directory to search path for imports

    // $less->addVariable('myColor', '#666');
    // you can now use @myColor in your *.less files

    // $less->addVariable('include', '@import url(ss-gizmo.css);');

    // $less->setVariab les(array(
    //     'include' => '#000',
    //     //'minSize' => '18px'
    // ));

    // if($options['opt-admin-login-body-background']['background-image']){

    //         'opt-admin-login-logo' => $options['opt-admin-login-body-background']['background-image'];

    // }

    // $less->addVariable(array(
    //     // 'opt-color-firm' => $options['opt-color-firm']['color']
    //        'opt-color-firm' => '#000'

    // ));

/**
 Cesta 
**/
    // $plugin_dir = plugin_dir_path( __FILE__ );2

/**

Typography

**/
    
    $params1 = array(
        // 'add-typography' => $options['opt-less-typography'],
        // 'opt-admin-login-logo' => $options['opt-admin-login-logo']['url']
    );

    $params2 = array(
     
/**

**/
 // 'plugin_dir' =>  "'$plugin_dir'",
'plugin_dir' =>  '"'.plugin_dir_path( __FILE__ ).'"',

/**

Webflow Grid

**/

    'add-webflow-grid' => $options['opt-less-webflow-grid'],

/**

Plugins

**/

/**
Sticky menu   
**/

// 'add-sticky-menu' => $options['opt-sticky-menu'],
//     'opt-sticky-zindex'   => $options['opt-sticky-zindex'],
//     'opt-sticky-active-on-height'   => $options['opt-sticky-active-on-height'],
//     'opt-sticky-transition-time'   => $options['opt-sticky-transition-time'].'s',
//     'opt-sticky-disable-small-screen'   => $options['opt-sticky-disable-small-screen'].'px',
//     'opt-sticky-background'   => $options['opt-sticky-background']['color'],

/**
Colorbox
**/

'add-colorbox' => $options['opt-less-colorbox'],

/**

 Woocommerce Cart

**/

'opt-woo-cart-typo-font-size' => $options['opt-woo-cart-typo']['font-size'],
'opt-woo-cart-typo-line-height' => $options['opt-woo-cart-typo']['line-height'],

'opt-woo-cart-color-regular' => $options['opt-woo-cart-color']['regular'],
'opt-woo-cart-color-hover' => $options['opt-woo-cart-color']['hover'],

// Background
'opt-woo-cart-background-regular' => $options['opt-woo-cart-background']['regular'],
'opt-woo-cart-background-hover' => $options['opt-woo-cart-background']['hover'],

'opt-woo-cart-padding-top' => $options['opt-woo-cart-padding']['padding-top'],
'opt-woo-cart-padding-bottom' => $options['opt-woo-cart-padding']['padding-bottom'],
'opt-woo-cart-padding-left' => $options['opt-woo-cart-padding']['padding-left'],
'opt-woo-cart-padding-right' => $options['opt-woo-cart-padding']['padding-right'],

// Row border

'opt-woo-cart-row-border-color' => $options['opt-woo-cart-row-border-color'],

// Btn
'opt-woo-cart-btn-width' => $options['opt-woo-cart-btn-width'],

'opt-woo-cart-btn-padding-top' => $options['opt-woo-cart-btn-padding']['padding-top'],
'opt-woo-cart-btn-padding-bottom' => $options['opt-woo-cart-btn-padding']['padding-bottom'],

'opt-woo-cart-btn-padding-right' => $options['opt-woo-cart-btn-padding']['padding-right'],
'opt-woo-cart-btn-padding-left' => $options['opt-woo-cart-btn-padding']['padding-left'],

// Btn Typo
'opt-woo-cart-btn-typo-font-size' => $options['opt-woo-cart-btn-typo']['font-size'],
'opt-woo-cart-btn-typo-line-height' => $options['opt-woo-cart-btn-typo']['line-height'],

// View Cart
'opt-woo-cart-btn-view-background-regular' => $options['opt-woo-cart-btn-view-background']['regular'],
'opt-woo-cart-btn-view-background-hover' => $options['opt-woo-cart-btn-view-background']['hover'],

'opt-woo-cart-btn-view-color-regular' => $options['opt-woo-cart-btn-view-color']['regular'],
'opt-woo-cart-btn-view-color-hover' => $options['opt-woo-cart-btn-view-color']['hover'],

// Margin
'opt-woo-cart-btn-view-margin-bottom' => $options['opt-woo-cart-btn-view-margin']['padding-bottom'],

// Checkout 
'opt-woo-cart-btn-checkout-background-regular' => $options['opt-woo-cart-btn-checkout-background']['regular'],
'opt-woo-cart-btn-checkout-background-hover' => $options['opt-woo-cart-btn-checkout-background']['hover'],

'opt-woo-cart-btn-checkout-color-regular' => $options['opt-woo-cart-btn-checkout-color']['regular'],
'opt-woo-cart-btn-checkout-color-hover' => $options['opt-woo-cart-btn-checkout-color']['hover'],

// Content
'opt-woo-cart-content-background' => $options['opt-woo-cart-content-background'],
'opt-woo-cart-content-typo-font-size' => $options['opt-woo-cart-content-typo']['font-size'],
'opt-woo-cart-content-typo-text-align' => $options['opt-woo-cart-content-typo']['text-align'],

// Padding
'opt-woo-cart-content-padding-top' => $options['opt-woo-cart-content-padding']['padding-top'],
'opt-woo-cart-content-padding-bottom' => $options['opt-woo-cart-content-padding']['padding-bottom'],
'opt-woo-cart-content-padding-left' => $options['opt-woo-cart-content-padding']['padding-left'],
'opt-woo-cart-content-padding-right' => $options['opt-woo-cart-content-padding']['padding-right'],

// Remove x
'opt-woo-cart-remove-color-regular' => $options['opt-woo-cart-remove-color']['regular'],
'opt-woo-cart-remove-color-hover' => $options['opt-woo-cart-remove-color']['hover'],

'opt-woo-cart-remove-typo-font-size' => $options['opt-woo-cart-remove-typo']['font-size'],
'opt-woo-cart-remove-typo-line-height' => $options['opt-woo-cart-remove-typo']['line-height'],

// Item Title
'opt-woo-cart-item-title-typo-font-size' => $options['opt-woo-cart-item-title-typo']['font-size'],
'opt-woo-cart-item-title-typo-line-height' => $options['opt-woo-cart-item-title-typo']['line-height'],

'opt-woo-cart-item-title-color-regular' => $options['opt-woo-cart-item-title-color']['regular'],
'opt-woo-cart-item-title-color-hover' => $options['opt-woo-cart-item-title-color']['hover'],

// Image
'opt-woo-cart-image-size' => $options['opt-woo-cart-image-size'].'px',

// Fixed Position
'opt-woo-cart-fixed-margin-left' => $options['opt-woo-cart-fixed-margin']['margin-left'],
'opt-woo-cart-fixed-margin-right' => $options['opt-woo-cart-fixed-margin']['margin-right'],
'opt-woo-cart-fixed-margin-top' => $options['opt-woo-cart-fixed-margin']['margin-top'],
'opt-woo-cart-fixed-margin-bottom' => $options['opt-woo-cart-fixed-margin']['margin-bottom'],

// Subtotal
'opt-woo-cart-subtotal-background' => $options['opt-woo-cart-subtotal-background'],
'opt-woo-cart-subtotal-color' => $options['opt-woo-cart-subtotal-color'],
'opt-woo-cart-subtotal-typo-text-align' => $options['opt-woo-cart-subtotal-typo']['text-align'],
'opt-woo-cart-subtotal-typo-font-size' => $options['opt-woo-cart-subtotal-typo']['text-align'],
'opt-woo-cart-subtotal-typo-line-height' => $options['opt-woo-cart-subtotal-typo']['line-height'],


/**
Fixed Sidebar
**/

'add-fixed-sidebar' => $options['opt-sidebar-fixed'],

'opt-sidebar-fixed-background-color' => $options['opt-sidebar-fixed-background']['color'],
'opt-sidebar-fixed-background-rgba' => $options['opt-sidebar-fixed-background']['alpha'],

'opt-sidebar-fixed-zindex' => $options['opt-sidebar-fixed-zindex'],
    'opt-sidebar-fixed-width' => $options['opt-sidebar-fixed-width']['width'],

    'opt-sidebar-fixed-height' => $options['opt-sidebar-fixed-height']['height'],

    'opt-sidebar-fixed-transition' => $options['opt-sidebar-fixed-transition'],
    'opt-sidebar-fixed-transition' => $options['opt-sidebar-fixed-transition'],


    'opt-sidebar-fixed-transform-x' => $options['opt-sidebar-fixed-transform']['width'],
    'opt-sidebar-fixed-transform-y' => $options['opt-sidebar-fixed-transform']['height'],
    'opt-sidebar-fixed-id' => $options['opt-sidebar-fixed-id'],

/**
Fix HW Full Width Wrapper
*/
'opt-hw-fullwidth-wrapper' => $options['opt-hw-fullwidth-wrapper'],
'opt-hw-fullwidth-wrapper-id' => $options['opt-hw-fullwidth-wrapper-id'],

/**
Scroll Back to top
**/    
    'opt-scroll-back-hover-background' => $options['opt-scroll-back-hover-background']['color'],

    'opt-scroll-back-hover' => $options['opt-scroll-back-hover']['color'],
    'opt-scroll-back-color' => $options['opt-scroll-back-color']['color'],

    'opt-scroll-back-dimension-width' => $options['opt-scroll-back-dimension']['width'],
    'opt-scroll-back-dimension-height' => $options['opt-scroll-back-dimension']['height'],
    'opt-scroll-back-border-radius' => $options['opt-scroll-back-border-radius'].'px',
    'opt-scroll-back-background' => $options['opt-scroll-back-background']['color'],
    'opt-scroll-back-horizontal-distance' => $options['opt-scroll-back-horizontal-distance'].'px',
    'opt-scroll-back-vertical-distance' => $options['opt-scroll-back-horizontal-distance'].'px',
   
    'opt-scroll-back-border-color' => $options['opt-scroll-back-border']['border-color'],
    'opt-scroll-back-border-style' => $options['opt-scroll-back-border']['border-style'],
    'opt-scroll-back-border-width' => $options['opt-scroll-back-border']['border-top'],

    'opt-scroll-back-font-size' => $options['opt-scroll-back-font-size'].'px',
    'opt-scroll-back-line-height' => $options['opt-scroll-back-line-height'].'px',

    // 'opt-scroll-back-border-top' => $options['opt-scroll-back-border']['border-top'],
    // 'opt-scroll-back-border-bottom' => $options['opt-scroll-back-border']['border-bottom'],
    // 'opt-scroll-back-border-right' => $options['opt-scroll-back-border']['border-right'],
    // 'opt-scroll-back-border-left' => $options['opt-scroll-back-border']['border-left'],


    
/**
Include Less module
**/
    'add-arrow-icons' => $options['opt-arrow-icons'],
    'add-wp-pagenavi' => $options['opt-less-wp-pagenavi'],

// 'add-typography' => $options['opt-less-typography'],
    'add-woocommerce' => $options['opt-less-woocommerce'],
    'add-fix-headway' => $options['opt-less-add-fix-headway'],
    'add-buttons' => $options['opt-less-buttons'],
    'add-wrappers' => $options['opt-less-wrappers'],
    'add-animations' => $options['opt-less-animations'],
    'add-labels-badges' => $options['opt-less-labels-badges'],
    'add-ss-gizmo' => $options['opt-less-ss-gizmo'],
    'add-alert-higlight' => $options['opt-less-alert-higlight'],
    'add-plugins'=> $options['opt-less-plugins'],
    'add-scroll-back' => $options['opt-scroll-back'],

// Min Height
    'disable-min-height' => $options['opt-less-disable-min-height'],

// opt-scroll-back-hover-background



/**
Define Global Colors
**/

    'opt-color-firm' => $options['opt-color-firm']['color'],
    'opt-color-firm-transparency' => $options['opt-color-firm']['alpha'],

    'opt-color-red' => $options['opt-color-red']['color'],
    'opt-color-red-transparency' => $options['opt-color-red']['alpha'],

    'opt-color-blue' => $options['opt-color-blue']['color'],
    'opt-color-blue-transparency' => $options['opt-color-blue']['alpha'],

    'opt-color-orange' => $options['opt-color-orange']['color'],
    'opt-color-orange-transparency' => $options['opt-color-orange']['alpha'],

    'opt-color-yellow' => $options['opt-color-yellow']['color'],
    'opt-color-yellow-transparency' => $options['opt-color-yellow']['alpha'],

    'opt-color-violet' => $options['opt-color-violet']['color'],
    'opt-color-violet-transparency' => $options['opt-color-violet']['alpha'],

    'opt-color-black' => $options['opt-color-black']['color'],
    'opt-color-black-transparency' => $options['opt-color-black']['alpha'],

    'opt-color-white' => $options['opt-color-white']['color'],
    'opt-color-white-transparency' => $options['opt-color-white']['alpha'],

    'opt-color-success' => $options['opt-color-success']['color'],
    'opt-color-success-transparency' => $options['opt-color-success']['alpha'],

    'opt-color-danger' => $options['opt-color-danger']['color'],
    'opt-color-danger-transparency' => $options['opt-color-danger']['alpha'],

    'opt-color-info' => $options['opt-color-info']['color'],
    'opt-color-info-transparency' => $options['opt-color-info']['alpha'],

    'opt-color-warning' => $options['opt-color-warning']['color'],
    'opt-color-warning-transparency' => $options['opt-color-warning']['alpha'],

    'opt-color-inverse' => $options['opt-color-inverse']['color'],
    'opt-color-inverse-transparency' => $options['opt-color-inverse']['alpha'],

    'opt-color-base' => $options['opt-color-base']['color'],
    'opt-color-base-transparency' => $options['opt-color-base']['alpha'],

    'opt-color-gray' => $options['opt-color-gray']['color'],
    'opt-color-gray-transparency' => $options['opt-color-gray']['alpha'],

    'opt-color-green' => $options['opt-color-green']['color'],
    'opt-color-green-transparency' => $options['opt-color-green']['alpha'],
   
/**
Admin / Login
**/

    'opt-admin-login-logo' => "'$url_admin_logo'",
    'opt-admin-login-body-background' => $options['opt-admin-login-body-background']['background-color'],
    
    // 'opt-admin-login-form-background' => $options['opt-admin-login-form-background']['color'],
    // 'opt-admin-login-color-link-text' => $options['opt-admin-login-color-link-text']['regular'],
    'opt-admin-login-color-link-text' => $options['opt-admin-login-color-link-text']['hover'],

    // 'opt-color-firm' => $options['opt-color-firm']['color'],
    // 'opt-color-firm-transparency' => $options['opt-color-firm']['alpha'],


/**
Admin Color
**/
    'opt-admin-highlight-color' => $options['opt-admin-highlight-color']['color'],
    'opt-admin-base-color' => $options['opt-admin-base-color']['color'],
    'opt-admin-notification-color'  => $options['opt-admin-notification-color']['color'],
    'opt-admin-body-background' => $options['opt-admin-body-background']['color'],
    'opt-admin-link-color' => $options['opt-admin-link-color']['color'],

/**
Admin Tweaks
**/
    'add-publish-view'=> $publishview, // tlacitko na stranke postu / page
    'opt-fix-acf-label' => $options['opt-fix-acf-label'],


);

$params = array_merge($params1, $params2);
    // $variables = array(
    //    'add-typography' => $options['opt-less-typography'],
    // );

$less->setVariables($params);

    // $less->setVariables(array(
    //     'myColor' => '#777',
    //     'minSize' => '18px'
    // ));
    // you can now use @minSize in your *.less files
    // @myColor value has been updated to #777
}
}
/**

Len ilustracne ake su moznosti 

**/
// var_dump ($options['opt-typography']);
// var_dump($options['opt-color-rgba']);

// $options['opt-typography']['font-family'];
// $options['opt-typography']['font-weight'];
// $options['opt-typography']['font-style'];
// $options['opt-typography']['subsets'];
// $options['opt-typography']['text-align'];
// $options['opt-typography']['font-size'];
// $options['opt-typography']['line-height'];
// $options['opt-typography']['color'];

?>