<?php
$main_wrapper = 'div';

?>
<a href="#">
     <div class="row property">
       <?php the_title( '<h3>', '</h3>' ); ?>
         <div class="col-lg-5 col-lg-offset-0 col-md-3 col-sm-3">
           <a href="#" class="post-thumbnail post-thumbnail-left">
          <?php if ( has_post_thumbnail() ) {
            	the_post_thumbnail('thumbnail');
            } ?>
            </a>
         </div>
         <div class="col-lg-6 col-md-8 col-sm-8">
          <?php the_content(); ?>
          <p class="align-left">Rozloha: 122 m2</p>
          <p class="align-right">Cena za m2: 721 € / m2</p>
          <p class="align-left">Lokalita: Bratislava V / Petržalka</p>
          <p class="align-right">Stav: čiastočná rekonštrukcia</p>
         </div>
     </div>
</a>
