<?php
$main_wrapper = 'div';
?>
<div class="row">
   <div class="col-lg-5 col-lg-offset-0 col-md-3 col-sm-3">
      <a href="<?php echo the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php if ( has_post_thumbnail() ) {
      	// the_post_thumbnail('thumbnail');
        woocommerce_template_loop_product_thumbnail();
      }
      ?>
       </a>
   </div>
   <div class="col-lg-6 col-md-8 col-sm-8">
    <?php the_title( '<h3>', '</h3>' ); ?>
    <?php
    woocommerce_show_product_loop_sale_flash();
    if( $hide_rating_products != 'yes' ) {
      woocommerce_template_loop_rating();
    } // product rating

    if( $hide_price_products != 'yes' ) {
      woocommerce_template_loop_price();
    } // product price

    if( $hide_add_to_cart_products != 'yes' ) {
      echo '<p class="add-to-cart-button">';
      woocommerce_template_loop_add_to_cart();
      echo '</p>';
    } // product add to cart
    // if( 'none' != $description_products ) {
      if( $description_products == 'short' ) {
        woocommerce_template_single_excerpt();
      } else {
        the_content();
      }

    // } // product description ?>
   </div>
</div>
