<?php
$main_wrapper = 'div';

?>
<div class="team-member">
        <?php if ( has_post_thumbnail() ) {
            the_post_thumbnail('thumbnail');
          } ?>
        <div class="team-details">
        <?php the_title( '<h3>', '</h3>' ); ?>
        <p class="position">Co-Founder</p>
        <?php the_content(); ?>
    </div>
</div>
