<?php

global $post;
$id = $post->ID;

ob_start();

the_content();

$content = ob_get_contents();

ob_end_clean();

?>
 
