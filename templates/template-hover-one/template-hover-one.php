<?php
$main_wrapper = 'div';

?>
<div class="grid">
    <figure class="effect-lily">
      <?php if ( has_post_thumbnail() ) {
          the_post_thumbnail('thumbnail');
        } ?>
            <figcaption>
            <?php the_title( '<h3>', '</h3>' ); ?>
            <?php the_content(); ?>
            <a href="#">View more</a>
        </figcaption>
    </figure>
</div>
