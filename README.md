## Turn LESS off 

url => ?lessoff

## Login

Shortcode [login]

### Options 

+ login redirect page
+ logout redirect page

## Error Log

~~~php
_log( 'Testing the error message logging' );
_log( array('it' => 'works' ));
~~~

### Read Log in Terminal

~~~
tail -f wp-content/debug.log
~~~

## What is implemented

### Login
+ Login Shake
+ Set Remember Me To Be Checked
+ Logo Title
+ Login Page Logo Url / presmeruje kliknutie na logo login page
+ Login Page Theme

## Redux to -> LESS (swith)
pre podmienene includovanie musi byt redux switch nastavene na **'default' => '0'** inac padne

## Fix fullwidth content

~~~css
#wrapper-wml55b7dd6fa4077 div.grid-container{
    width: auto !important;
    max-width: 100% !important;
}
~~~

## Hmm?
~~~
[post_title]
[post_excerpt]
[permalink]
[post_status]

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
~~~