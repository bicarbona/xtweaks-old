<?php


function headway_hooks() {
		$hooks = array(
			'headway_after_block' => array(),
			'headway_after_entry' => array(),
			'headway_after_entry_comments' => array(),
			'headway_after_entry_content' => array(),
			'headway_after_entry_title' => array(),
			'headway_after_footer' => array(),
			'headway_after_header_link' => array(),
			'headway_after_tagline' => array(),
			'headway_after_wrapper' => array(),
			'headway_before_block' => array(),
			'headway_before_entry' => array(),
			'headway_before_entry_comments' => array(),
			'headway_before_entry_content' => array(),
			'headway_before_entry_title' => array(),
			'headway_before_footer' => array(),
			'headway_before_header_link' => array(),
			'headway_before_wrapper' => array(),
			'headway_block_close' => array(),
			'headway_block_content_close' => array(),
			'headway_block_content_open' => array(),
			'headway_block_open' => array(),
			'headway_body_close' => array(),
			'headway_body_open' => array(),
			'headway_entry_close' => array(),
			'headway_entry_open' => array(),
			'headway_footer_close' => array(),
			'headway_footer_open' => array(),
			'headway_head_extras' => array(),
			'headway_html_close' => array(),
			'headway_html_open' => array(),
			'headway_page_start' => array(),
			'headway_register_elements' => array(),
			'headway_scripts' => array(),
			'headway_seo_meta' => array(),
			'headway_setup' => array(),
			'headway_setup_child_theme' => array(),
			'headway_stylesheets' => array(),
			'headway_whitewrap_close' => array(),
			'headway_whitewrap_open' => array(),
			'headway_wrapper_close' => array(),
			'headway_wrapper_open' => array(),
		);

		return $hooks;
	}


// pizza

/**
arc_before_panel_open_filter
arc_after_architect
arc_after_architect_{$bp_shortname}
arc_bottom_right_navigation_{$bp_shortname}
arc_after_navigation
arc_after_navigation_{$bp_shortname}
arc_after_pagination_below_{$bp_shortname}
arc_after_pagination_below
arc_before_pagination_below
arc_before_pagination_below_{$bp_shortname}
arc_after_panels_wrapper

arc_after_panels_wrapper_{$bp_shortname}
**/
 ?>
