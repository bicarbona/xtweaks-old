<?php 

// SECURITY
// Limit Login Attempts
// https://wordpress.org/plugins/limit-login-attempts/screenshots/
//http://www.paulund.co.uk/secure-wordpress


/**
*
*    Login Page Theme
*
**/

function xtw_login_theme_loader(){
    wp_enqueue_style('login-theme', plugins_url("../less/admin/admin-login.less", __FILE__));
}
add_action('login_enqueue_scripts', 'xtw_login_theme_loader' );


/**
*
*   Login Page Logo Url
*
**/

add_filter( 'login_headerurl', 'xtw_login_headerurl' );
    
function xtw_login_headerurl( $url ) {
    return esc_url( home_url( '/' ) );
}

/**
*
*   Logo Title
*
**/

add_filter( 'login_headertitle', 'xtw_login_headertitle' );

function xtw_login_headertitle( $title ) {
    return get_bloginfo ( 'name' );
}


/**
*
*   Change the Logout Message
*
*   + iddle user.. atd navod
*   http://reneeshupe.com/2015/logging-out-of-wordpress-step-by-step-tutorial/
*
**/

add_filter( 'wp_login_errors', 'xtw_logout_message' );

function xtw_logout_message( $errors ){

    //if ( isset( $errors->errors['loggedout'] ) ){
        unset($errors->errors['loggedout'][0]);
        unset($errors->errors['loggedout'][2]);
  //  }

    return $errors;
}

/**
*
*   Login Shake
*
**/

function xtw_login_shake() {
    remove_action('login_head', 'wp_shake_js', 12);
}

add_action('login_head', 'xtw_login_shake');


/**
*
*   Set Remember Me To Be Checked
*
**/

function xtw_login_checked_remember_me() {
    add_filter( 'login_footer', 'xtw_rememberme_checked' );
}

add_action( 'init', 'xtw_login_checked_remember_me' );

function xtw_rememberme_checked() {
    echo "<script>document.getElementById('rememberme').checked = true;</script>";
}


/**
*
*   Remove Lost Password Text
*
**/

function xtw_remove_lostpassword_text ( $text ) {
    if ($text == 'Zabudli ste heslo?'){ $text = ''; }
        return $text;
    }
add_filter( 'gettext', 'xtw_remove_lostpassword_text' );

// add_filter('login_errors', create_function('$a', "return '<b>Error:</b> Invalid Username or Password';"));


/**
*
*    Login Error Message
*
**/

function xtw_login_error_override() {
    return 'Incorrect login details';
}

add_filter('login_errors', 'xtw_login_error_override');


?>