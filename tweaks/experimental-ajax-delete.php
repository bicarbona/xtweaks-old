<?php
//  http://www.wordpressintegration.com/blog/wordpress-post-using-ajax/

if($options['opt-experimental-ajax-delete']){

/**
*
*   Add change status
*
**/

function status_link() {

   $statuses = get_post_statuses();
   // print_r($statuses);
    print(' ');

    echo $_REQUEST['id'];

    foreach ($statuses as $key => $value) {

        print '<a href="#" data-id="'.get_the_ID().'" data-nonce="'. wp_create_nonce('my_delete_post_nonce').'" data-action="change_post_status_1" data-status="'.$key.'" class="delete-post">'.$key.'</a> ';

    }

}

/**
*
*   Add Delete link after HW content
*
**/

function ajax_delete_post_link() {

  if( current_user_can( 'delete_post' ) ) {

    print '<a href="#" data-id="'.get_the_ID().'" data-nonce="'. wp_create_nonce('my_delete_post_nonce').'" data-action="my_delete_post" class="delete-post">delete</a>';

        status_link();

  }

  // print get_site_url().'/ajaxflow/my_delete_post';
}

add_action('headway_after_entry_content','ajax_delete_post_link' );

/**
*
*   Enque JS / Localize
*
**/

function ajax_frontend_script() {

if ( !is_admin() ) {
  wp_enqueue_script( 'my_script', plugins_url( '../lib/js/experimental-delete-post.js', __FILE__ ), array( 'jquery-ui-sortable', 'jquery' ), '1.0.0', true );
}


/*
Localize
*/
wp_localize_script( 'my_script', 'MyAjax',
                      array( 'ajaxurl' => admin_url( 'admin-ajax.php' ),
                      //'ajax_acion' => 'my_delete_post'
                      ) );

   /**
    Pokus zaviest plugin AJAX FLOW - mal by zrychlit ajax dopytovanie...
    zatial vracia chybu ze hooks neexistuje ... debil
   **/
    // wp_localize_script( 'my_script', 'MyAjax', array( 'ajaxurl' => get_site_url().'/ajaxflow/my_delete_post' ) );
    // wp_localize_script( 'my_script', 'MyAjax', array( 'ajaxurl' => '/ajaxflow/my_delete_post' ) );

}

add_action( 'wp_enqueue_scripts', 'ajax_frontend_script' );


/**
*
*   Ajax Request
*
**/

function my_delete_post(){

    $permission = check_ajax_referer( 'my_delete_post_nonce', 'nonce', false );
    if( $permission == false ) {
        echo 'error';
    }
    else {
        wp_delete_post( $_REQUEST['id'] );
        echo 'success';
    }

    die();

}

add_action( 'wp_ajax_my_delete_post', 'my_delete_post' );


/**
*
*   Update Post Status
*   http://www.wordpressintegration.com/blog/wordpress-post-using-ajax/
*   more about status http://code.tutsplus.com/articles/how-to-implement-post-status-transitions-for-custom-web-applications--wp-33486
*   http://wordpress.stackexchange.com/questions/115693/delete-post-meta-by-ajax
*   http://www.vandelaydesign.com/delete-a-wordpress-post-using-ajax/
**/

if(!function_exists('wp_get_current_user')) {
    include(ABSPATH . "wp-includes/pluggable.php");
}

function change_post_status_1( $post_id, $status ){

    $post_id = $_REQUEST['id'];
    $status = $_REQUEST['status'];

    $current_post = get_post( $post_id, 'ARRAY_A' );
    $current_post['post_status'] = $status;
    wp_update_post($current_post);
}

add_action( 'wp_ajax_change_post_status_1', 'change_post_status_1' );

}
?>
