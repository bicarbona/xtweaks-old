<?php 
//WordPress Dashicons List
// http://www.kevinleary.net/wordpress-dashicons-list-custom-post-type-icons/

/**
*
*  @TODO flush rewrite
*  add_action( 'init', 'flush_rewrite_rules' );
*
**/

//CPT Portfolio
if(isset($options['opt-cpt-portfolio']) && $options['opt-cpt-portfolio']){

    add_action( 'init', 'cptui_register_portfolio' );
    
    function cptui_register_portfolio() {

        global $options;

        $labels = array(
            "name" => $options['opt-cpt-portfolio-name-singular'],
            "singular_name" => $options['opt-cpt-portfolio-name-singular'],
            "menu_name" => $options['opt-cpt-portfolio-menu-name'],
           // "all_items" => "Všetky položky",
            "add_new" => "Pridať položku",
            "add_new_item" => "Pridať položku",
            "edit" => "Upraviť",
            "edit_item" => "Upraviť položku",
            "new_item" => "Nová položka",
            "view" => "Zobraziť",
            "view_item" => "Zobraziť",
            "search_items" => "Hľadať",
            "not_found" => "Nebolo nájdené",
            "not_found_in_trash" => "Nebolo nájdené v koši",
            "parent" => "Nadradené",
        );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => false,
            "show_in_menu" => true,
            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( 
                               "slug" => $options['opt-cpt-portfolio-slug'], 
                               "with_front" => true 
                               ),
            "query_var" => true,
                "menu_icon" => "dashicons-format-gallery",
                "supports" => array( "title", "editor", "thumbnail" ),      
                "taxonomies" => array( "category" ) 
            );
        register_post_type( "xtw_portfolio", $args );

    }
}

//{"portfolio":{"name":"portfolio","label":"Portf\u00f3lio","singular_label":"Portf\u00f3lio","description":"","public":"true","show_ui":"true","has_archive":"false","has_archive_string":"","exclude_from_search":"false","capability_type":"post","hierarchical":"false","rewrite":"true","rewrite_slug":"","rewrite_withfront":"true","query_var":"true","menu_position":"","show_in_menu":"true","show_in_menu_string":"","menu_icon":null,"supports":["title","editor","thumbnail"],"taxonomies":["category"],"labels":{"menu_name":"Portf\u00f3lio","all_items":"V\u0161etky polo\u017eky","add_new":"Prida\u0165 polo\u017eku","add_new_item":"Prida\u0165 polo\u017eku","edit":"Upravi\u0165","edit_item":"Upravi\u0165 polo\u017eku","new_item":"Nov\u00e1 polo\u017eka","view":"Zobrazi\u0165","view_item":"Zobrazi\u0165","search_items":"H\u013eada\u0165","not_found":"Nebolo n\u00e1jden\u00e9","not_found_in_trash":"Nebolo n\u00e1jden\u00e9 v ko\u0161i","parent":"Nadraden\u00e9"}}}


/**
*
*   Experieces
*
**/

if(isset($options['opt-cpt-experiences']) && $options['opt-cpt-experiences']){

    add_action( 'init', 'cptui_register_experience' );

    function cptui_register_experience() {
        $labels = array(
            "name" => "Skúsenosti",
            "singular_name" => "Skúsenosť",
            "menu_name" => "Skúsenosť",
            "all_items" => "Skúsenosti",
            "add_new" => "Pridať Skúsenosť",
            "add_new_item" => "Pridať Skúsenosť",
            "edit" => "Upraviť Skúsenosť",
            "edit_item" => "Upraviť Skúsenosť",
            "new_item" => "Nová Skúsenosť",
            "view" => "Zobraziť",
            "view_item" => "Zobraziť",
            "search_items" => "Hľadať",
            "not_found" => "Nebolo nájdené",
            "not_found_in_trash" => "Nebolo nájdené v koši",
            "parent" => "Nadradené",
        );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => false,
            "show_in_menu" => true,
            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "experiences", "with_front" => true ),
            "query_var" => true,
                "menu_icon" => "dashicons-format-status",
                "supports" => array( "title", "editor", "excerpt", "thumbnail" ),           
        );
        register_post_type( "xtw_experiences", $args );
    }
}

//{"experiences":{"name":"experiences","label":"Sk\u00fasenosti","singular_label":"Sk\u00fasenos\u0165","description":"","public":"true","show_ui":"true","has_archive":"false","has_archive_string":"","exclude_from_search":"false","capability_type":"post","hierarchical":"false","rewrite":"true","rewrite_slug":"","rewrite_withfront":"true","query_var":"true","menu_position":"","show_in_menu":"true","show_in_menu_string":"","menu_icon":null,"supports":["title","editor","excerpt","thumbnail"],"taxonomies":[],"labels":{"menu_name":"Sk\u00fasenos\u0165","all_items":"Sk\u00fasenosti","add_new":"Prida\u0165 Sk\u00fasenos\u0165","add_new_item":"Prida\u0165 Sk\u00fasenos\u0165","edit":"Upravi\u0165 Sk\u00fasenos\u0165","edit_item":"Upravi\u0165 Sk\u00fasenos\u0165","new_item":"Nov\u00e1 Sk\u00fasenos\u0165","view":"Zobrazi\u0165","view_item":"Zobrazi\u0165","search_items":"H\u013eada\u0165","not_found":"Nebolo n\u00e1jden\u00e9","not_found_in_trash":"Nebolo n\u00e1jden\u00e9 v ko\u0161i","parent":"Nadraden\u00e9"}}}


/**
*
*   Sliders
*
**/

if(isset($options['opt-cpt-slider']) && $options['opt-cpt-slider']) {

    add_action( 'init', 'cptui_register_slider' );
    function cptui_register_slider() {
        $labels = array(
            "name" => "Slider",
            "singular_name" => "Slider",
            "menu_name" => "Slider",
            "all_items" => "Všetky položky",
            "add_new" => "Pridať položku",
            "add_new_item" => "Pridať položku",
            "edit" => "Upraviť",
            "edit_item" => "Upraviť položku",
            "new_item" => "Nová položka",
            "view" => "Zobraziť",
            "view_item" => "Zobraziť",
            "search_items" => "Hľadať",
            "not_found" => "Nebolo nájdené",
            "not_found_in_trash" => "Nebolo nájdené v koši",
            "parent" => "Nadradené",
        );

        $args = array(

            "labels" => $labels,
            "description" => "",
            "public" => false, // vypnute koli wp seo
            "show_ui" => true,
            "has_archive" => false,
            "show_in_menu" => true,
            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( 
                               "slug" => 'kkt',
                            // "slug" => $options['opt-cpt-slider-slug'],
                            "with_front" => true 
                            ),
            "query_var" => true,
                "menu_icon" => "dashicons-slides",
                "supports" => array( "title", "editor", "thumbnail" ),      
                "taxonomies" => array( "category" ) 
        );
        register_post_type( "xtw_slider", $args );
    }

    // Slider ACF - link to post

    if( function_exists('register_field_group') ):

    register_field_group(array (
        'key' => 'group_54ecfbc7e4237',
        'title' => 'Slider',
        'fields' => array (
            array (
                'key' => 'field_54ed02a24ca0e',
                'label' => 'link_to_another_post',
                'name' => 'link_to_another_post',
                'prefix' => '',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => '',
                'taxonomy' => '',
                'filters' => array (
                    0 => 'search',
                    1 => 'post_type',
                    2 => 'taxonomy',
                ),
                'elements' => '',
                'max' => '',
                'return_format' => 'object',
            ),
            array (
                'key' => 'field_54ef22b729c2c',
                'label' => 'slider_icon_class',
                'name' => 'slider_icon_class',
                'prefix' => '',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'xtw_slider',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
    ));

    endif;

    // ACF - Slider Type
    if( function_exists('register_field_group') ):

    register_field_group(array (
        'key' => 'group_54ce603b40c8b',
        'title' => 'Slider Type',
        'fields' => array (
            array (
                'key' => 'field_54ce60457e884',
                'label' => 'Slider Type',
                'name' => 'slider_type',
                'prefix' => '',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array (
                    'not_set' => 'not_set',
                    'columns_excerpt_featured' => 'columns_excerpt_featured',
                    'columns_featured_excerpt' => 'columns_featured_excerpt',
                    'single_featured' => 'single_featured',
                    'single_excerpt' => 'single_excerpt',
                    'above_excerpt_featured' => 'above_excerpt_featured',
                    'above_featured_excerpt' => 'above_featured_excerpt',
                ),
                'default_value' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'placeholder' => '',
                'disabled' => 0,
                'readonly' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'xtw_slider',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'side',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
    ));

    endif;

}    

/**
*
*   Blocks
*
**/

if(isset($options['opt-cpt-blocks']) && $options['opt-cpt-blocks']){

    add_action( 'init', 'cptui_register_content_block' );

    function cptui_register_content_block() {
        $labels = array(
            "name" => "Blok",
            "singular_name" => "Blok",
            "menu_name" => "Bloky",
            "all_items" => "Bloky",
            "add_new" => "Pridať položku",
            "add_new_item" => "Pridať položku",
            "edit" => "Upraviť",
            "edit_item" => "Upraviť položku",
            "new_item" => "Nová položka",
            "view" => "Zobraziť",
            "view_item" => "Zobraziť",
            "search_items" => "Hľadať",
            "not_found" => "Nebolo nájdené",
            "not_found_in_trash" => "Nebolo nájdené v koši",
            "parent" => "Nadradené",
        );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => false,
            "show_in_menu" => true,
            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "content_block", "with_front" => true ),
            "query_var" => true,
                "menu_icon" => "dashicons-screenoptions",
                "supports" => array( "title", "editor", "excerpt", "revisions", "thumbnail" ),      
                "taxonomies" => array( "category" )
        );
        register_post_type( "content_block", $args );

    }
}

/**
*
*   Poradna
*
**/

if(isset($options['opt-cpt-talk-about']) && $options['opt-cpt-talk-about']){

    add_action( 'init', 'cptui_register_talk_about' );
    function cptui_register_talk_about() {

        $labels = array(
            "name" => "Poradňa",
            "singular_name" => "Poradňa",
            "menu_name" => "Poradňa",
            "all_items" => "Otázky a odpovede",
            "add_new" => "Pridať O / O",
            "add_new_item" => "Pridať otázku / odpoveď",
            "edit" => "Upraviť",
            "edit_item" => "Upraviť O / O",
            "new_item" => "Nová O / O",
            "view" => "Zobraziť",
            "view_item" => "Zobraziť O / O",
            "search_items" => "Hľadať O / O",
        );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => true,
            "show_in_menu" => true,
            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "poradna", "with_front" => true ),
            "query_var" => true,
                "menu_icon" => "dashicons-format-chat",
                "supports" => array( "title", "editor", "excerpt", "revisions", "thumbnail" ),      
                //"taxonomies" => array( "category" )        
        );
    register_post_type( "online_poradna", $args );

    }

    // ACF
    
    if( function_exists('register_field_group') ):

    register_field_group(array (
        'key' => 'group_54abe729892ce',
        'title' => 'Poradňa',
        'fields' => array (
            array (
                'key' => 'field_53244a3009289',
                'label' => 'Odpoveď',
                'name' => 'expert_answer',
                'prefix' => '',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 0,
                'tabs' => 'all',
            ),
            array (
                'key' => 'field_5378b3224726d',
                'label' => 'Odpovedajúci',
                'name' => 'expert',
                'prefix' => '',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array (
                    'MUDr. Mikuláš Meszároš' => 'MUDr. Mikuláš Meszároš',
                ),
                'default_value' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'placeholder' => '',
                'disabled' => 0,
                'readonly' => 0,
            ),
            array (
                'key' => 'field_5378c002ee166',
                'label' => 'Pozdrav',
                'name' => 'salute',
                'prefix' => '',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array (
                    'Dobrý deň' => 'Dobrý deň',
                ),
                'default_value' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'placeholder' => '',
                'disabled' => 0,
                'readonly' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'online_poradna',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'acf_after_title',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
    ));

    endif;

 // Frontend
 
    add_action('headway_after_entry_content', 'xtw_answer', 10, 2);
    
    function xtw_answer() {
        
        // Overime ci je nainstalovany PLUGIN ACF
        if(function_exists('get_field') && get_field('expert_answer')){

            printf('<div class="%s %s talk-bubble tri-right border btm-right-in">','cell1', 'cell');
            printf('<div class="talktext"><span class="salute">%s</span> %s</div>', get_field('salute'), get_field('expert_answer'));

            printf('</div>');
            echo '<div class="expert-person">'.get_field('expert');
                edit_post_link('edit', ' &nbsp; &nbsp;');
            echo '</div>';
        }

    }
}

/**
*
*   Recipes
*
**/

if(isset($options['opt-cpt-recipes']) && $options['opt-cpt-recipes']){

add_action( 'init', 'cptui_register_recipes' );
function cptui_register_recipes() {
    
    $labels = array(
        "name" => "Recepty",
        "singular_name" => "Recept",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        'menu_position' => 10,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "recipes", "with_front" => true ),
        "query_var" => true,
                        "menu_icon" => "dashicons-editor-ol",

                        "supports" => array( "title", "editor", "excerpt", "revisions", "thumbnail" )
                        );
    register_post_type( "recipes", $args );

// End of cptui_register_my_cpts()
}
 
}

/**
*
*   Myths
*
**/

if(isset($options['opt-cpt-myths']) && $options['opt-cpt-myths']){

add_action( 'init', 'cptui_register_myth_truth' );

function cptui_register_myth_truth() {
    

    $labels = array(
        "name" => "Mýty a Pravdy",
        "singular_name" => "Mýty a Pravdy",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "myth_truth", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "excerpt", "thumbnail" ),       
    );
    register_post_type( "myth_truth", $args );

// End of cptui_register_my_cpts()
}
}

/**
*
*   FAQs
*
**/

if(isset($options['opt-cpt-faqs']) && $options['opt-cpt-faqs']){

add_action( 'init', 'cptui_register_faqs' );

function cptui_register_faqs() {

    $labels = array(
        "name" => "FAQ",
        "singular_name" => "FAQ",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "faqs", "with_front" => true ),
        "query_var" => true,
                
        "supports" => array( "title", "editor", "excerpt", "thumbnail" ),       
    );
    register_post_type( "faqs", $args );

// End of cptui_register_my_cpts()
}
}

/**
*
*   Diarys / Dennik
*
**/

if(isset($options['opt-cpt-diarys']) && $options['opt-cpt-diarys'] ){

    add_action( 'init', 'cptui_register_diarys' );

    function cptui_register_diarys() {

        $labels = array(
            "name" => "Denník",
            "singular_name" => "Denník",
        );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => true,
            "show_in_menu" => true,
            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( 

                               "slug" => "chudnutie-s-dietou", 
                               "with_front" => true
                        ),
            
            "query_var" => true,

            "menu_icon" => "dashicons-book",

            "supports" => array( "title", 
                                "editor", 
                                "excerpt", 
                                "revisions", 
                                "thumbnail" 
                                            )
            );
        register_post_type( "diary", $args );

    // End of cptui_register_my_cpts()
    }

}


/**
*
*   Popup
*
**/

if(isset($options['opt-cpt-popup']) && $options['opt-cpt-popup'] ){

    add_action( 'init', 'cptui_register_popup' );

    function cptui_register_popup() {

        $labels = array(
            "name" => "Popup",
            "singular_name" => "Popup",
            );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => false,
            // "show_in_menu" => true,
                                // 'show_in_menu' => 'edit.php',

            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "popup", "with_front" => true ),
            "query_var" => true,
                            "menu_icon" => "dashicons-book",
                            "supports" => array( "title", "editor", "excerpt", "revisions", "thumbnail" )
                            );
        register_post_type( "popup", $args );

    // End of cptui_register_my_cpts()
    }

    add_action( 'init', 'cptui_register_popup_theme' );

    function cptui_register_popup_theme() {

        $labels = array(
            "name" => "Themes",
            "singular_name" => "Theme",
            );

        $args = array(
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "show_ui" => true,
            "has_archive" => false,
            // "show_in_menu" => true,

            "show_in_menu" => 'edit.php?post_type=popup',

            'menu_position' => 10,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "popup", "with_front" => true ),
            "query_var" => true,
                            "menu_icon" => "dashicons-book",
                            "supports" => array( "title", "revisions" )
                            );
        register_post_type( "popup_theme", $args );

    // End of cptui_register_my_cpts()
    }
}

?>