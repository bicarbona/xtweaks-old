<?php 

/** 

ScrollUp Header

**/

    function xtw_okay_nav_script() {
        global $options;
        
        // Register scripts
            wp_register_script('okay-nav', plugins_url( '../lib/js/jquery.okayNav-min.js', __FILE__ ),false,'1.0.0', true);

            wp_enqueue_script( 'okay-nav' );

        // Localize script with options
    }

    // if($options['opt-scrollup-header']){
        add_action( 'wp_enqueue_scripts', 'xtw_okay_nav_script' );
    // }

 ?>