<?php 

/**
*
*   LESS Enque Main Frontend Style
*
**/

if (class_exists('WPLessPlugin')){
	function theme_enqueue_main_style() {
	if ( ! is_admin() )
		
        wp_enqueue_style( 'less-main', plugins_url( '../less/main.less', __FILE__ ) );

	}
}

if (class_exists('WPLessPlugin')){
 
    if ($options['opt-main-less']){
    	add_action('init', 'theme_enqueue_main_style');
    }
}

?>