<?php 

/**
*
*   Load text domain for translation
*
**/

load_theme_textdomain('headway');
load_theme_textdomain('headway', get_stylesheet_directory() . '/languages');


/** 
*
*   Headway Fix styling
*
**/

if( $options['remove-hw-content-styling']){

    function remove_hw_content_styling() {
        // remove_theme_support('headway-block-basics-css');
        remove_theme_support('headway-content-styling-css');
    }

    add_action('headway_setup_child_theme', 'remove_hw_content_styling');

}


/**
*
*   Hide Closed Comments notification
*
**/

function change_search_text() {
return '';}
add_filter('headway_comments_closed', 'change_search_text');


/**
*
*   Category Archive Title
*
**/

function my_category_title() { return 'čítate: '.single_cat_title('', false); }
add_filter('headway_category_title', 'my_category_title');


/**
*
*   Headway TinyMce
*
**/

remove_filter( 'mce_buttons_2', array('HeadwayAdmin', 'tiny_mce_buttons') );
remove_filter('tiny_mce_before_init', array( 'HeadwayAdmin', 'tiny_mce_formats') );

remove_action('headway_visual_editor_menu_links', array('HeadwayVisualEditorDisplay', 'menu_links'));
remove_action('headway_visual_editor_menu_links', array('HeadwayVisualEditorDisplay', 'add_default_panel_link'), 20);


/**
	Wrap In whitewrap

	blok ma ID #after-whitewrap
	
	Aby sa dal nastylovat backgound..
	padding atd pod vsetkymi blokmi naraz

**/

function whitewrap_open() {
    
    echo '<div id="after-whitewrap" class="wrapper-fixed">';

}

function whitewrap_close() { 
    
    echo '</div>';

}
	
	// add_action('headway_whitewrap_open','whitewrap_open');
	// add_action('headway_whitewrap_close','whitewrap_close');


/**
* 
*   Headway Custom Fonts
*
*   25.3.16
*   nacitavam font z vlasneho zdroja
*   pozor na crossdomain (a specialne localhost crossdomain)
*
**/

class GoogleFonts {
    public static function add_fonts() {
        $fonts = array(
            // 'http://fonts.googleapis.com/css?family=Chango',
           'http://dev/fonts/my-font.css',
        );

        $fonts_css = '';
        foreach ($fonts as $key => $font_url) {
            $fonts_css .= file_get_contents($font_url);
        }   
        return $fonts_css;
    }

    public static function register_fonts() {
        $roboto_condensed = array(
            'id' => 'encode_sans_condensed',
            'stack' => 'encode_sans_condensed',
            'name' => 'encode_sans condensed'
        );

        /*  change variable name to font name */
        $wajko = array(
        
         //set id for the font using font name lowercase 
        'id' => 'wajko',
        
        /* stack is the "font-family:" value that google gives you in Step 4. 
        "Integrate the fonts into your CSS:"" */
        'stack' => 'wajko',
        
        /* the name = the text that displays in the headway visual editor */
        'name' => 'wajko'
        );
        /* remember to change variable to font name */
        
        HeadwayFonts::register_font($roboto_condensed);
        // HeadwayFonts::register_font($chango);

    }

    public static function add_font_css($general_css_fragments) {
        $general_css_fragments[] = array('GoogleFonts', 'add_fonts');
        return $general_css_fragments;
    }
}

 if(class_exists('HeadwayFonts')){
    add_action('after_setup_theme', array('GoogleFonts', 'register_fonts'));
    add_action('headway_general_css', array('GoogleFonts', 'add_font_css')); 
 }

?>