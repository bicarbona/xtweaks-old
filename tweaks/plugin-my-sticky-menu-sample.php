<?php 

/** 
*
* Sticky Menu
*
**/

    function xtw_stickymenu_script() {
        global $options;

        // needed for update 1.7 => 1.8 ... will be removed in the future ()
        if (isset($options['mysticky_active_on_height_home'])){
                 //do nothing
            } else {
            $options['mysticky_active_on_height_home'] = $options['mysticky_active_on_height'];
        }
        
        // if set to 0 => display default active_on_height
        if  ($options['mysticky_active_on_height_home'] == 0 ){
            $options['mysticky_active_on_height_home'] = $options['mysticky_active_on_height'];
        }
        
        if ( is_home() ) {
            $options['mysticky_active_on_height'] = $options['mysticky_active_on_height_home'];
        }
        
        // Register scripts
            wp_register_script('mystickymenu', plugins_url( '../lib/js/mystickymenu.js', __FILE__ ),false,'1.0.0', true);

            wp_enqueue_script( 'mystickymenu' );

            // Localize mystickymenu.js script with myStickymenu options
            $mysticky_translation_array = array( 
                'mysticky_string' => $options['opt-sticky-menu-id-class'] ,
                'mysticky_active_on_height_string' => $options['opt-sticky-active-on-height'],
                'mysticky_disable_at_width_string' => $options['opt-sticky-disable-small-screen']
            );
            wp_localize_script( 'mystickymenu', 'mysticky_name', $mysticky_translation_array );
    }

    if($options['opt-sticky-menu']){
        add_action( 'wp_enqueue_scripts', 'xtw_stickymenu_script' );
        add_filter( 'the_content_more_link', 'xtw_mysticky_remove_more_jump_link' );
    }

// Remove default option for more link that jumps at the midle of page and its covered by menu

    function xtw_mysticky_remove_more_jump_link($link) { 
        $offset = strpos($link, '#more-');
        if ($offset) {
            $end = strpos($link, '"',$offset);
        }
        if ($end) {
            $link = substr_replace($link, '', $offset, $end-$offset);
        }
        return $link;
    }


// Create style from options

    function xtw_mysticky_inline_css() {
        global $options;
    
   // echo '#wpadminbar { position: absolute !important; top: 0px !important;}';

    // Ak nieje definovane vlastne CSS
    if ( $options['opt-sticky-custom-style'] == "" )  {
    
    $default_sticky_css = '.myfixed { margin:0 auto!important; float:none!important; border:0px!important; background:none!important; max-width:100%!important; }';
    }

    $default_sticky_css = $options ['opt-sticky-custom-style'] ; 
    #mysticky-nav { width:100%!important;  position: static;';
    
    if (isset($options['myfixed_fade'])){
    
    $fade = 'top: -100px;';
    
        //
        }
        if  (isset($options['myfixed_opacity']) && $options['myfixed_opacity'] == 100 ){
        
        }
    
        //
        if  (isset($options['myfixed_opacity']) && $options['myfixed_opacity'] < 100 ){

        }
    
        // Ak je povolene vypnutie fixnuteho menu pri nejakej sirke
        if  (isset($options['myfixed_disable_small_screen']) && $options['myfixed_disable_small_screen'] > 0 ){

        }
    
    }
    add_action( 'wp_head', 'xtw_mysticky_inline_css' );

 ?>