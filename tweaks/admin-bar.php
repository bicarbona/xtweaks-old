<?php 

/**
*
*   Admin Bar Menu
*
**/

function xtw_admin_bar_remove() {
    global $wp_admin_bar, $current_user;

    /* Remove their stuff */
    $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
    // $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
    $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
    $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
    $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
    $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
    // $wp_admin_bar->remove_menu('site-name');     // Remove the site name menu
    // $wp_admin_bar->remove_menu('view-site');     // Remove the view site link
    $wp_admin_bar->remove_menu('comments');         // Remove the comments link
    // $wp_admin_bar->remove_menu('new-content');   // Remove the content link
    $wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
    $wp_admin_bar->remove_menu('my-account');       // Remove the user details tab
    $wp_admin_bar->remove_menu('search');
    // $wp_admin_bar->remove_menu('edit');         // Remove edit link
    $wp_admin_bar->remove_menu('updates');          // Remove the updates link
    $wp_admin_bar->remove_menu( 'customize' );  

    // Iba pre admina
    if ($current_user->ID != 1) {
        $wp_admin_bar->remove_menu('updates');       // Remove the updates link
        $wp_admin_bar->remove_menu('w3tc');          // If you use w3 total cache remove the performance link
    }
    
    /* Add custom menu */

    // $wp_admin_bar->add_menu( array(
    //     'id' => 'reports',  
    //     'title' => __( 'Reports'),  
    //     'href' => __('xyz.html'),
    // ));   

    // Add logout
    $wp_admin_bar->add_menu( array(
        'id'    => 'wp-custom-logout',
        'title' => 'Logout',
        // 'parent'=> 'top-secondary', // samostatne menu
        'parent'=> 'site-name',
        'href'  => wp_logout_url(),
        'meta'  => array( 'class' => 'my-toolbar-page' ),
        // 'meta' => array('target' => '_blank')
    ));

    // Adds Customizer
    $wp_admin_bar->add_node( array(
        'parent' => 'site-name',
        'id'     => 'customizer', 
        'title'  => 'Customizer',
        'href'   => esc_url( admin_url( 'customize.php' ) ),
        'meta'   => false       
    ));

    // Adds Plugins
    $wp_admin_bar->add_node( array(
            'parent' => 'site-name',
            'id'     => 'plugins', 
            'title'  => 'Plugins',
            'href'   => esc_url( admin_url( 'plugins.php' ) ),
            'meta'   => false       
    ));

    // Adds All Pages
    $wp_admin_bar->add_node( array(
        'parent' => 'site-name',
        'id'     => 'all-pages', 
        'title'  => 'All Pages',
        'href'   => esc_url( admin_url( 'edit.php?post_type=page' ) ),
        'meta'   => false       
    ));  
            
    // Adds All Posts
    $wp_admin_bar->add_node( array(
        'parent' => 'site-name',
        'id'     => 'all-posts', 
        'title'  => 'All Posts',
        'href'   => esc_url( admin_url( 'edit.php' ) ),
        'meta'   => false       
    ));
           
}

add_action('wp_before_admin_bar_render', 'xtw_admin_bar_remove', 0);


/**
*
*   Admin BarShorten Sitename
* 
**/

function xtw_add_toolbar_items($admin_bar){

    $admin_bar->add_menu( array(
        'id'    => 'site-name',
        'title' => is_admin() ? ('Page') : ( 'Admin' ),
        'href'  => is_admin() ? home_url( '/' ) : admin_url(),
    ));
}

add_action('admin_bar_menu', 'xtw_add_toolbar_items', 100);


/**
*
*   Admin Bar Logout Link
*
**/

function xtw_custom_logout_link() {
    global $wp_admin_bar;
    $wp_admin_bar->add_menu( array(
        'id'    => 'wp-custom-logout',
        'title' => 'Logout',
        'parent'=> 'top-secondary', // root-default / top-secondary (ak ma byt napravo)
        'href'  => wp_logout_url()
    ) );
    $wp_admin_bar->remove_menu('my-account');
}

add_action( 'wp_before_admin_bar_render', 'xtw_custom_logout_link' );


/**
*
*   Admin Bar Replace Howdy 
*
**/

function xtw_replace_howdy( $wp_admin_bar ) {
    $my_account=$wp_admin_bar->get_node('my-account');
    $newtitle = str_replace( 'Howdy,', '', $my_account->title );
    $wp_admin_bar->add_node( 
            array(
                'id' => 'my-account',
                'title' => $newtitle,
            ) 
    );
}
add_filter( 'admin_bar_menu', 'xtw_replace_howdy',25 );

?>