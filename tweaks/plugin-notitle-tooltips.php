<?php

/** 
*
* No title tooltips
*
**/

    function xtw_add_no_tttips() {
        wp_enqueue_script('notttips', plugins_url( '../lib/js/notttips.js', __FILE__ ), array( 'jquery' ));
    }

    if($options['opt-no-tttips']){
        add_action('wp_enqueue_scripts', 'xtw_add_no_tttips');
    }

?>