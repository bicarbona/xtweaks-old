<?php 

/**
*
*   Highlight Search Term
*   Note - Finalize
*
**/

function xtw_search_excerpt_highlight() {
    $excerpt = get_the_excerpt();
    $keys = implode('|', explode(' ', get_search_query()));
    $excerpt = preg_replace('/(' . $keys .')/iu', '<mark>\0</mark>', $excerpt);
    echo '<p>' . $excerpt . '</p>';
}

function xtw_search_title_highlight() {
    $title = get_the_title();
    $keys = implode('|', explode(' ', get_search_query()));
    $title = preg_replace('/(' . $keys .')/iu', '<mark>\0</mark>', $title);
    echo $title;
}

//  In your loop or search.php file call  
//  search_title_highlight();  instead of <?php the_title(); 
//  and use <?php search_excerpt_highlight(); 
//  instead of <?php the_excerpt(); 

// add_filter('the_excerpt', 'highlight_search_term');
// add_filter('the_title', 'highlight_search_term');


/**
*
*   Exclude Pages from Search
*
**/

function xtw_exclude_pages_from_search() {
    global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}

add_action('init', 'xtw_exclude_pages_from_search');

?>