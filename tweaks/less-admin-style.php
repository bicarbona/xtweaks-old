<?php 

/**
*
* enqueuing our styles correctly
*
**/

if ( INC_LESS == TRUE ) {
    
    if(!function_exists('xtw_admin_style')){
       
        function xtw_admin_style() {

            wp_register_style( 'admin_tweaks_style', plugins_url( '../less/admin-tweaks.less', __FILE__ ) );
            wp_enqueue_style( 'admin_tweaks_style' );
        }

        if($options['opt-admin-style']){
            add_action( 'admin_enqueue_scripts', 'xtw_admin_style' );
        }
    }
}

?>