<?php 

if (is_admin()) {

/**
*
* Remove metaboxes
* https://codex.wordpress.org/Function_Reference/remove_meta_box
*
**/

function xtw_remove_post_meta_boxes(){
    // if ( ! current_user_can( 'administrator' ) ) { // Only run if the user is not an admin

    global $options;

/**
*
* Headway Metaboxes
*
**/

        // Remove Headway Shared layout Meta Box
        if ($options['opt-post-headway-admin-meta-box-template'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-template', 'post', 'side' );
        }

        if ($options['opt-page-headway-admin-meta-box-template'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-template', 'page', 'side' );
        }

        // Title Control
        if ($options['opt-post-headway-admin-meta-box-alternate-title'] == FALSE ) {
            remove_meta_box( 'headway-admin-meta-box-alternate-title', 'post', 'side' );
        }

        if ($options['opt-page-headway-admin-meta-box-alternate-title'] == FALSE ) {
            remove_meta_box( 'headway-admin-meta-box-alternate-title', 'page', 'side' );
        }

        // Featured Image Position
        if ($options['opt-post-headway-admin-meta-box-post-thumbnail'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-post-thumbnail', 'post', 'side' );
        }

        if ($options['opt-page-headway-admin-meta-box-post-thumbnail'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-post-thumbnail', 'page', 'side' );
        }
        
        // Display (Custom CSS)
        if ($options['opt-post-headway-admin-meta-box-display'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-display', 'post', '' );
        }

        if ($options['opt-page-headway-admin-meta-box-display'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-display', 'page', '' );
            // remove_meta_box( 'headway-admin-meta-box-display', 'page', 'normal' );
        }

        // SEO headway-seo
        if ($options['opt-post-headway-admin-meta-box-seo'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-seo', 'post', '' );
        }
        
        if ($options['opt-page-headway-admin-meta-box-seo'] == FALSE) {
            remove_meta_box( 'headway-admin-meta-box-seo', 'page', '' );
        }
/**
* 
* Wordpress Metaboxes
*
**/

        // Custom fields metabox
        if ($options['opt-post-postcustom'] == FALSE) {
            remove_meta_box('postcustom', 'post', 'normal');
        }
         
        if ($options['opt-page-postcustom'] == FALSE) {
            remove_meta_box('postcustom', 'page', 'normal');
        }

        // Comments Status
        if ($options['opt-post-commentstatusdiv'] == FALSE) {
            remove_meta_box('commentstatusdiv', 'post', 'normal');
        }


        // Comments
        if ($options['opt-post-commentsdiv'] == FALSE) {
            remove_meta_box('commentsdiv', 'post', 'normal');
        }

        if ($options['opt-page-commentsdiv'] == FALSE) {
            // remove_meta_box('commentsdiv', 'page', 'normal');
        }

        // Trackbacks
        if ($options['opt-post-trackbacksdiv'] == FALSE) {
            remove_meta_box('trackbacksdiv', 'post', 'normal');
        }

        if ($options['opt-post-trackbacksdiv'] == FALSE) {
            remove_meta_box('trackbacksdiv', 'page', 'normal');
        }

        // Displays post revision links
        if ($options['opt-post-revisionsdiv'] == FALSE) {
            remove_meta_box('revisionsdiv', 'post', 'normal');
        }

        if ($options['opt-page-revisionsdiv'] == FALSE) {
            remove_meta_box('revisionsdiv', 'page', 'normal');
        }

        // Author
        if ($options['opt-post-authordiv'] == FALSE) {
            remove_meta_box('authordiv', 'post', 'normal');
        }

        if ($options['opt-page-authordiv'] == FALSE) {
            remove_meta_box('authordiv', 'page', 'normal');
        }    

        // The “Publish” box that allows you to set several things
        // remove_meta_box('submitdiv', 'post', 'normal');

        // Category
        if ($options['opt-post-categorydiv'] == FALSE) {
            remove_meta_box('categorydiv', 'post', 'normal');
        }
        
        // Post image
        if ($options['opt-post-postimagediv'] == FALSE) {
            remove_meta_box('postimagediv', 'post', 'side');
        }

        if ($options['opt-page-postimagediv'] == FALSE) {
            remove_meta_box('postimagediv', 'page', 'side');
        }

        // The “Attributes” box for choosing a page parent and template
        // remove_meta_box('pageparentdiv', 'page', 'side');

        //  Allows the user to select a post format
        if ($options['opt-post-formatdiv'] == FALSE) {
        remove_meta_box('formatdiv', 'post', 'normal');
        }

        // Slug
        if ($options['opt-post-slugdiv'] == FALSE) {
            remove_meta_box('slugdiv', 'post', 'normal');
        }

        if ($options['opt-page-slugdiv'] == FALSE) {
            remove_meta_box('slugdiv', 'page', 'normal');
        }

        // Tags - Displays the post tags meta box for selecting tags
        if ($options['opt-post-tagsdiv-post_tag'] == FALSE) {
            remove_meta_box('tagsdiv-post_tag', 'post', 'normal');
        }

        // Creates a textarea for writing a custom excerpt

        // tagsdiv-{$taxonomy} – Lets you choose terms for a non-hierarchical taxonomy (use the taxonomy name).

        // {$taxonomy}div – Allows you to set terms for a hierarchical taxonomy (use the taxonomy name).
    // }

}

add_action( 'do_meta_boxes', 'xtw_remove_post_meta_boxes' );



/**
*   
*   @since 6.6.8
*   Adds support for Headway meta boxes to all post types
*   @link http://headwaythemes.com/docs/article/enable-the-headway-meta-boxes-in-your-custom-post-types/
*
**/

function xtw_cpt_metabox_support() {
    $post_types = get_post_types();
    foreach ( $post_types as $post_type ) {

        if (($post_type !== "post") || ($post_type !== "page")) {

        add_post_type_support( $post_type, 'headway-admin-meta-box-template' );
        // add_post_type_support( $post_type, 'headway-admin-meta-box-alternate-title' );
        // add_post_type_support( $post_type, 'headway-admin-meta-box-post-thumbnail' );
        // add_post_type_support( $post_type, 'headway-seo');
        add_post_type_support( $post_type, 'headway-admin-meta-box-display' );
    }
    }
}
add_action('admin_init', 'xtw_cpt_metabox_support');


} // is admin
?>