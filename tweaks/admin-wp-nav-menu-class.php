<?php 

if (is_admin()) {

/**
* 
*   Adds a custom dropdown option to wp Nav Menus
*   where the user can select a predefined css class for each menu item
*   wp-admin/nav-menus.php
*
**/

function xtw_menu_item_class_select(){
    global $pagenow;
    if ($pagenow == "nav-menus.php"){
    ?>
    
    <script>
    jQuery(document).ready(function(){
        function create_dd(v){
            //create dropdown
            var dd = jQuery('<select class="my_class"></select>');
            
            //create dropdown options
            
            //array with the options you want
            var classes = ["", "highlight-menu", "class1", "class3"];
            jQuery.each(classes, function(i,val) {
                if (v == val){
                    dd.append('<option value="'+val+'" selected="selected">'+val+'</option>');
                }else{
                    dd.append('<option value="'+val+'">'+val+'</option>');
                }
            });
            return dd;
        }

        jQuery(".edit-menu-item-classes").each(function() {
            //add dropdown
            var t = create_dd(jQuery(this).val());
            jQuery(this).before(t);
            //hide all inputs
           // jQuery(this).css("display","none");

        });
        
        //update input on selection
        jQuery(".my_class").bind("change", function() {
            var v = jQuery(this).val();
            var inp = jQuery(this).next();
            inp.attr("value",v);
        });
    });
    </script>
    <?php
    }
}

add_action('admin_footer','xtw_menu_item_class_select');
}
?>