<?php 

if (is_admin()) {
/**
*
*   Removes comments from admin START
*
**/

// Removes comments from admin 
add_action( 'admin_menu', 'xtw_remove_admin_menus' );
function xtw_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}

// Removes from post and pages
add_action('init', 'xtw_remove_comment_support', 100);

function xtw_remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}


/**
*
*   Disable HTML in Coomments
*
**/

if(!function_exists('xtw_disable_html_in_comments')){
    function xtw_disable_html_in_comments(){
        add_filter( 'pre_comment_content','wp_specialchars' );
    }

    if($options['opt-disable-html-in-commets']){
        xtw_disable_html_in_comments();
    }

}


/**
 * Remove pings to your own blog
 * @param  [type] $links [description]
 * @return [type]         [description]
*/

if(!function_exists('xtw_no_self_ping')){

    function xtw_no_self_ping( &$links ) {
        $home = get_option( 'home' );
        foreach ( $links as $l => $link )
            if ( 0 === strpos( $link, $home ) )
                unset($links[$l]);
    }

    if($options['opt-disable-self-ping']){
        add_action( 'pre_ping', 'xtw_no_self_ping' );
    }
}


/**
*
*   Custom Error Log
*
**/

if( !function_exists('_log') ){

  function _log( $message ) {
    
    if( WP_DEBUG === true ){
  
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
  
    }
  
  }

}}


?>