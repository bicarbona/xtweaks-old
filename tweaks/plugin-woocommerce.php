<?php

/**
*
*   WoooCommerce
*
**/

// Pridanie poctu stlpcov do body class
// Musi byt cez action WP inac nenacita
add_action( 'wp', 'xtw_shop_init' );

function xtw_shop_init(){
    if ( class_exists( 'WooCommerce' ) ) {

        if ( is_shop() || is_product_category() || is_product_tag()) {
            add_filter( 'body_class', 'xtw_woocommerce_pac_columns'  );
        //  add_filter( 'loop_shop_columns', array( $this, 'woocommerce_pac_products_row' ) );
        }
    }
}

/**
*
*   Number of maximum columns for shop archives - Add class to <body>
*
**/

// Product columns
function xtw_woocommerce_pac_columns( $classes ) {
    global $options;
    $columns    =  $options['opt-woo-product-columns'];
    $classes[]  = 'product-columns-' . $columns;
    return $classes;
}


/**
*
*    Number of maximum columns for shop ➜ Archive loop / hooks woo
*
**/

if(!function_exists('xtw_loop_shop_columns')){
    /*
     * Return a new number of maximum columns for shop archives
     * @param int Original value
     * @return int New number of columns
     */
    function xtw_loop_shop_columns( $number_columns ) {
        global $options;
        return $options['opt-woo-product-columns'];
    }

    add_filter( 'loop_shop_columns', 'xtw_loop_shop_columns', 1, 10 );
}


/**
*
*    Display products per page ➜ Archive
*
**/
 
add_filter( 'loop_shop_per_page', function ( $cols ) {

    global $options;
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.
    return $options['opt-woo-products-per-page'];
}, 20 );


/**
*
*    Display products sorting ➜ Archive loop
*
**/

if(!$options['opt-woo-products-sorting']){
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
}


/**
*
*    Display products image ➜ Single page
*
**/

if(!$options['opt-woo-product-image-single']){

    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
    remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );

}

/**
*
*   Display products image ➜ Archive loop
*
**/

if(!$options['opt-woo-product-image-loop']){

    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

}


/**
*
*   Add to cart ➜ Archive loop
*
**/

    if(!$options['opt-woo-loop-add-to-cart']){
        remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10); 
    }


/**
*
*    Add to cart ➜ Single page
*
**/

    if(!$options['opt-woo-single-add-to-cart']){
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
    }


/**
*
*    Review ➜ Single Page
*
**/

    if(!$options['opt-woo-single-review']){
        add_filter( 'woocommerce_product_tabs', 'xtw_woo_remove_reviews_tab', 98 );
    }

    if(!function_exists('xtw_woo_catalog_orderby')){
        function xtw_woo_remove_reviews_tab($tabs) {
            unset($tabs['reviews']);
            return $tabs;
        }
    }


/**
*
*   FUTURE - add options
*   Modify the default WooCommerce orderby dropdown
*
**/

// Options: menu_order, popularity, rating, date, price, price-desc
if(!function_exists('xtw_woo_catalog_orderby')){

    function xtw_woo_catalog_orderby( $orderby ) {
        // unset($orderby["price"]);
        // unset($orderby["price-desc"]);
        // unset($orderby["menu_order"]);
        // unset($orderby["rating"]);
        // unset($orderby["date"]);
        // unset($orderby["popularity"]);
        
        return $orderby;
    }
    add_filter( "woocommerce_catalog_orderby", "xtw_woo_catalog_orderby", 20 );
}


/**
*
*   Sale
*
**/

if( !$options['opt-woo-sale-loop'] ){
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
}

if( !$options['opt-woo-sale-single'] ){
    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
}


/**
*
*   Price ➜ Single Page
*
**/

if(!$options['opt-woo-product-price-single']){

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

}


/**
*
*   Price ➜ Archive Loop
*
**/

if(!$options['opt-woo-product-price-loop']){
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
}


/**
*
*   Rating ➜ Archive Loop
*
**/

if(!$options['opt-woo-product-rating-loop']){
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
}


/**
*
*   Rating ➜ Single Page
*
**/

if(!$options['opt-woo-product-rating-single']){

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

}


/**
*
*   Product result Count ➜ Archive loop
*
**/

if(!$options['opt-woo-product-result-count-loop']){

    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

}

// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

/**
*
*   Product result Count ➜ Single Page
*
**/

if(!$options['opt-woo-product-sumary-single']){

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

}


/**
*
*   Display New Badge
*
**/

if (!function_exists('xtw_woo_new_badge')) {
    
    function xtw_woo_new_badge() {
        global $options;

        $postdate       = get_the_time( 'Y-m-d' );          // Post date
        $postdatestamp  = strtotime( $postdate );           // Timestamped post date

        $newness        = $options['opt-woo-new-badge-days'];   // Newness in days as defined by option

        if ( ( time() - ( 60 * 60 * 24 * $newness ) ) < $postdatestamp ) { // If the product was published within the newness time frame display the new badge
            echo '<p class="wc-new-badge"><span>' . $options['opt-woo-new-badge-text'] . '</span></p>';
        }
    }

    if($options['opt-woo-new-badge']){
        add_action( 'woocommerce_before_shop_loop_item_title', 'xtw_woo_new_badge' , 30 );
    }

}


/**
*
*   Catalog mode - disable Add to cart / Price / Rating ... etc
*
**/

if(!function_exists('xtw_woo_catalog')){
    global $options;
    
    function xtw_woo_catalog() {

    /**
    LOOP
    */ 
    //  remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10, 2);

    /**
    SINGLE
    */ 

    // Remove add to cart button from the product details page
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
    remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
    remove_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );

    //disabled actions (add to cart, checkout and pay)
    remove_action( 'init', 'woocommerce_add_to_cart_action', 10);
    remove_action( 'init', 'woocommerce_checkout_action', 10 );
    remove_action( 'init', 'woocommerce_pay_action', 10 );

    }

    if($options['opt-woo-catalog-mode']){

        xtw_woo_catalog();

    }
}


/**
*
*	Zrusim Default Tabs ➜ Single Page
*
**/

add_filter( 'woocommerce_product_tabs', 'xtw_woo_remove_product_tabs', 98 );
 
function xtw_woo_remove_product_tabs( $tabs ) {
 
    unset( $tabs['description'] );      	// Remove the description tab
  //  unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab
 
    return $tabs;
}


/**
*
*	Druhy nadpis (anglicky nazov) a externy link na eshop ➜ Archive Loop
*   Note - u Single page je potrebne prepisat template single-product/title.php
*
**/

    // druhy nadpis - anglicky nazov
    function woo_subtitle() {
        $subtitle = get_field('alt_title');
        if($subtitle){
             echo '<h3>'.$subtitle.'</h3>';
        }
       
    }

   // global $options;

    if ( function_exists( 'get_field' ) ) {
        add_action( 'woocommerce_after_shop_loop_item_title', 'woo_subtitle', 0 );

        if ($options['opt-woo-external-link']){
            add_action( 'woocommerce_after_shop_loop_item', 'woo_external_eshop', 15 );
        }
    }


/**
*
*    Remove Related products ➜ Single Page
*
**/

    function xtw_remove_related_products( $args ) {
        return array();
    }

    if(!$options['opt-woo-related-products']){
        add_filter('woocommerce_related_products_args','xtw_remove_related_products', 10); 
    }


/**
*
*   External link
*
**/

    // Funkcia osetri URL ak chyba http://
    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }

    // link na externy eshop
    if(!function_exists('woo_external_eshop')){
        function woo_external_eshop() {
            //  echo  '[  <a href = "' . get_edit_post_link ( ) . '"> Edit </a> ]' ;
            edit_post_link( );

            $external_link = get_field('external_link');
            $title = get_the_title();

            if($external_link){
                echo '<a href="'. addhttp($external_link) .'" target="_blank" title="'.$title.'" class="button">Kúpiť online</a>';
            }
        }
    }


/**
*
*	Premiestnenie tabov ➜ Single Page
*
**/

    //Removes tabs from their original location 
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

    // Inserts tabs under the main right product content 
    add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60 );
    add_filter( 'woocommerce_product_tabs', 'xtw_theme_product_tabs' );


/**
*
*	Vytvorenie vlastnych tabov ➜ Single Page
*
**/

if($options['opt-woo-custom-tabs']){

    function xtw_theme_product_tabs( $tabs ) {
        global $options;

      // ensure ACF is available
      if ( !function_exists( 'get_field' ) )
        return;

      $content = trim( get_field( 'product_usage' ) );
      if ( !empty( $content ) ) {
        $tabs[] = array(
          'title' => $options['opt-woo-custom-tab1'],
          'priority' => 20,
          'callback' => 'custom_woo_tab1' // zavolam funkciu
        );
    	
    	}
        $tabs[] = array(
          'title' => $options['opt-woo-custom-tab2'],
          'priority' => 15,
          'callback' => 'custom_woo_tab2' // zavolam funkciu
        );
    	
        $tabs[] = array(
          'title' => $options['opt-woo-custom-tab3'],
          'priority' => 10,
          'callback' => 'custom_woo_tab3' // zavolam funkciu
        );
      
      return $tabs;
    }


/**
*
*	Obsah tabov ➜ Single Page
*
**/

    function custom_woo_tab1() {
        global $options;
    	echo '<h2>'.$options['opt-woo-custom-tab1'].'</h2>';
    	echo get_field( 'product_usage' );
    }

    function custom_woo_tab2() {
    	global $woocommerce, $post, $product;
    	echo '<h2>'.$options['opt-woo-custom-tab1'].'</h2>';
    	//	echo get_terms_with_desc('','ucinne_latky','<ul>','', '</ul>');
    	//	echo $product->list_attributes();
    }

    function custom_woo_tab3() {
    	echo '<h2>'.$options['opt-woo-custom-tab1'].'</h2>';
    	the_content();
    }


/**
*
*	Vlastnosti ➜ Single Page
*
**/

    function get_terms_with_desc( $id = 0 , $taxonomy = 'ucinne_latky', $before = '', $sep = '', $after = '' ){
        $terms = get_the_terms( $id, $taxonomy );
    	
        if ( is_wp_error( $terms ) )
                return $terms;
        if ( empty( $terms ) )
                return false;
        foreach ( $terms as $term ) {
                $link = get_term_link( $term, $taxonomy );
                if ( is_wp_error( $link ) )
                        return $link;
                $term_links[] = '<li><a href="' . esc_url( $link ) . '" rel="tag" title="'.esc_attr( $term->description ).'">' . $term->name . '</a> '. $term->description.'</li>';
        }
        $term_links = apply_filters( "term_links-$taxonomy", $term_links );
        return $before . join( $sep, $term_links ) . $after;
    }

}


// add_filter( 'woocommerce_product_tabs', 'woo_custom_description_tab', 98 );
// function woo_custom_description_tab( $tabs ) {
// 
// 	$tabs['description']['callback'] = 'woo_custom_description_tab_content';	// Custom description callback
// 
// 	return $tabs;
// }
//  
// function woo_custom_description_tab_content() {
// 	echo '<h2>Odporúčaný</h2>';
// 	the_content();
// }


    // remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
    // remove_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );



   // wp_dequeue_style( 'woocommerce_frontend_styles' );
   // wp_dequeue_script( 'wc-single-product' );


// Products per page
// function wc_pac_fire_customisations(){

//     if(!function_exists('xtw_woo_products_per_page')){

//     global $options;
//         function xtw_woo_products_per_page() {

//         $per_page   = 3; // $options[ 'opt-woo-products-per-page' ];
//         if ( isset( $_COOKIE['per_page'] ) ) {
//             $per_page = $_COOKIE['per_page'];
//         }
//         if ( isset( $_POST['per_page'] ) ) {
//             setcookie( 'per_page', $_POST['per_page'], time()+1209600, '/' );
//             $per_page = $_POST['per_page'];
//         }
//         return $per_page;
//         }

//     add_filter( 'loop_shop_per_page', array( $this, 'xtw_woo_products_per_page' ), 20 );

//     }
// }

// add_action( 'init', array( $this, 'wc_pac_fire_customisations' ) );

/**
*
*    Optimize WooCommerce Scripts
*    Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
*
**/

//add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );
 
function child_manage_woocommerce_styles() {
    //remove generator meta tag
    remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
 
    //first check that woo exists to prevent fatal errors
    if ( function_exists( 'is_woocommerce' ) ) {
        //dequeue scripts and styles
        if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
			wp_dequeue_style( 'woocommerce-layout' );
			wp_dequeue_style( 'woocommerce-smallscreen' );
			wp_dequeue_style( 'woocommerce-general' );
			wp_dequeue_style( 'woocommerce_frontend_styles' );
			wp_dequeue_style( 'woocommerce_fancybox_styles' );
			wp_dequeue_style( 'woocommerce_chosen_styles' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
			
			wp_dequeue_script( 'wc_price_slider' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-add-to-cart' );
			wp_dequeue_script( 'wc-cart-fragments' );
			wp_dequeue_script( 'wc-checkout' );
			wp_dequeue_script( 'wc-add-to-cart-variation' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-cart' );
			wp_dequeue_script( 'wc-chosen' );
			           
			wp_dequeue_script( 'woocommerce' );
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_script( 'jquery-blockui' );
			wp_dequeue_script( 'jquery-placeholder' );
			wp_dequeue_script( 'fancybox' );
			wp_dequeue_script( 'jqueryui' );

        }
    }
 
}

/**
*
*	MULTIPLE
*	External Link
*	TODO: dorobit funkciu / rotate ze sa bude menit kazdy tyzden napriklad -> ina lekaren
*
**/

function woo_external_link(){

	// Default external link description
	if ( function_exists('default_external_link_desc') && get_field('default_external_link_desc') ) : 
			$default_title =  get_field('default_external_link_desc');

		// check if the repeater field has rows of data
		if( have_rows('external_links') ):

//		 	loop through the rows of data
		    while ( have_rows('external_links') ) : the_row();

				// External link
				$external_link = get_sub_field('external_link');

				// External link description
				if($default_title &&  ! get_sub_field('external_link_desc')){
					$title = $default_title;
				}	else if(get_sub_field('external_link_desc')){
					$title = get_sub_field('external_link_desc');
				}	else {
					$title = get_the_title();
				}
				
		        // display a sub field value
					echo '<a href="'. addhttp($external_link) .'" target="_blank" title="'.$title.'" class="button">Kúpiť online</a>';
		    endwhile;

		 //    $rows = get_field('external_links'); // get all the rows
			// $first_row = $rows[0]; // get the first row
			// $first_row_link = $first_row['external_link']; // get the sub field value 
			// $first_row_desc = $first_row['external_link_desc']; // get the sub field value 
			
			// echo $first_row_desc[0];
			// //echo '<a href="'. $first_row_link[0].'" target="_blank" title="'.$first_row_desc[0].'" class="button">Kúpiť online</a>';

		else :
		    // no rows found
        endif;

	endif;
}


/**
*
*	Vypnutie niektorych CSS / neskor su pridane cez LESS
*
**/

    add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles2', 99 );

    function child_manage_woocommerce_styles2() {
         //first check that woo exists to prevent fatal errors
        if ( function_exists( 'is_woocommerce' ) ) {
    		wp_dequeue_style( 'woocommerce-general' );
    		wp_dequeue_style( 'grid-list-button' );
    		//	wp_dequeue_script( 'woocommerce' );
        }
     
    }


// Remove upsell
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );



// Replace shop page title

// if(!function_exists('xtw_shop_page_title')){


//     function xtw_shop_page_title($options){
    
//     $options['opt-shop-page-title'] = 'kpko';

//         return $options['opt-shop-page-title'];

//     }

//     if ($options['opt-shop-page-title']){

//         add_filter( 'woocommerce_page_title', 'xtw_shop_page_title');

//     }

// }
//         add_filter( 'woocommerce_page_title', 'xtw_shop_page_title');


if( !function_exists('xtw_shop_page_title') ){

global $options;

function xtw_shop_page_title(  ) {
   return $options['opt-shop-page-title'];
    // return "My new title";
}

if ($options['opt-shop-page-title']){

    add_filter( 'woocommerce_page_title', 'xtw_shop_page_title');
 
} 

}


/**
*
*  FUTURE
*
**/

/**
  Changing Ajax Loader on WooCommerce Cart
 */

add_filter('woocommerce_ajax_loader_url', 'xtw_woo_custom_cart_loader');

function xtw_woo_custom_cart_loader() {
  return __(plugin_dir_path( __FILE__ ).'/images/ajax-loader-2.gif', 'woocommerce');
}


?>