<?php 

/** 
*
*   ScrollUp Header
*
**/

    function xtw_scroll_up_script() {
        global $options;
        
        // Register scripts
            wp_register_script('scrollup-header', plugins_url( '../lib/js/scroll-up/stickUp.min.js', __FILE__ ), false,'1.0.0', true);
            wp_register_script('scbar', plugins_url( '../lib/js/scroll-up/scbar.js', __FILE__ ), false,'1.0.0', true);

            wp_enqueue_script( 'scrollup-header' );
            wp_enqueue_script( 'scbar' );

        // Localize script with options
            $scrollup_translation_array = array( 
                'scrollup_element' =>  $options['opt-scrollup-header-id-class'] ,
            );
            wp_localize_script( 'scrollup-header', 'scrollup_name', $scrollup_translation_array );
    }

    if($options['opt-scrollup-header']){
        add_action( 'wp_enqueue_scripts', 'xtw_scroll_up_script' );
    }

 ?>