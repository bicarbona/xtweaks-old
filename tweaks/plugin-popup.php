<?php

/**
*
* Popup Plugin
*
* @ OSETRIT get field lebo inac padne pri vypnutom acf
* nastavenia v redux-config-popup.php - treba spravne nastavenie cpt aby sa zobrazilo
*
**/

/**
* Random string do ACF
*
* http://www.jaiswar-vipin-r.com/generate-alphanumeric-random-string-random-javascript-or-jquery/
* http://www.mediacollege.com/internet/javascript/number/random.html
*
*/


function xtw_popup(){

wp_enqueue_script('xtw-popup', plugins_url( '../lib/js/popup.js', __FILE__ ), array( 'jquery' ));

wp_enqueue_script('xtw-colorbox', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.3/jquery.colorbox.js', array( 'jquery' ));


/**
  v admin casti zabezpecuje targetign conditions
 Admin - TEPORARY OFF hadze chybu
*/

function xtw_popup_load_admin_scripts() {

$js_dir = XTW_URL . '/lib/js/';

$suffix = '.js';

        wp_enqueue_script( 'popup-maker-admin', $js_dir . 'popup-maker-admin' . $suffix, array(
            'jquery',
            'wp-color-picker',
            'jquery-ui-slider'
        ) );
}

// add_action( 'admin_enqueue_scripts', 'xtw_popup_load_admin_scripts', 100 );


// WP_Query arguments
$args = array (
    'post_type'              => array( 'popup' ),
    'post_status'            => array( 'publish' ),
);

// The Query
$query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
            $query->the_post();

        $xtw_colorbox_array = array(
            'html' => get_the_content(),
            'width' => get_field('xtw_popup_width'),
            'height' => get_field('xtw_popup_height'),
            'escKey' => true,
            'overlayClose' => true,
            'cookieKey' => get_the_ID().'-'.get_field('xtw_popup_cookie_key'),
            'cookieExpire' => get_field('xtw_popup_cookie_expire'),
           // 'scrolling' => "false",
            'opacity' => get_field('xtw_popup_opacity'),
            'timeoutOpen' => get_field('xtw_popup_timeout_open'),
            'autoClose' => get_field('xtw_popup_auto_close'),
            'timeoutAutoClose' => get_field('xtw_popup_auto_close_timeout'),
            'className' => get_field('xtw_popup_class_name'),
            'closeButton' => get_field('xtw_popup_close_button'),
            'closeButtonText' => get_field('xtw_popup_close_button_text'),
            // 'escKey' => false,
        );


        }
    } else {
        // no posts found
    }

// Restore original Post Data
wp_reset_postdata();

wp_localize_script( 'xtw-popup', 'modal_popup', $xtw_colorbox_array);
// wp localize nazov ako skript ktory lokalizujem
}

if ($options['opt-cpt-popup']){

    add_action( 'template_redirect', '__conditions' ); // dolezite => template_redirect

}


function __conditions(){

    global $post, $wp_query;

    $conditions = array('on_404' => 1,
                        'on_search' => 4,
                        'on_home' => 1,
                       // 'on_pages' => 1,
                        'on_posts' => 1,
                       // 'on_woocommerce'=>1,
                        'on_products' =>1,
                        'on_mobile' => 1
                        );
    $sitewide = false;

    // wp_is_mobile()


    /**
     * Front Page Checks
     */
    if ( is_front_page() ) {
        if ( ! $sitewide && array_key_exists( 'on_home', $conditions ) ) {
            $is_loadable = true;
        } elseif ( $sitewide && array_key_exists( 'exclude_on_home', $conditions ) ) {
            $is_loadable = false;
        }
    }


    /**
     * Mobile
     */
    if ( wp_is_mobile() ) {
        if ( ! $sitewide && array_key_exists( 'on_mobile', $conditions ) ) {
            $is_loadable = false;
        } elseif ( $sitewide && array_key_exists( 'exclude_on_mobile', $conditions ) ) {
            $is_loadable = false;
        }
    }


    /**
     * Page Checks
     */
    elseif ( is_page() ) {
        if ( ! $sitewide ) {
            // Load on all pages
            if ( array_key_exists( 'on_pages', $conditions ) && ! array_key_exists( 'on_specific_pages', $conditions ) ) {
                $is_loadable = true;
            } // Load on specific pages
            elseif ( array_key_exists( 'on_specific_pages', $conditions ) && array_key_exists( 'on_page_' . $post->ID, $conditions ) ) {
                $is_loadable = true;
            }
        } else {
            // Exclude on all pages.
            if ( array_key_exists( 'exclude_on_pages', $conditions ) && ! array_key_exists( 'exclude_on_specific_pages', $conditions ) ) {
                $is_loadable = false;
            } // Exclude on specific pages.
            elseif ( array_key_exists( 'exclude_on_specific_pages', $conditions ) && array_key_exists( 'exclude_on_page_' . $post->ID, $conditions ) ) {
                $is_loadable = false;
            }
        }
    }

    /**
     * Post Checks
     */
    elseif ( is_single() && $post->post_type == 'post' ) {
        if ( ! $sitewide ) {
            // Load on all posts`1
            if ( array_key_exists( 'on_posts', $conditions ) && ! array_key_exists( 'on_specific_posts', $conditions ) ) {
                $is_loadable = true;
            } // Load on specific posts
            elseif ( array_key_exists( 'on_specific_posts', $conditions ) && array_key_exists( 'on_post_' . $post->ID, $conditions ) ) {
                $is_loadable = true;
            }
        } else { //sitewide
            // Exclude on all posts.
            if ( array_key_exists( 'exclude_on_posts', $conditions ) && ! array_key_exists( 'exclude_on_specific_posts', $conditions ) ) {
                $is_loadable = false;
            } // Exclude on specific posts.
            elseif ( array_key_exists( 'exclude_on_specific_posts', $conditions ) && array_key_exists( 'exclude_on_post_' . $post->ID, $conditions ) ) {
                $is_loadable = false;
            }
        }
    }

    /**
     * Category Checks
     */
    elseif ( is_category() ) {
        $category_id = $wp_query->get_queried_object_id();
        if ( ! $sitewide ) {
            // Load on all categories
            if ( array_key_exists( 'on_categories', $conditions ) && ! array_key_exists( 'on_specific_categories', $conditions ) ) {
                $is_loadable = true;
            } // Load on specific categories
            elseif ( array_key_exists( 'on_specific_categories', $conditions ) && array_key_exists( 'on_category_' . $category_id, $conditions ) ) {
                $is_loadable = true;
            }
        } else {
            // Exclude on all categories.
            if ( array_key_exists( 'exclude_on_categories', $conditions ) && ! array_key_exists( 'exclude_on_specific_categories', $conditions ) ) {
                $is_loadable = false;
            } // Exclude on specific categories.
            elseif ( array_key_exists( 'exclude_on_specific_categories', $conditions ) && array_key_exists( 'exclude_on_category_' . $category_id, $conditions ) ) {
                $is_loadable = false;
            }
        }
    }

    /**
     * Tag Checks
     */
    elseif ( is_tag() ) {
        $tag_id = $wp_query->get_queried_object_id();
        if ( ! $sitewide ) {
            // Load on all tags
            if ( array_key_exists( 'on_tags', $conditions ) && ! array_key_exists( 'on_specific_tags', $conditions ) ) {
                $is_loadable = true;
            } // Load on specific tags
            elseif ( array_key_exists( 'on_specific_tags', $conditions ) && array_key_exists( 'on_tag_' . $tag_id, $conditions ) ) {
                $is_loadable = true;
            }
        } else {
            // Exclude on all tags.
            if ( array_key_exists( 'exclude_on_tags', $conditions ) && ! array_key_exists( 'exclude_on_specific_tags', $conditions ) ) {
                $is_loadable = false;
            } // Exclude on specific tags.
            elseif ( array_key_exists( 'exclude_on_specific_tags', $conditions ) && array_key_exists( 'exclude_on_tag_' . $tag_id, $conditions ) ) {
                $is_loadable = false;
            }
        }
    }

    /**
     * Custom Post Type Checks
     * Add support for custom post types
     */
    elseif ( is_single() && ! in_array( $post->post_type, array( 'post', 'page' ) ) ) {
        $pt = $post->post_type;

        if ( ! $sitewide ) {
            // Load on all post type items
            if ( array_key_exists( "on_{$pt}s", $conditions ) && ! array_key_exists( "on_specific_{$pt}s", $conditions ) ) {
                $is_loadable = true;
            } // Load on specific post type items
            elseif ( array_key_exists( "on_specific_{$pt}s", $conditions ) && array_key_exists( "on_{$pt}_" . $post->ID, $conditions ) ) {
                $is_loadable = true;
            }
        } else {
            // Exclude on all post type items.
            if ( array_key_exists( "exclude_on_{$pt}s", $conditions ) && ! array_key_exists( "exclude_on_specific_{$pt}s", $conditions ) ) {
                $is_loadable = false;
            } // Exclude on specific post type items.
            elseif ( array_key_exists( "exclude_on_specific_{$pt}s", $conditions ) && array_key_exists( "exclude_on_{$pt}_" . $post->ID, $conditions ) ) {
                $is_loadable = false;
            }
        }
    }

    /**
     * Custom Taxonomy Checks
     * Add support for custom taxonomies
     */
    elseif ( is_tax() ) {
        $term_id = $wp_query->get_queried_object_id();
        $tax     = get_query_var( 'taxonomy' );
        if ( ! $sitewide ) {
            // Load on all custom tax terms.
            if ( array_key_exists( "on_{$tax}s", $conditions ) && ! array_key_exists( "on_specific_{$tax}s", $conditions ) ) {
                $is_loadable = true;
            } // Load on specific custom tax terms.
            elseif ( array_key_exists( "on_specific_{$tax}s", $conditions ) && array_key_exists( "on_{$tax}_" . $term_id, $conditions ) ) {
                $is_loadable = true;
            }
        } else {
            // Exclude on all custom tax terms.
            if ( array_key_exists( "exclude_on_{$tax}s", $conditions ) && ! array_key_exists( "exclude_on_specific_{$tax}s", $conditions ) ) {
                $is_loadable = false;
            } // Exclude on specific custom tax terms.
            elseif ( array_key_exists( "exclude_on_specific_{$tax}s", $conditions ) && array_key_exists( "exclude_on_{$tax}_" . $term_id, $conditions ) ) {
                $is_loadable = false;
            }
        }
    }

    /**
     * Blog Index Page Checks
     */
    if ( is_home() ) {
        if ( ! $sitewide && array_key_exists( 'on_blog', $conditions ) ) {
            $is_loadable = true;
        } elseif ( $sitewide && array_key_exists( 'exclude_on_blog', $conditions ) ) {
            $is_loadable = false;
        }
    }

    /**
     * Search Checks
     */
    if ( is_search() ) {
        if ( ! $sitewide && array_key_exists( 'on_search', $conditions ) ) {
            $is_loadable = true;
        } elseif ( $sitewide && array_key_exists( 'exclude_on_search', $conditions ) ) {
            $is_loadable = false;
        }
    }

    /**
     * 404 Page Checks
     */
    if ( is_404() ) {
        if ( ! $sitewide && array_key_exists( 'on_404', $conditions ) ) {
            $is_loadable = true;

        } elseif ( $sitewide && array_key_exists( 'exclude_on_404', $conditions ) ) {
            $is_loadable = false;

        }
    }

    /**
     * WooCommerce Page Checks
     */

    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

    if ( is_woocommerce() ) {
        if ( ! $sitewide && array_key_exists( 'on_woocommerce', $conditions ) ) {
            $is_loadable = true;
        } elseif ( $sitewide && array_key_exists( 'exclude_on_woocommerce', $conditions ) ) {
            $is_loadable = false;
        }
    }

    /**
     * Shop Page Checks
     */
    if ( is_shop() ) {
        if ( ! $sitewide && array_key_exists( 'on_shop', $conditions ) ) {
            $is_loadable = true;
        } elseif ( $sitewide && array_key_exists( 'exclude_on_shop', $conditions ) ) {
            $is_loadable = false;
        }
    }

   }
   if($is_loadable){
        add_action( 'wp_enqueue_scripts', 'xtw_popup' );
   }

}

/**
 test
 */
     // add_action('init', 'hello_world');
     // function hello_world() { echo "Hello World"; }

/**
 End test
 */


/**
 Admin Side
 */

function popmake_add_popup_meta_box() {

    /** Loading Meta **/
    add_meta_box( 'popmake_popup_targeting_condition', __( 'Targeting Conditions', 'popup-maker' ), 'popmake_render_popup_targeting_condition_meta_box', 'popup', 'side', 'high' );

}
add_action( 'add_meta_boxes', 'popmake_add_popup_meta_box' );


/**
 * Popup Load Settings Metabox
 *
 * Extensions (as well as the core plugin) can add items to the popup targeting_condition
 * metabox via the `popmake_popup_targeting_condition_meta_box_fields` action.
 *
 * @since 1.0
 * @return void
 */
function popmake_render_popup_targeting_condition_meta_box() {
    global $post; ?>
    <div id="popmake_popup_targeting_condition_fields" class="popmake_meta_table_wrap">
    <?php do_action( 'popmake_popup_targeting_condition_meta_box_fields', $post->ID ); ?>
    </div><?php
}



if ( ! function_exists( 'get_term_name' ) ) {
    function get_term_name( $term_id, $taxonomy ) {
        $term = get_term_by( 'id', absint( $term_id ), $taxonomy );

        return $term->name;
    }
}


add_action( 'popmake_popup_targeting_condition_meta_box_fields', 'popmake_popup_targeting_condition_meta_box_fields', 10 );

function popmake_popup_targeting_condition_meta_box_fields( $popup_id ) {
    $targeting_condition = popmake_get_popup_targeting_condition( $popup_id );
    /**
     * Create nonce used for post type and taxonomy ajax searches. Copied from wp-admin/includes/nav-menu.php
     */
    wp_nonce_field( 'add-menu_item', 'menu-settings-column-nonce' );

    /**
     * Render Load on entire site toggle.
     */ ?>
    <div id="targeting_condition-on_entire_site" class="targeting_condition form-table">
        <input type="checkbox"
               id="popup_targeting_condition_on_entire_site"
               name="popup_targeting_condition_on_entire_site"
               value="true"
            <?php if ( ! empty( $targeting_condition['on_entire_site'] ) ) {
                echo 'checked="checked" ';
            } ?>
            />
        <label for="popup_targeting_condition_on_entire_site"><?php _e( 'On Entire Site', 'popup-maker' ); ?></label>

        <div class="options">
            <?php do_action( "popmake_popup_targeting_condition_on_entire_site_options", $targeting_condition ); ?>
        </div>
    </div>
    <div id="targeting_condition-on_home" class="targeting_condition form-table">
        <input type="checkbox"
               id="popup_targeting_condition_on_home"
               name="popup_targeting_condition_on_home"
               value="true"
            <?php if ( ! empty( $targeting_condition['on_home'] ) ) {
                echo 'checked="checked" ';
            } ?>
            />
        <label for="popup_targeting_condition_on_home"><?php _e( 'On Home Page', 'popup-maker' ); ?></label>

        <div class="options">
            <?php do_action( "popmake_popup_targeting_condition_on_home_options", $targeting_condition ); ?>
        </div>
    </div>
    <div id="targeting_condition-exclude_on_home" class="targeting_condition form-table">
        <input type="checkbox"
               id="popup_targeting_condition_exclude_on_home"
               name="popup_targeting_condition_exclude_on_home"
               value="true"
            <?php if ( ! empty( $targeting_condition['exclude_on_home'] ) ) {
                echo 'checked="checked" ';
            } ?>
            />
        <label for="popup_targeting_condition_exclude_on_home"><?php _e( 'Exclude on Home Page', 'popup-maker' ); ?></label>

        <div class="options">
            <?php do_action( "popmake_popup_targeting_condition_exclude_on_home_options", $targeting_condition ); ?>
        </div>
    </div>
    <div id="targeting_condition-on_blog" class="targeting_condition form-table">
        <input type="checkbox"
               id="popup_targeting_condition_on_blog"
               name="popup_targeting_condition_on_blog"
               value="true"
            <?php if ( ! empty( $targeting_condition['on_blog'] ) ) {
                echo 'checked="checked" ';
            } ?>
            />
        <label for="popup_targeting_condition_on_blog"><?php _e( 'On Blog Index', 'popup-maker' ); ?></label>

        <div class="options">
            <?php do_action( "popmake_popup_targeting_condition_on_blog_options", $targeting_condition ); ?>
        </div>
    </div>
    <div id="targeting_condition-exclude_on_blog" class="targeting_condition form-table">
    <input type="checkbox"
           id="popup_targeting_condition_exclude_on_blog"
           name="popup_targeting_condition_exclude_on_blog"
           value="true"
        <?php if ( ! empty( $targeting_condition['exclude_on_blog'] ) ) {
            echo 'checked="checked" ';
        } ?>
        />
    <label for="popup_targeting_condition_exclude_on_blog"><?php _e( 'Exclude on Blog Index', 'popup-maker' ); ?></label>

    <div class="options">
        <?php do_action( "popmake_popup_targeting_condition_exclude_on_blog_options", $targeting_condition ); ?>
    </div>
    </div><?php

    do_action( 'popmake_before_post_type_targeting_conditions', $targeting_condition );

    $includes = popmake_get_popup_targeting_condition_includes( $popup_id );
    $excludes = popmake_get_popup_targeting_condition_excludes( $popup_id );

    foreach ( popmake_get_supported_types() as $pt ) {
        $is_post_type = get_post_type_object( $pt );
        $labels       = $is_post_type ? $is_post_type : get_taxonomy( $pt );
        if ( ! $labels ) {
            continue;
        }
        $plural = esc_attr( strtolower( $labels->labels->name ) );

        foreach ( array( 'include', 'exclude' ) as $include_exclude ) {
            $key     = ( $include_exclude != 'include' ? 'exclude_' : '' ) . "on_{$pt}s";
            $current = $include_exclude == 'include' ?
                ( ! empty( $includes[ $pt ] ) ? $includes[ $pt ] : array() ) :
                ( ! empty( $excludes[ $pt ] ) ? $excludes[ $pt ] : array() ); ?>
        <div id="targeting_condition-<?php echo $key; ?>" class="targeting_condition form-table">
            <input type="checkbox"
                   id="popup_targeting_condition_<?php echo $key; ?>"
                   name="popup_targeting_condition_<?php echo $key; ?>"
                   value="true"
                <?php if ( ! empty( $targeting_condition[ $key ] ) ) {
                    echo 'checked="checked" ';
                } ?>
                /><?php
            $label = ( $include_exclude != 'include' ? 'Exclude ' : '' ) . 'On '; ?>
            <label for="popup_targeting_condition_<?php echo $key; ?>"><?php echo __( $label, 'popup-maker' ) . $labels->labels->name; ?></label>

            <div class="options">
                <p style="margin:0;"><?php
                    $key = ( $include_exclude != 'include' ? 'exclude_' : '' ) . "on_specific_{$pt}s"; ?>
                    <input type="checkbox" style="display:none" name="popup_targeting_condition_<?php echo $key; ?>" value="true" <?php if ( isset( $targeting_condition[ $key ] ) ) {
                        echo 'checked';
                    } ?>/>
                    <label><?php
                        $label = ( $include_exclude == 'include' ? 'Load' : 'Exclude' ) . ' on All ';
                        echo __( $label, 'popup-maker' ) . $labels->labels->name; ?>
                        <input type="radio"
                               name="<?php echo $key; ?>"
                               id="popup_targeting_condition_<?php echo $key; ?>"
                               value=""
                            <?php if ( ! isset( $targeting_condition[ $key ] ) ) {
                                echo 'checked';
                            } ?>
                            />
                    </label><br/>
                    <label><?php
                        $label = ( $include_exclude == 'include' ? 'Load' : 'Exclude' ) . ' on Specific ';
                        echo __( $label, 'popup-maker' ) . $labels->labels->name; ?>
                        <input type="radio"
                               name="<?php echo $key; ?>"
                               id="popup_targeting_condition_<?php echo $key; ?>"
                               value="true"
                            <?php if ( isset( $targeting_condition[ $key ] ) ) {
                                echo 'checked';
                            } ?>
                            />
                    </label>
                </p>

                <div id="<?php echo $key; ?>">
                    <div class="nojs-tags hide-if-js">
                            <textarea
                                name="popup_targeting_condition_<?php echo $include_exclude == 'exclude' ? 'exclude_' : ''; ?>on_<?php echo $pt; ?>"
                                rows="3" cols="20"
                                id="popup_targeting_condition_<?php echo $include_exclude == 'exclude' ? 'exclude_' : ''; ?>on_<?php echo $pt; ?>"
                                ><?php esc_html_e( trim( implode( ',', $current ) ) ); ?></textarea>
                    </div>
                    <div class="hide-if-no-js"><?php
                        if ( $is_post_type ) {
                            popmake_post_type_item_metabox( $pt );
                        } else {
                            popmake_taxonomy_item_metabox( $pt );
                        } ?>
                        <div class="tagchecklist"><?php
                            foreach ( $current as $post_id ) { ?>
                                <span><a class="ntdelbutton" data-id="<?php echo $post_id; ?>">X</a>
                                <?php echo $is_post_type ? get_the_title( $post_id ) : get_term_name( $post_id, $pt ); ?>
                                </span><?php
                            } ?>
                        </div>
                    </div>
                    <hr/>
                </div>
            </div>
            </div><?php
        }
    } ?>
    <div id="targeting_condition-on_search" class="targeting_condition form-table">
        <input type="checkbox"
               id="popup_targeting_condition_on_search"
               name="popup_targeting_condition_on_search"
               value="true"
            <?php if ( ! empty( $targeting_condition['on_search'] ) ) {
                echo 'checked="checked" ';
            } ?>
            />
        <label for="popup_targeting_condition_on_search"><?php _e( 'On Search Pages', 'popup-maker' ); ?></label>

        <div class="options">
            <?php do_action( "popmake_popup_targeting_condition_on_search_options", $targeting_condition ); ?>
        </div>
    </div>
    <div id="targeting_condition-exclude_on_search" class="targeting_condition form-table">
        <input type="checkbox"
               id="popup_targeting_condition_exclude_on_search"
               name="popup_targeting_condition_exclude_on_search"
               value="true"
            <?php if ( ! empty( $targeting_condition['exclude_on_search'] ) ) {
                echo 'checked="checked" ';
            } ?>
            />
        <label for="popup_targeting_condition_exclude_on_search"><?php _e( 'Exclude on Search Pages', 'popup-maker' ); ?></label>

        <div class="options">
            <?php do_action( "popmake_popup_targeting_condition_exclude_on_search_options", $targeting_condition ); ?>
        </div>
    </div>
    <div id="targeting_condition-on_404" class="targeting_condition form-table">
        <input type="checkbox"
               id="popup_targeting_condition_on_404"
               name="popup_targeting_condition_on_404"
               value="true"
            <?php if ( ! empty( $targeting_condition['on_404'] ) ) {
                echo 'checked="checked" ';
            } ?>
            />
        <label for="popup_targeting_condition_on_404"><?php _e( 'On 404 Pages', 'popup-maker' ); ?></label>

        <div class="options">
            <?php do_action( "popmake_popup_targeting_condition_on_404_options", $targeting_condition ); ?>
        </div>
    </div>
    <div id="targeting_condition-exclude_on_404" class="targeting_condition form-table">
    <input type="checkbox"
           id="popup_targeting_condition_exclude_on_404"
           name="popup_targeting_condition_exclude_on_404"
           value="true"
        <?php if ( ! empty( $targeting_condition['exclude_on_404'] ) ) {
            echo 'checked="checked" ';
        } ?>
        />
    <label for="popup_targeting_condition_exclude_on_404"><?php _e( 'Exclude on 404 Pages', 'popup-maker' ); ?></label>

    <div class="options">
        <?php do_action( "popmake_popup_targeting_condition_exclude_on_404_options", $targeting_condition ); ?>
    </div>
    </div><?php
}



/**
 * Returns the load settings meta of a popup.
 *
 * @since 1.0
 *
 * @param int $popup_id ID number of the popup to retrieve a overlay meta for
 *
 * @return mixed array|string of the popup load settings meta
 */
function popmake_get_popup_targeting_condition( $popup_id = null, $key = null ) {
    return popmake_get_popup_meta_group( 'targeting_condition', $popup_id, $key );
}

function popmake_get_popup_targeting_condition_includes( $popup_id, $post_type = null ) {
    $post_meta = get_post_custom_keys( $popup_id );
    $includes  = array();
    if ( ! empty( $post_meta ) ) {
        foreach ( $post_meta as $meta_key ) {
            if ( strpos( $meta_key, 'popup_targeting_condition_on_' ) !== false ) {
                $id = intval( substr( strrchr( $meta_key, "_" ), 1 ) );

                if ( $id > 0 ) {
                    $remove = strrchr( $meta_key, strrchr( $meta_key, "_" ) );
                    $name   = str_replace( 'popup_targeting_condition_on_', "", str_replace( $remove, "", $meta_key ) );

                    $includes[ $name ][] = intval( $id );
                }
            }
        }
    }
    if ( $post_type ) {
        if ( ! isset( $includes[ $post_type ] ) || empty( $includes[ $post_type ] ) ) {
            $includes[ $post_type ] = array();
        }

        return $includes[ $post_type ];
    }

    return $includes;
}

function popmake_get_popup_targeting_condition_excludes( $popup_id, $post_type = null ) {
    $post_meta = get_post_custom_keys( $popup_id );
    $excludes  = array();
    if ( ! empty( $post_meta ) ) {
        foreach ( $post_meta as $meta_key ) {
            if ( strpos( $meta_key, 'popup_targeting_condition_exclude_on_' ) !== false ) {
                $id = intval( substr( strrchr( $meta_key, "_" ), 1 ) );

                if ( $id > 0 ) {
                    $remove = strrchr( $meta_key, strrchr( $meta_key, "_" ) );
                    $name   = str_replace( 'popup_targeting_condition_exclude_on_', "", str_replace( $remove, "", $meta_key ) );

                    $excludes[ $name ][] = intval( $id );
                }
            }
        }
    }
    if ( $post_type ) {
        if ( ! isset( $excludes[ $post_type ] ) || empty( $excludes[ $post_type ] ) ) {
            $excludes[ $post_type ] = array();
        }

        return $excludes[ $post_type ];
    }

    return $excludes;
}


/**
 * Returns the meta group of a popup or value if key is set.
 *
 * @since 1.0
 *
 * @param int $popup_id ID number of the popup to retrieve a overlay meta for
 *
 * @return mixed array|string
 */
function popmake_get_popup_meta_group( $group, $popup_id = null, $key = null, $default = null ) {
    if ( ! $popup_id ) {
        $popup_id = popmake_get_the_popup_ID();
    }

    $post_meta         = get_post_custom( $popup_id );

    if ( ! is_array( $post_meta ) ) {
        $post_meta = array();
    }

    $default_check_key = 'popup_defaults_set';
    if ( ! in_array( $group, array( 'auto_open', 'close', 'display', 'targeting_condition' ) ) ) {
        $default_check_key = "popup_{$group}_defaults_set";
    }

    $group_values = array_key_exists( $default_check_key, $post_meta ) ? array() : apply_filters( "popmake_popup_{$group}_defaults", array() );
    foreach ( $post_meta as $meta_key => $value ) {
        if ( strpos( $meta_key, "popup_{$group}_" ) !== false ) {
            $new_key = str_replace( "popup_{$group}_", '', $meta_key );
            if ( count( $value ) == 1 ) {
                $group_values[ $new_key ] = $value[0];
            } else {
                $group_values[ $new_key ] = $value;
            }
        }
    }
    if ( $key ) {
        $key = str_replace( '.', '_', $key );
        if ( ! isset( $group_values[ $key ] ) ) {
            $value = $default;
        } else {
            $value = $group_values[ $key ];
        }

        return apply_filters( "popmake_get_popup_{$group}_$key", $value, $popup_id );
    } else {
        return apply_filters( "popmake_get_popup_{$group}", $group_values, $popup_id );
    }
}


function popmake_get_supported_types( $type = null, $collapse = true ) {
    $types = array(
        'post_type' => apply_filters( 'popmake_supported_post_types', array( 'post', 'page' ) ),
        'taxonomy'  => apply_filters( 'popmake_supported_taxonomies', array( 'category', 'post_tag' ) )
    );

    if ( $type ) {
        return $types[ $type ];
    } elseif ( $collapse ) {
        return array_merge( $types['post_type'], $types['taxonomy'] );
    }

    return $types;
}



/**
 * Displays a metabox for a post type menu item.
 *
 * @since 1.0.0
 *
 * @param string $object Not used.
 * @param string $post_type The post type object.
 */
function popmake_post_type_item_metabox( $post_type_name ) {
    if ( ! function_exists( 'wp_nav_menu_item_post_type_meta_box' ) ) {
        include ABSPATH . 'wp-admin/includes/nav-menu.php';
    }
    global $_nav_menu_placeholder, $nav_menu_selected_id;

    $post_type = get_post_type_object( $post_type_name );


    // Paginate browsing for large numbers of post objects.
    $per_page = 50;
    $pagenum  = isset( $_REQUEST[ $post_type_name . '-tab' ] ) && isset( $_REQUEST['paged'] ) ? absint( $_REQUEST['paged'] ) : 1;
    $offset   = 0 < $pagenum ? $per_page * ( $pagenum - 1 ) : 0;

    $args = array(
        'offset'                 => $offset,
        'order'                  => 'ASC',
        'orderby'                => 'title',
        'posts_per_page'         => $per_page,
        'post_type'              => $post_type_name,
        'suppress_filters'       => true,
        'update_post_term_cache' => false,
        'update_post_meta_cache' => false
    );

    if ( isset( $post_type->_default_query ) ) {
        $args = array_merge( $args, (array) $post_type->_default_query );
    }

    // @todo transient caching of these results with proper invalidation on updating of a post of this type
    $get_posts = new WP_Query;
    $posts     = $get_posts->query( $args );
    if ( ! $get_posts->post_count ) {
        echo '<p>' . __( 'No items.' ) . '</p>';

        return;
    }

    $num_pages = $get_posts->max_num_pages;

    $page_links = paginate_links( array(
        'base'      => add_query_arg(
            array(
                $post_type_name . '-tab' => 'all',
                'paged'                  => '%#%',
                'item-type'              => 'post_type',
                'item-object'            => $post_type_name,
            )
        ),
        'format'    => '',
        'prev_text' => __( '&laquo;' ),
        'next_text' => __( '&raquo;' ),
        'total'     => $num_pages,
        'current'   => $pagenum
    ) );

    $db_fields = false;
    if ( is_post_type_hierarchical( $post_type_name ) ) {
        $db_fields = array( 'parent' => 'post_parent', 'id' => 'ID' );
    }

    $walker = new Walker_Nav_Menu_Checklist( $db_fields );

    $current_tab = 'most-recent';
    if ( isset( $_REQUEST[ $post_type_name . '-tab' ] ) && in_array( $_REQUEST[ $post_type_name . '-tab' ], array(
            'all',
            'search'
        ) )
    ) {
        $current_tab = $_REQUEST[ $post_type_name . '-tab' ];
    }

    if ( ! empty( $_REQUEST[ 'quick-search-posttype-' . $post_type_name ] ) ) {
        $current_tab = 'search';
    }

    $removed_args = array(
        'action',
        'customlink-tab',
        'edit-menu-item',
        'menu-item',
        'page-tab',
        '_wpnonce',
    );

    ?>
    <div id="posttype-<?php echo $post_type_name; ?>" class="posttypediv">
        <ul id="posttype-<?php echo $post_type_name; ?>-tabs" class="posttype-tabs category-tabs add-menu-item-tabs">
            <li <?php echo( 'most-recent' == $current_tab ? ' class="tabs"' : '' ); ?>>
                <a class="nav-tab-link" data-type="tabs-panel-posttype-<?php echo esc_attr( $post_type_name ); ?>-most-recent" href="<?php if ( $nav_menu_selected_id ) {
                    echo esc_url( add_query_arg( $post_type_name . '-tab', 'most-recent', remove_query_arg( $removed_args ) ) );
                } ?>#tabs-panel-posttype-<?php echo $post_type_name; ?>-most-recent">
                    <?php _e( 'Most Recent' ); ?>
                </a>
            </li>
            <li <?php echo( 'all' == $current_tab ? ' class="tabs"' : '' ); ?>>
                <a class="nav-tab-link" data-type="<?php echo esc_attr( $post_type_name ); ?>-all" href="<?php if ( $nav_menu_selected_id ) {
                    echo esc_url( add_query_arg( $post_type_name . '-tab', 'all', remove_query_arg( $removed_args ) ) );
                } ?>#<?php echo $post_type_name; ?>-all">
                    <?php _e( 'View All' ); ?>
                </a>
            </li>
            <li <?php echo( 'search' == $current_tab ? ' class="tabs"' : '' ); ?>>
                <a class="nav-tab-link" data-type="tabs-panel-posttype-<?php echo esc_attr( $post_type_name ); ?>-search" href="<?php if ( $nav_menu_selected_id ) {
                    echo esc_url( add_query_arg( $post_type_name . '-tab', 'search', remove_query_arg( $removed_args ) ) );
                } ?>#tabs-panel-posttype-<?php echo $post_type_name; ?>-search">
                    <?php _e( 'Search' ); ?>
                </a>
            </li>
        </ul>
        <!-- .posttype-tabs -->

        <div id="tabs-panel-posttype-<?php echo $post_type_name; ?>-most-recent" class="tabs-panel <?php
        echo( 'most-recent' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' );
        ?>">
            <ul id="<?php echo $post_type_name; ?>checklist-most-recent" class="categorychecklist form-no-clear">
                <?php
                $recent_args    = array_merge( $args, array(
                    'orderby'        => 'post_date',
                    'order'          => 'DESC',
                    'posts_per_page' => 15
                ) );
                $most_recent    = $get_posts->query( $recent_args );
                $args['walker'] = $walker;
                echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $most_recent ), 0, (object) $args );
                ?>
            </ul>
        </div>
        <!-- /.tabs-panel -->

        <div class="tabs-panel <?php
        echo( 'search' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' );
        ?>" id="tabs-panel-posttype-<?php echo $post_type_name; ?>-search">
            <?php
            if ( isset( $_REQUEST[ 'quick-search-posttype-' . $post_type_name ] ) ) {
                $searched       = esc_attr( $_REQUEST[ 'quick-search-posttype-' . $post_type_name ] );
                $search_results = get_posts( array(
                    's'         => $searched,
                    'post_type' => $post_type_name,
                    'fields'    => 'all',
                    'order'     => 'DESC',
                ) );
            } else {
                $searched       = '';
                $search_results = array();
            }
            ?>
            <p class="quick-search-wrap">
                <input type="search" class="quick-search input-with-default-title" title="<?php esc_attr_e( 'Search' ); ?>" value="<?php echo $searched; ?>" name="quick-search-posttype-<?php echo $post_type_name; ?>"/>
                <span class="spinner"></span>
                <?php submit_button( __( 'Search' ), 'button-small quick-search-submit button-secondary hide-if-js', 'submit', false, array( 'id' => 'submit-quick-search-posttype-' . $post_type_name ) ); ?>
            </p>

            <ul id="<?php echo $post_type_name; ?>-search-checklist" data-wp-lists="list:<?php echo $post_type_name ?>" class="categorychecklist form-no-clear">
                <?php if ( ! empty( $search_results ) && ! is_wp_error( $search_results ) ) : ?>
                    <?php
                    $args['walker'] = $walker;
                    echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $search_results ), 0, (object) $args );
                    ?>
                <?php elseif ( is_wp_error( $search_results ) ) : ?>
                    <li><?php echo $search_results->get_error_message(); ?></li>
                <?php elseif ( ! empty( $searched ) ) : ?>
                    <li><?php _e( 'No results found.' ); ?></li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.tabs-panel -->

        <div id="<?php echo $post_type_name; ?>-all" class="tabs-panel tabs-panel-view-all <?php
        echo( 'all' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' );
        ?>">
            <?php if ( ! empty( $page_links ) ) : ?>
                <div class="add-menu-item-pagelinks">
                    <?php echo $page_links; ?>
                </div>
            <?php endif; ?>
            <ul id="<?php echo $post_type_name; ?>checklist" data-wp-lists="list:<?php echo $post_type_name ?>" class="categorychecklist form-no-clear">
                <?php
                $args['walker'] = $walker;

                /*
                 * If we're dealing with pages, let's put a checkbox for the front
                 * page at the top of the list.
                 */
                if ( 'page' == $post_type_name ) {
                    $front_page = 'page' == get_option( 'show_on_front' ) ? (int) get_option( 'page_on_front' ) : 0;
                    if ( ! empty( $front_page ) ) {
                        $front_page_obj                = get_post( $front_page );
                        $front_page_obj->front_or_home = true;
                        array_unshift( $posts, $front_page_obj );
                    } else {
                        $_nav_menu_placeholder = ( 0 > $_nav_menu_placeholder ) ? intval( $_nav_menu_placeholder ) - 1 : - 1;
                        array_unshift( $posts, (object) array(
                            'front_or_home' => true,
                            'ID'            => 0,
                            'object_id'     => $_nav_menu_placeholder,
                            'post_content'  => '',
                            'post_excerpt'  => '',
                            'post_parent'   => '',
                            'post_title'    => _x( 'Home', 'nav menu home label' ),
                            'post_type'     => 'nav_menu_item',
                            'type'          => 'custom',
                            'url'           => home_url( '/' ),
                        ) );
                    }
                }

                /**
                 * Filter the posts displayed in the 'View All' tab of the current
                 * post type's menu items meta box.
                 *
                 * The dynamic portion of the hook name, $post_type_name,
                 * refers to the slug of the current post type.
                 *
                 * @since 3.2.0
                 *
                 * @see WP_Query::query()
                 *
                 * @param array $posts The posts for the current post type.
                 * @param array $args An array of WP_Query arguments.
                 * @param object $post_type The current post type object for this menu item meta box.
                 */
                $posts          = apply_filters( "nav_menu_items_{$post_type_name}", $posts, $args, $post_type );
                $checkbox_items = walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $posts ), 0, (object) $args );

                if ( 'all' == $current_tab && ! empty( $_REQUEST['selectall'] ) ) {
                    $checkbox_items = preg_replace( '/(type=(.)checkbox(\2))/', '$1 checked=$2checked$2', $checkbox_items );

                }

                echo $checkbox_items;
                ?>
            </ul>
            <?php if ( ! empty( $page_links ) ) : ?>
                <div class="add-menu-item-pagelinks">
                    <?php echo $page_links; ?>
                </div>
            <?php endif; ?>
        </div>
        <!-- /.tabs-panel -->

        <p class="button-controls">
            <span class="list-controls">
                <a href="<?php
                echo esc_url( add_query_arg(
                    array(
                        $post_type_name . '-tab' => 'all',
                        'selectall'              => 1,
                    ),
                    remove_query_arg( $removed_args )
                ) );
                ?>#posttype-<?php echo $post_type_name; ?>" class="select-all"><?php _e( 'Select All' ); ?></a>
            </span>

            <span class="add-to-list">
                <button type="button" <?php wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary submit-add-to-menu right" id="<?php echo esc_attr( 'submit-posttype-' . $post_type_name ); ?>"><?php esc_attr_e( 'Add Selected' ); ?></button>
                <span class="spinner"></span>
            </span>
        </p>

    </div><!-- /.posttypediv -->
    <?php
}




/**
 * Displays a metabox for a taxonomy menu item.
 *
 * @since 1.0.0
 *
 * @param string $taxonomy The taxonomy object.
 */
function popmake_taxonomy_item_metabox( $taxonomy_name ) {
    if ( ! function_exists( 'wp_nav_menu_item_post_type_meta_box' ) ) {
        include ABSPATH . 'wp-admin/includes/nav-menu.php';
    }
    global $nav_menu_selected_id;

    $taxonomy = get_taxonomy( $taxonomy_name );

    // Paginate browsing for large numbers of objects.
    $per_page = 50;
    $pagenum  = isset( $_REQUEST[ $taxonomy_name . '-tab' ] ) && isset( $_REQUEST['paged'] ) ? absint( $_REQUEST['paged'] ) : 1;
    $offset   = 0 < $pagenum ? $per_page * ( $pagenum - 1 ) : 0;

    $args = array(
        'child_of'     => 0,
        'exclude'      => '',
        'hide_empty'   => false,
        'hierarchical' => 1,
        'include'      => '',
        'number'       => $per_page,
        'offset'       => $offset,
        'order'        => 'ASC',
        'orderby'      => 'name',
        'pad_counts'   => false,
    );

    $terms = get_terms( $taxonomy_name, $args );

    if ( ! $terms || is_wp_error( $terms ) ) {
        echo '<p>' . __( 'No items.' ) . '</p>';

        return;
    }

    $num_pages = ceil( wp_count_terms( $taxonomy_name, array_merge( $args, array(
            'number' => '',
            'offset' => ''
        ) ) ) / $per_page );

    $page_links = paginate_links( array(
        'base'      => add_query_arg(
            array(
                $taxonomy_name . '-tab' => 'all',
                'paged'                 => '%#%',
                'item-type'             => 'taxonomy',
                'item-object'           => $taxonomy_name,
            )
        ),
        'format'    => '',
        'prev_text' => __( '&laquo;' ),
        'next_text' => __( '&raquo;' ),
        'total'     => $num_pages,
        'current'   => $pagenum
    ) );

    $db_fields = false;
    if ( is_taxonomy_hierarchical( $taxonomy_name ) ) {
        $db_fields = array( 'parent' => 'parent', 'id' => 'term_id' );
    }

    $walker = new Walker_Nav_Menu_Checklist( $db_fields );

    $current_tab = 'most-used';
    if ( isset( $_REQUEST[ $taxonomy_name . '-tab' ] ) && in_array( $_REQUEST[ $taxonomy_name . '-tab' ], array(
            'all',
            'most-used',
            'search'
        ) )
    ) {
        $current_tab = $_REQUEST[ $taxonomy_name . '-tab' ];
    }

    if ( ! empty( $_REQUEST[ 'quick-search-taxonomy-' . $taxonomy_name ] ) ) {
        $current_tab = 'search';
    }

    $removed_args = array(
        'action',
        'customlink-tab',
        'edit-menu-item',
        'menu-item',
        'page-tab',
        '_wpnonce',
    );

    ?>
    <div id="taxonomy-<?php echo $taxonomy_name; ?>" class="taxonomydiv">
        <ul id="taxonomy-<?php echo $taxonomy_name; ?>-tabs" class="taxonomy-tabs add-menu-item-tabs">
            <li <?php echo( 'most-used' == $current_tab ? ' class="tabs"' : '' ); ?>>
                <a class="nav-tab-link" data-type="tabs-panel-<?php echo esc_attr( $taxonomy_name ); ?>-pop" href="<?php if ( $nav_menu_selected_id ) {
                    echo esc_url( add_query_arg( $taxonomy_name . '-tab', 'most-used', remove_query_arg( $removed_args ) ) );
                } ?>#tabs-panel-<?php echo $taxonomy_name; ?>-pop">
                    <?php _e( 'Most Used' ); ?>
                </a>
            </li>
            <li <?php echo( 'all' == $current_tab ? ' class="tabs"' : '' ); ?>>
                <a class="nav-tab-link" data-type="tabs-panel-<?php echo esc_attr( $taxonomy_name ); ?>-all" href="<?php if ( $nav_menu_selected_id ) {
                    echo esc_url( add_query_arg( $taxonomy_name . '-tab', 'all', remove_query_arg( $removed_args ) ) );
                } ?>#tabs-panel-<?php echo $taxonomy_name; ?>-all">
                    <?php _e( 'View All' ); ?>
                </a>
            </li>
            <li <?php echo( 'search' == $current_tab ? ' class="tabs"' : '' ); ?>>
                <a class="nav-tab-link" data-type="tabs-panel-search-taxonomy-<?php echo esc_attr( $taxonomy_name ); ?>" href="<?php if ( $nav_menu_selected_id ) {
                    echo esc_url( add_query_arg( $taxonomy_name . '-tab', 'search', remove_query_arg( $removed_args ) ) );
                } ?>#tabs-panel-search-taxonomy-<?php echo $taxonomy_name; ?>">
                    <?php _e( 'Search' ); ?>
                </a>
            </li>
        </ul>
        <!-- .taxonomy-tabs -->

        <div id="tabs-panel-<?php echo $taxonomy_name; ?>-pop" class="tabs-panel <?php
        echo( 'most-used' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' );
        ?>">
            <ul id="<?php echo $taxonomy_name; ?>checklist-pop" class="categorychecklist form-no-clear">
                <?php
                $popular_terms  = get_terms( $taxonomy_name, array(
                    'orderby'      => 'count',
                    'order'        => 'DESC',
                    'number'       => 10,
                    'hierarchical' => false
                ) );
                $args['walker'] = $walker;
                echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $popular_terms ), 0, (object) $args );
                ?>
            </ul>
        </div>
        <!-- /.tabs-panel -->

        <div id="tabs-panel-<?php echo $taxonomy_name; ?>-all" class="tabs-panel tabs-panel-view-all <?php
        echo( 'all' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' );
        ?>">
            <?php if ( ! empty( $page_links ) ) : ?>
                <div class="add-menu-item-pagelinks">
                    <?php echo $page_links; ?>
                </div>
            <?php endif; ?>
            <ul id="<?php echo $taxonomy_name; ?>checklist" data-wp-lists="list:<?php echo $taxonomy_name ?>" class="categorychecklist form-no-clear">
                <?php
                $args['walker'] = $walker;
                echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $terms ), 0, (object) $args );
                ?>
            </ul>
            <?php if ( ! empty( $page_links ) ) : ?>
                <div class="add-menu-item-pagelinks">
                    <?php echo $page_links; ?>
                </div>
            <?php endif; ?>
        </div>
        <!-- /.tabs-panel -->

        <div class="tabs-panel <?php
        echo( 'search' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' );
        ?>" id="tabs-panel-search-taxonomy-<?php echo $taxonomy_name; ?>">
            <?php
            if ( isset( $_REQUEST[ 'quick-search-taxonomy-' . $taxonomy_name ] ) ) {
                $searched       = esc_attr( $_REQUEST[ 'quick-search-taxonomy-' . $taxonomy_name ] );
                $search_results = get_terms( $taxonomy_name, array(
                    'name__like'   => $searched,
                    'fields'       => 'all',
                    'orderby'      => 'count',
                    'order'        => 'DESC',
                    'hierarchical' => false
                ) );
            } else {
                $searched       = '';
                $search_results = array();
            }
            ?>
            <p class="quick-search-wrap">
                <input type="search" class="quick-search input-with-default-title" title="<?php esc_attr_e( 'Search' ); ?>" value="<?php echo $searched; ?>" name="quick-search-taxonomy-<?php echo $taxonomy_name; ?>"/>
                <span class="spinner"></span>
                <?php submit_button( __( 'Search' ), 'button-small quick-search-submit button-secondary hide-if-js', 'submit', false, array( 'id' => 'submit-quick-search-taxonomy-' . $taxonomy_name ) ); ?>
            </p>

            <ul id="<?php echo $taxonomy_name; ?>-search-checklist" data-wp-lists="list:<?php echo $taxonomy_name ?>" class="categorychecklist form-no-clear">
                <?php if ( ! empty( $search_results ) && ! is_wp_error( $search_results ) ) : ?>
                    <?php
                    $args['walker'] = $walker;
                    echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $search_results ), 0, (object) $args );
                    ?>
                <?php elseif ( is_wp_error( $search_results ) ) : ?>
                    <li><?php echo $search_results->get_error_message(); ?></li>
                <?php elseif ( ! empty( $searched ) ) : ?>
                    <li><?php _e( 'No results found.' ); ?></li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.tabs-panel -->

        <p class="button-controls">
            <span class="list-controls">
                <a href="<?php
                echo esc_url( add_query_arg(
                    array(
                        $taxonomy_name . '-tab' => 'all',
                        'selectall'             => 1,
                    ),
                    remove_query_arg( $removed_args )
                ) );
                ?>#taxonomy-<?php echo $taxonomy_name; ?>" class="select-all"><?php _e( 'Select All' ); ?></a>
            </span>

            <span class="add-to-menu">
                <input type="submit"<?php wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e( 'Add to Menu' ); ?>" name="add-taxonomy-menu-item" id="<?php echo esc_attr( 'submit-taxonomy-' . $taxonomy_name ); ?>"/>
                <span class="spinner"></span>
            </span>
        </p>

    </div><!-- /.taxonomydiv -->
    <?php
}

?>
