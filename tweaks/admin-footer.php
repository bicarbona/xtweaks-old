<?php 

/**
*
*   Footer Text in Admin area
*
**/

function xtw_admin_footer_text( $text ) {
  
  return sprintf( __( '' ), 'http://#' );

}

add_filter( 'admin_footer_text', 'xtw_admin_footer_text' );


/**
*
*   Footer Version in Admin area
*
**/

function xtw_admin_footer_version() {

    // if ( ! current_user_can('manage_options') ) { // 'update_core' may be more appropriate
        remove_filter( 'update_footer', 'core_update_footer' ); 
    // }

}

add_action( 'admin_menu', 'xtw_admin_footer_version' );


/**
*
*   Footer Remove Admin Logo
*
**/

function xtw_footer_remove() {
 if ( ! current_user_can('manage_options') ) { // 'update_core' may be more appropriate
        remove_filter( 'update_footer', 'core_update_footer' ); 
    }
}
 
add_action( 'admin_menu', 'xtw_footer_remove' );

?>