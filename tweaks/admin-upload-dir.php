<?php 

/**
 * CPT upload dir
 * @param  [type] $dir [description]
 * @return [type]      [description]

*/

function xtw_upload_dir($dir) {
    // Lots of $_REQUEST usage in here, not a great idea.

    // Are we where we want to be?
    if (!isset($_REQUEST['action']) || 'upload-attachment' !== $_REQUEST['action']) {
        return $dir;
    }

    // make sure we have a post ID
    if (!isset($_REQUEST['post_id'])) {
        return $dir;
    }

    // modify the path and url.
    $type = get_post_type($_REQUEST['post_id']);
    $uploads = apply_filters("{$type}_upload_directory", $type);
    $dir['path'] = path_join($dir['basedir'], $uploads);
    $dir['url'] = path_join($dir['baseurl'], $uploads);

    return $dir;

      // error_log("path={$args['path']}");
      // error_log("url={$args['url']}");
      // error_log("subdir={$args['subdir']}");
      // error_log("basedir={$args['basedir']}");
      // error_log("baseurl={$args['baseurl']}");
      // error_log("error={$args['error']}"); 

    
}

if($options['opt-post-type-upload-dir']){
    //add_filter('upload_dir', 'xtw_upload_dir');
}


/**
*
* 2 version
*
**/

function wpse_16722_type_upload_dir( $args ) {

    // Get the current post_id
    $id = ( isset( $_REQUEST['post_id'] ) ? $_REQUEST['post_id'] : '' );

    if( $id ) {    
       // Set the new path depends on current post_type
       $newdir = '/' . get_post_type( $id );

       $args['path']    = str_replace( $args['subdir'], '', $args['path'] ); //remove default subdir
       $args['url']     = str_replace( $args['subdir'], '', $args['url'] );      
       $args['subdir']  = $newdir;
       $args['path']   .= $newdir; 
       $args['url']    .= $newdir; 

       return $args;

      // error_log("path={$args['path']}");
      // error_log("url={$args['url']}");
      // error_log("subdir={$args['subdir']}");
      // error_log("basedir={$args['basedir']}");
      // error_log("baseurl={$args['baseurl']}");
      // error_log("error={$args['error']}"); 

   }
}
// add_filter( 'upload_dir', 'wpse_16722_type_upload_dir' );


/**
*
* 3 version
*
**/

// Change the upload path to the one we want
function spacepad_pre_upload($file){
    add_filter('upload_dir', 'spacepad_custom_upload_dir');
    return $file;
}

// Change the upload path back to the one WordPress uses by default
function spacepad_post_upload($fileinfo){
    remove_filter('upload_dir', 'spacepad_custom_upload_dir');
    return $fileinfo;
}

function spacepad_custom_upload_dir($path){    
  
  /*
  * Determines if uploading from inside a Post / Page / CPT - if not, default Upload folder is used
  */
  
    $use_default_dir = ( isset($_REQUEST['post_id'] ) && $_REQUEST['post_id'] == 0 ) ? true : false; 
  
    if( !empty( $path['error'] ) || $use_default_dir )
        return $path; //error or uploading not from a Post / Page / CPT
  
  /*
  * Save uploads in SLUG based folders 
  */

  $the_cat = get_the_category( $_REQUEST['post_id'] );
  
  // If there is a category lets use that for our organization
  if ( $the_cat ){
  
    $customdir = '/' . strtolower(str_replace(" ", "-", $the_cat[0]->cat_name));
  
  } else {
  
    $post_data = get_post($_REQUEST['post_id'], ARRAY_A);
    
    $post_slug = $post_data['post_name'];
    
    $post_type_slug = get_post_type( $_REQUEST['post_id'] );
  
    //$terms = get_the_terms( $post->ID, 'your-taxonomy' );

    $customdir = '/' . $post_type_slug .'/' .$post_slug;  
  
  }

    $path['path']    = str_replace($path['subdir'], '', $path['path']); //remove default subdir (year/month)
    $path['url']     = str_replace($path['subdir'], '', $path['url']);      
    $path['subdir']  = $customdir;
    $path['path']   .= $customdir; 
    $path['url']    .= $customdir;  

    return $path;
}

add_filter('wp_handle_upload_prefilter', 'spacepad_pre_upload', 2);
add_filter('wp_handle_upload', 'spacepad_post_upload', 2);

?>