<?php

/**
*
*   Fixed Sidebar
*
**/

 function xtw_fixed_sidebar_script() {
    global $options;

        // Register scripts
        wp_register_script('fixed-sidebar', plugins_url( '../lib/js/fixed-sidebar.js', __FILE__ ),false,'', true);

        wp_enqueue_script( 'fixed-sidebar' );

        // Localize .js script with  options
        $fixed_sidebar_translation_array = array( 
            'opt_sidebar_fixed_id' => $options['opt-sidebar-fixed-id'] ,
        );

        wp_localize_script( 'fixed-sidebar', 'fixed_sidebar', $fixed_sidebar_translation_array );
}

   if($options['opt-sidebar-fixed']){
        add_action( 'wp_enqueue_scripts', 'xtw_fixed_sidebar_script' );
   }

 ?>