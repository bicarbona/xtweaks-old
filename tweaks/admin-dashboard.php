<?php 

/**
*
*   Core Dashboard Page
*   xtw_dashboard_widgets
*   @return [?] [unset widget]
*
**/

if(!function_exists('xtw_dashboard_widgets')){

    function xtw_dashboard_widgets(){
        global $options;
        global $wp_meta_boxes;

        foreach ($options['opt-admin-dashboard-widget2']['disabled'] as $key => $option) {

            switch ($key) {
                case 'dashboard_right_now':
                case 'network_dashboard_right_now':
                case 'dashboard_activity':
//              case 'dashboard_recent_comments':
//              case 'dashboard_incoming_links':
//              case 'dashboard_plugins':
                    unset($wp_meta_boxes['dashboard']['normal']['core'][$key]);
                    break;
                case 'dashboard_primary':
//              case 'dashboard_secondary':
                case 'dashboard_quick_press':
//              case 'dashboard_recent_drafts':
                    unset($wp_meta_boxes['dashboard']['side']['core'][$key]);
                    break;
                case 'woocommerce_dashboard_status':
                case 'woocommerce_dashboard_recent_reviews':
                    unset($wp_meta_boxes['dashboard']['normal']['core'][$key]);
                    break;
                case 'redux_dashboard_widget':
                    unset($wp_meta_boxes['dashboard']['side']['high'][$key]);
                    break;

                case 'wpseo-dashboard-overview':
                    unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);
                    break;

                case 'pb_backupbuddy_stats':
                    unset($wp_meta_boxes['dashboard']['normal']['core']['pb_backupbuddy_stats'] );
                    break;

            }// switch

        } // foreach

    } //function
    
}  //function exist

add_action('wp_dashboard_setup', 'xtw_dashboard_widgets', 999);


/**
*
*   Custom Dashboard Page Loader
* 
**/

class xtw_custom_dashboard {

    /*--------------------------------------------*
     * Constructor
     *--------------------------------------------*/
 
    /**
     * Initializes the plugin by setting localization, filters, and administration functions.
     */
    function __construct() {
    
        add_action('admin_menu', array( &$this,'xtw_register_menu') );
        add_action('load-index.php', array( &$this,'xtw_redirect_dashboard') );
 
    } // end constructor
 
    function xtw_redirect_dashboard() {
    
        if( is_admin() ) {
            $screen = get_current_screen();
            
            if( $screen->base == 'dashboard' ) {

                wp_redirect( admin_url( 'index.php?page=dashboard' ) );
                
            }
        }
    }
    
    function xtw_register_menu() {
        add_dashboard_page( 'Dashboard', 'Dashboard', 'read', 'dashboard', array( &$this,'xtw_create_dashboard') );
    }
    
    function xtw_create_dashboard() {
        include_once( 'dashboard-page/dashboard-page.php' );
    }

}

global $options;

if( $options['opt-admin-dashboard-custom-page'] ){

    $GLOBALS['sweet_custom_dashboard'] = new xtw_custom_dashboard();

}
// instantiate plugin's class

?>