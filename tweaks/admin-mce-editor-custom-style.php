<?php
# https://shellcreeper.com/complete-guide-to-style-format-drop-down-in-wp-editor/
# https://www.gavick.com/blog/wordpress-tinymce-custom-buttons#tc-section-2
if (is_admin()) {

/**
*
* rearrange 1 and 2 row
*
**/

if (isset($wp_version)) {
  add_filter("mce_buttons", "extended_editor_mce_buttons", 0);
  add_filter("mce_buttons_2", "extended_editor_mce_buttons_2", 0);
}

function extended_editor_mce_buttons($buttons) {
return array(
 "formatselect", "bold", "italic", "underline", "code", "strikethrough", "bullist", "numlist", "blockquote", "alignleft", "aligncenter", "alignright", "link", "unlink", 'wp_adv', 'fullscreen' );
}

function extended_editor_mce_buttons_2($buttons) {
// the second toolbar line
return array(
  'hr', "subscript", "subscript", "charmap");
}


/*
 * The TinyMCE "Code" button is not included with WP by default
 */
// function my_mce_external_plugins($plugins) {   

//     $plugins['code'] = get_stylesheet_directory_uri() . '/code/plugin.min.js';
//     return $plugins;
// }
// add_filter('mce_external_plugins', 'my_mce_external_plugins');

# "undo", "redo", 'pastetext', 'removeformat'
# -nefunguje  "code", "search", "replace", "anchor",


/**
*
* Apply styles to the visual editor
*
**/ 

add_filter('mce_css', 'xtw_mcekit_editor_style');

function xtw_mcekit_editor_style($url) {
 
    if ( !empty($url) )
        $url .= ',';
 
    // Retrieves the plugin directory URL
    $url .= trailingslashit( plugin_dir_url(__FILE__) ) . '../less/admin/admin-editor.less';
 
    return $url;
}

/**
*
* Add styles/classes to the "Styles" drop-down
*
**/

add_filter( 'tiny_mce_before_init', 'xtw_mce_before_init' );
 
function xtw_mce_before_init( $settings ) {
 
    $style_formats = array(
      array(
          'title' => 'Download Link',
          'selector' => 'a',
          'classes' => 'download'
      ),
      
      array(
          'title' => 'Testimonial',
          'selector' => 'p',
          'classes' => 'testimonial',
      ),
      
      array(
          'title' => 'Warning Box',
          'block' => 'div',
          'classes' => 'warning box',
          'wrapper' => true
      ),

      array(
          'title' => 'UL disc', 
          'selector' => 'ul', 
          'classes' => 'disc' 
      ),

      array(
          'title' => 'UL square', 
          'selector' => 'ul', 
          'classes' => 'square'
      ),

      array(
          'title' => 'UL simple', 
          'selector' => 'ul', 
          'classes' => 'simple-list'
      ),

      array(
          'title' => 'IMG circle', 
          'selector' => 'img', 
          'classes' => 'img-circle'
      ),

      array(
        'title' => 'Animate - Fade In',
        'selector' => '*',
        'classes' => 'animated fadeIn',
      ),

      array(
        'title' => 'Animate - Fade In Left',
        'selector' => '*',
        'classes' => 'animated fadeInLeft',
      ),

      array(
        'title' => 'Animate - Fade In Right',
        'selector' => '*',
        'classes' => 'animated fadeInRight',
      ),
	  
      array(
       'title' => 'Uppercase',
       'selector' => '*',
       'classes' => 'uppercase',
      ),

      array(
         'title' => 'Red Uppercase Text',
            'inline' => 'span',
            'styles' => array(
                'color' => '#ff0000',
                'fontWeight' => 'bold',
                'textTransform' => 'uppercase'
            ),
      )
    );
        
    // $settings['style_formats_merge'] = true; // ponecha aj default WP > h1 - h6, Bold Italic, 
    // mozno v buducnosti do prveho riadku a merge
  
    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;
 
}
 
/* Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats */

//add_action('wp_enqueue_scripts', 'tuts_mcekit_editor_enqueue');
 
/**
Style Format Options

title [required]              label for this dropdown item

selector|block|inline         selector limits the style to a specific HTML
[required]                    tag, and will apply the style to an existing tag
                              instead of creating one
                              
                              block creates a new block-level element with the
                              style applied, and will replace the existing block
                              element around the cursor
                              
                              inline creates a new inline element with the style
                              applied, and will wrap whatever is selected in the
                              editor, not replacing any tags

classes [optional]            space-separated list of classes to apply to the
                              element

styles [optional]             array of inline styles to apply to the element
                              (two-word attributes, like font-weight, are written
                              in Javascript-friendly camel case: fontWeight)

attributes [optional]         assigns attributes to the element (same syntax as styles)

wrapper [optional,            if set to true, creates a new block-level element
default = false]              around any selected block-level elements

exact [optional,              disables the “merge similar styles” feature, needed
default = false]              for some CSS inheritance issues

**/

/**
 * Add "Styles" drop-down
 */

// function tuts_mce_editor_buttons( $buttons ) {
//     array_unshift( $buttons, 'styleselect' );
//     return $buttons;
// }
//  tuts_mce_editor_buttons();


/**
* ONLY FOR DEBUG  
**/

# row 1
function loop_tinymce_buttons( $buttons ){
  //Loop through all buttons for mce_buttons hook
  //echoes on the dashboard, for debugging purposes only!
  echo '<strong>mce_buttons hook</strong><br/>';
  foreach( $buttons as $button ){
    echo $button . '</br>';
  }
 }

// add_filter( 'mce_buttons','loop_tinymce_buttons' );

# row 2 
function loop_tinymce2_buttons( $buttons ){
  //Loop through all buttons for mce_buttons_2 hook
  //echoes on the dashboard, for debugging purposes only!
  echo '<strong>mce_buttons_2 hook</strong><br/>';
  foreach( $buttons as $button ){
    echo $button . '</br>';
  }
 }
 
// add_filter( 'mce_buttons_2','loop_tinymce2_buttons' );


} // end if is_admin




?>