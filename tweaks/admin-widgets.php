<?php 

if (is_admin()) {

/**
*
* Manage Default  Widgets
* xtw_default_widgets description
* @return [?] [unset widget]
*
*/

if(!function_exists('xtw_default_widgets')){

    function xtw_default_widgets(){
        global $options;

       foreach ($options['opt-admin-default-widget']['disabled'] as $key => $option) {

            switch ($key) {
                case 'WP_Widget_Pages':
                    unregister_widget('WP_Widget_Pages');
                break;

                case 'WP_Widget_Calendar':
                    unregister_widget('WP_Widget_Calendar');
                break;

                case 'WP_Widget_Archives':
                    unregister_widget('WP_Widget_Archives');
                break;

                case 'WP_Widget_Links':
                    unregister_widget('WP_Widget_Links');
                break;

                case 'WP_Widget_Meta':
                    unregister_widget('WP_Widget_Meta');
                break;

                case 'WP_Widget_Search':
                    unregister_widget('WP_Widget_Search');
                break;

                case 'WP_Widget_Text':
                    unregister_widget('WP_Widget_Text');
                break;    
                               
                case 'WP_Widget_Categories':
                    unregister_widget('WP_Widget_Categories');
                break;

                case 'WP_Widget_Recent_Posts':
                    unregister_widget('WP_Widget_Recent_Posts');
                break;

                case 'WP_Widget_Recent_Comments':
                    unregister_widget('WP_Widget_Recent_Comments');
                break;

                case 'WP_Widget_RSS':
                    unregister_widget('WP_Widget_RSS');
                break;

                case 'WP_Widget_Tag_Cloud':
                    unregister_widget('WP_Widget_Tag_Cloud');
                break;

                case 'WP_Nav_Menu_Widget':
                    unregister_widget('WP_Nav_Menu_Widget');
                break;

                case 'WC_Widget_Recent_Products':
                    unregister_widget('WC_Widget_Recent_Products');
                break;

// -----
                case 'WC_Widget_Featured_Products':
                    unregister_widget('WC_Widget_Featured_Products');
                break;

                case 'WC_Widget_Product_Categories':
                    unregister_widget('WC_Widget_Product_Categories');
                break;
// -----
                case 'WC_Widget_Product_Tag_Cloud':
                    unregister_widget('WC_Widget_Product_Tag_Cloud');
                break;

                case 'WC_Widget_Cart':
                    unregister_widget('WC_Widget_Cart');
                break;                                

                case 'WC_Widget_Layered_Nav':
                    unregister_widget('WC_Widget_Layered_Nav');
                break;

                case 'WC_Widget_Layered_Nav_Filters':
                    unregister_widget('WC_Widget_Layered_Nav_Filters');
                break;  

                case 'WC_Widget_Price_Filter':
                    unregister_widget('WC_Widget_Price_Filter');
                break;

                case 'WC_Widget_Product_Search':
                    unregister_widget('WC_Widget_Product_Search');
                break;                                

                case 'WC_Widget_Top_Rated_Products':
                    unregister_widget('WC_Widget_Top_Rated_Products');
                break;

                case 'WC_Widget_Recent_Reviews':
                    unregister_widget('WC_Widget_Recent_Reviews');
                break; 

                case 'WC_Widget_Recently_Viewed':
                    unregister_widget('WC_Widget_Recently_Viewed');
                break;

                case 'WC_Widget_Best_Sellers':
                    unregister_widget('WC_Widget_Best_Sellers');
                break;                                

                case 'WC_Widget_Onsale':
                    unregister_widget('WC_Widget_Onsale');
                break;

                case 'WC_Widget_Random_Products':
                    unregister_widget('WC_Widget_Random_Products');
                break;

                case 'WC_Widget_Products':
                    unregister_widget('WC_Widget_Products');
                break;

            }// switch

        } // foreach

    } //function

} //function exist

add_action('widgets_init','xtw_default_widgets');


/**
*
* Enable shortcodes in widgets
*
**/

if(!function_exists('xtw_shortcode_in_widgets')){
    function xtw_shortcode_in_widgets(){
        if ( !is_admin() ){
            add_filter('widget_text', 'do_shortcode', 11);
        } 
    }

    if($options['opt-shortcode-in-widget']){
        xtw_shortcode_in_widgets();
    }

}

} //is admin

?>