<?php

/**
*
* Clean Up <head>
*
**/

 if(!is_admin()){


if( !function_exists('xtw_remove_header_info' )){
    
    function xtw_remove_header_info() {
        remove_action( 'wp_head', 'rsd_link' );
        remove_action( 'wp_head', 'wlwmanifest_link' );
        remove_action( 'wp_head', 'wp_generator' ); // Remove WP Generator from Header
        remove_action( 'wp_head', 'start_post_rel_link' ); // Start link
        remove_action( 'wp_head', 'index_rel_link' ); // index link
        remove_action( 'wp_head', 'adjacent_posts_rel_link' ); // for WordPress <  3.0
        remove_action( 'wp_head', 'parent_post_rel_link' ); // prev link

        add_action('template_redirect', 'link_rel_buffer_start', -1);
        add_action('get_header', 'link_rel_buffer_start');
        add_action('wp_head', 'link_rel_buffer_end', 999);
    }

    function remove_links(){
                /** Work? **/
        remove_action('wp_head', 'feed_links', 2);  // Remove Post and Comment Feeds
        remove_action('wp_head', 'feed_links_extra', 3); // Remove category feeds
    }

    function link_rel_buffer_callback($buffer) {
        $buffer = preg_replace('/(<link.*?rel=("|\')pingback("|\').*?href=("|\')(.*?)("|\')(.*?)?\/?>|<link.*?href=("|\')(.*?)("|\').*?rel=("|\')pingback("|\')(.*?)?\/?>)/i', '', $buffer);
                return $buffer;
    }

    function link_rel_buffer_start() {
        ob_start("link_rel_buffer_callback");
    }

    function link_rel_buffer_end() {
        ob_flush();
    }

    if( $options['opt-remove-header-info'] ){
        add_action( 'init', 'xtw_remove_header_info' );
        add_action( 'wp_loaded', 'remove_links' );
    }

}

}
?>