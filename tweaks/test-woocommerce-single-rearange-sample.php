<?php 

/**
* woocommerce_before_single_product_summary hook
*
* @hooked woocommerce_show_product_sale_flash - 10
* @hooked woocommerce_show_product_images - 20
*/

/**
* woocommerce_single_product_summary hook
*
* @hooked woocommerce_template_single_title - 5
* @hooked woocommerce_template_single_price - 10
* @hooked woocommerce_template_single_excerpt - 20
* @hooked woocommerce_template_single_add_to_cart - 30
* @hooked woocommerce_template_single_meta - 40
* @hooked woocommerce_template_single_sharing - 50
*/

/**
* woocommerce_after_single_product_summary hook
*
* @hooked woocommerce_output_product_data_tabs - 10
* @hooked woocommerce_output_related_products - 20
*/


remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating' );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
Reorder Parts
**/

// Sumary

function add_to_cart(){
    add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart' );
}

function single_excerpt(){
    add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt' );
}

function single_price(){
    add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price' );
}

function single_title(){
    add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title' );
}

function single_rating(){
    add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating' );
}

function single_meta(){
 //  some code

}


$d = array(
   'single_title'      =>  'single_title',
    'add_to_cart'       =>  'add_to_cart',
    'single_price'      =>  'single_price',
    'single_excerpt'    =>  'single_excerpt',
    'single_rating'     =>  'single_rating',
    'single_meta'       =>  'single_meta',
);


global $options;

function order_wooparts($d){
    
    foreach ($d as $key => $value) {
        
        if ($key == 'placebo') continue;
        //print $value;
        call_user_func($key);
        
    }

}

$parts = $options['opt-woo-sortable']['kokot'];

order_wooparts($parts);



// Tabs


add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabss', 98 );

function woo_reorder_tabss( $tabs ) {

    /**
    Unset Tabs
    **/

    // // Remove the description tab
    // unset( $tabs['description'] );
    
    // // Remove the reviews tab
    // unset( $tabs['reviews'] );

    // // Remove the additional information tab
    // unset( $tabs['additional_information'] );

    /**
    Reorder Tabs
    **/

    // Reviews first
    if ( $tabs['reviews']['priority'] ) {
        $tabs['reviews']['priority'] = 30;           
    }
    
    // Description second
    if ( $tabs['description']['priority'] ) {
        $tabs['description']['priority'] = 10;          
    }
    
    // Additional information third
    if( $tabs['additional_information']['priority'] ){
        $tabs['additional_information']['priority'] = 5;   
    }

    return $tabs;
}   



/**
*
* Disable Update Notice Woocommerce
*
*/
remove_action( 'admin_notices', 'woothemes_updater_notice' );

?>