<?php 

/**
* 
* Hide Admin Notices - On post / CPT publish page  
* Post Published. View Post / Saved ... etc 
*
**/

if (is_admin()){
    
function xtw_admin_post_messages( $messages ) {

global $options;

    if(!$options['opt-disable-messages-updated-draft']) {
        unset($messages['post'][10]); // Updated este pred publikovanim / Draft
    }

    // Updated publikovaneho clanku / aj status viditelny, verejny...
    if (!$options['opt-disable-messages-updated-published']) {
        unset($messages['post'][1]);  
    }

    if (!$options['opt-disable-messages-publish']) {
        unset($messages['post'][6]);  // Publikovanie
    }

    if (!$options['opt-disable-messages-updated-shedule']) {
        unset($messages['post'][9]);  // Shedule
    }

    if (!$options['opt-disable-messages-restore-revision']) {
        unset($messages['post'][5]);  // Restorovanie revizie
    }

    return $messages;
}

add_filter( 'post_updated_messages', 'xtw_admin_post_messages' );


/**
*
* Post Revisions
*
**/

if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', $options['opt-post-revisions-limit']);

/**
*
* Disable Autosave
*
**/

if(!function_exists('xtw_disable_auto_save')){
    function xtw_disable_auto_save(){
        wp_deregister_script('autosave');  
    }

    if($options['opt-disable-autosave']){
        add_action( 'wp_print_scripts', 'xtw_disable_auto_save' );
    }

}

/**
*
* Autosave Interval
*
**/

if(!function_exists('xtw_autosave_interval')){
    
    function xtw_autosave_interval(){
    global $options;

        define( 'AUTOSAVE_INTERVAL', $options['opt-autosave-interval'] );
    }

    xtw_autosave_interval();
}

/**
*
* Publish View
* Adds a button so you can Publish and View Pages, Posts etc. in one step
*
**/
     
function xtw_publish_view_submitbox_start(){
    global $post;
    global $wp_version;
    
    if($post->post_type != 'acf') {
        
        if($post->post_status == 'auto-draft' || $post->post_status == 'draft') {
            submit_button('&#xf177;','','',false, array('onclick'=>"jQuery(this).after('<input type=\"hidden\" name=\"publishview\" value=\"Y\" />')",'title'=>'Publish & View','id'=>'publishview'));
        } else if($post->post_status == 'publish') {
            submit_button('&#xf177;','','',false, array('onclick'=>"jQuery(this).after('<input type=\"hidden\" name=\"publishview\" value=\"Y\" />')",'title'=>'Update & View','id'=>'publishview'));
        }
    }
}

add_action( 'post_submitbox_start', 'xtw_publish_view_submitbox_start' );

function xtw_publish_view_redirect($location) {
    global $post;
    if (isset($_POST['publishview'])) {
        $location = get_permalink($post->ID);
    }
    return $location;
}

add_filter('redirect_post_location', 'xtw_publish_view_redirect');

} // is admin
?>