<?php
if (is_admin()) {

global $options;

/**
*
*   Put Separators Back In Admin Menu
*
**/

function xtw_separators() {
   echo '<style type="text/css">
        #adminmenu li.wp-menu-separator {margin: 0;}
        #adminmenu li.wp-menu-separator {
          height: 1px;
        background-color: rgba(0,0,0,0.10);
        }
        </style>';
}

add_action( 'admin_head', 'xtw_separators' );


/** 
*
*   Remove Menu
*
**/

function xtw_remove_tools() {
    //remove_menu_page( 'tools.php' );
}

add_action( 'admin_menu', 'xtw_remove_tools', 99 );


/**
*
*   Admin Submenu
*
**/

function xtw_remove_admin_submenus() {
if ( !is_admin())
    return;
    global $submenu;
    
    unset($submenu['index.php'][10]); // Removes Updates
    unset($submenu['index.php'][0]); //  Hide Admin dashboard submenu to self 
    // //Posts menu
    // unset($submenu['edit.php'][5]); // Leads to listing of available posts to edit
    // unset($submenu['edit.php'][10]); // Add new post
    // unset($submenu['edit.php'][15]); // Remove categories
    // unset($submenu['edit.php'][16]); // Removes Post Tags
    // //Media Menu
    // unset($submenu['upload.php'][5]); // View the Media library
    // unset($submenu['upload.php'][10]); // Add to Media library
    // //Links Menu
    // unset($submenu['link-manager.php'][5]); // Link manager
    // unset($submenu['link-manager.php'][10]); // Add new link
    // unset($submenu['link-manager.php'][15]); // Link Categories
    // //Pages Menu
    // unset($submenu['edit.php?post_type=page'][5]); // The Pages listing
    // unset($submenu['edit.php?post_type=page'][10]); // Add New page
    // //Appearance Menu
    // unset($submenu['themes.php'][5]); // Removes 'Themes'
    // unset($submenu['themes.php'][7]); // Widgets
    // unset($submenu['themes.php'][15]); // Removes Theme Installer tab
    // //Plugins Menu
    // unset($submenu['plugins.php'][5]); // Plugin Manager
    // unset($submenu['plugins.php'][10]); // Add New Plugins
    // unset($submenu['plugins.php'][15]); // Plugin Editor
    // //Users Menu
    // unset($submenu['users.php'][5]); // Users list
    // unset($submenu['users.php'][10]); // Add new user
    // unset($submenu['users.php'][15]); // Edit your profile
    // //Tools Menu
    // unset($submenu['tools.php'][5]); // Tools area
    // unset($submenu['tools.php'][10]); // Import
    // unset($submenu['tools.php'][15]); // Export
    // unset($submenu['tools.php'][20]); // Upgrade plugins and core files
    // //Settings Menu
    // unset($submenu['options-general.php'][10]); // General Options
    // unset($submenu['options-general.php'][15]); // Writing
    // unset($submenu['options-general.php'][20]); // Reading
    // unset($submenu['options-general.php'][25]); // Discussion
    // unset($submenu['options-general.php'][30]); // Media
    // unset($submenu['options-general.php'][35]); // Privacy
    // unset($submenu['options-general.php'][40]); // Permalinks
    // unset($submenu['options-general.php'][45]); // Misc
}

add_action('admin_menu', 'xtw_remove_admin_submenus', 999);


/**
*
*   Show Draft Count in Menu
*
**/

function xtw_show_pending_number($menu) {

    /** @TODO add redux  opt **/
    $types = array("post", "page", "zamestnanec", 'praca');
    
    /** @TODO add redux opt **/

    $status = "draft"; // pending
    
    foreach($types as $type) {
        $num_posts = wp_count_posts($type, 'readable');
        $pending_count = 0;
        if (!empty($num_posts->$status)) $pending_count = $num_posts->$status;

        if ($type == 'post') {
            $menu_str = 'edit.php';
        } else {
            $menu_str = 'edit.php?post_type=' . $type;
        }

        foreach( $menu as $menu_key => $menu_data ) {
            if( $menu_str != $menu_data[2] )
                continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
            }
        }
    return $menu;
}

add_filter('add_menu_classes', 'xtw_show_pending_number');
}

?>