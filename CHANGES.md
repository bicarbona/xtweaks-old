### 29.10 2015

+ woocommerce columns 

### 14.10.2015
+ zopar admin tweaks
+ vycistenie adresarovej struktury 
+ v Git repozitari odobrane vyvojarske adresare '# folders'
+ presunutie ReduxFramework - teraz ako vlastny plugin 
+ pridane debug.php

### 24.2. 2015
+ LESS compliler compress

#### 6.3. 2015
+ change structure

#### 7.3 2015
file: redux-config-admin.php

+ opt-disable-self-ping
+ opt-post-revisions-limit
+ opt-remove-wp-generator
+ .block{
	min-height:0!important;
}

#### 13.4. 2015



### 26.8.2015
+ Fix Admin Bar - Sitename (skrati sitename)
+ define('INC_LESS', true); //  * Vypnutie / zapnutie LESS
+ premenovana globalna premenna redux_demo  =>  redux_tweaks
+ Scroll Back to Top
+ Sticky Menu
+ plugin install TGM-Plugin-Activation
+ Clean up the <head>
+ Disable WP emoji / na pevno
+ Category rewrites  - remove **/category**
+ CPT 
	+ Bloky
	+ Skúsenosti
	+ Portfólio
	+ Slider
	+ Poradňa
	+ FAQ
	+ Denník
	+ Recepty
	+ Mýty a pravdy
+ Hide Dashboard Widgets
+ Hide Widgets
+ Enable Shortcode in Widget
+ Disable Posts Auto Saving
+ Autosave interval
+ Disable HTML in comments
+ Post Revisions (limit)