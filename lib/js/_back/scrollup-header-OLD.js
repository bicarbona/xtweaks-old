jQuery(document).ready(function($) {
	var previousScroll = 0,
		navBarOrgOffset = jQuery(scrollup_name.scrollup_wrapper).offset().top;
	//grid-container
	//#block-blg55e5c419d6095
	jQuery(scrollup_name.scrollup_wrapper).height(jQuery(scrollup_name.scrollup_wrapper+'.grid-container').height());

	jQuery(window).scroll(function() {
		var currentScroll = jQuery(this).scrollTop();
	   // console.log(currentScroll + " and " + previousScroll + " and " + navBarOrgOffset);
		if(currentScroll > navBarOrgOffset) {
			if (currentScroll > previousScroll) {
				jQuery(scrollup_name.scrollup_wrapper).fadeOut();
			} else {
			

				jQuery(scrollup_name.scrollup_wrapper).fadeIn();
				//var explode = function(){

				jQuery(scrollup_name.scrollup_wrapper).addClass('fixed');
			
				//};

				//setTimeout(explode, 2000);

			}
		} else {
			 jQuery(scrollup_name.scrollup_wrapper).removeClass('fixed');   
		}
		previousScroll = currentScroll;
	});
	
// if (admin_debug !== undefined && admin_debug.enabled) {
//	console.debug(scrollup_name.scrollup_wrapper);
	
});