
jQuery( document ).ready(function($) {
//	console.debug( "ready!" );

// http://www.jacklmoore.com/colorbox/faq/

function openColorBox(){

		var	autoClose = true;

			jQuery.colorbox({
				html: modal_popup.html,
				transition: 'none', // "elastic", "fade", or "none". 
			//	width: modal_popup.width,
				width: '80%',

			//	height: modal_popup.height,
				maxWidth:'60%',
				maxHeight:'50%',

				escKey: modal_popup.escKey,
				loop: false,
				scrolling: false,
		//		scrolling: modal_popup.scrolling,
				opacity: modal_popup.opacity, //The overlay opacity level. Range: 0 to 1.
				fadeOut: 450, //Sets the fadeOut speed, in milliseconds, when closing Colorbox. 
				overlayClose: modal_popup.overlayClose,
				closeButton: modal_popup.closeButton, // Set to false to remove the close button.
				className: modal_popup.className, // Adds a given class to colorbox and the overlay. 
				close: modal_popup.closeButtonText,
				//==============================
				iframe: false,
				//href: "http://option/?p=14",
				//==============================
				// onClosed:function(){ alert('onClosed: colorbox has completely closed'); },
				onClosed:function(){ set_cookie(); },
				onComplete: function() {
					jQuery.colorbox.resize({width:"60%"});
				},

			});
			
			// resize colorbox on screen rotate in mobile devices and set to cover 90% of screen
			jQuery(window).resize(function() {
				jQuery.colorbox.resize({width:"60%"});
			});
			
			// Auto Close Popup
			if (modal_popup.autoClose) {
			//	console.debug('autoclose = ', 'ok' ); // print "10"
				setTimeout(jQuery.colorbox.close, modal_popup.timeoutAutoClose);
			}

		}

		function set_cookie(){
			
			var date = new Date();
			var minutes = modal_popup.cookieExpire; 
			date.setTime(date.getTime() + (minutes * 60 * 1000)); // 30 minutes is 30 * 60 * 1000 miliseconds

			jQuery.cookie(modal_popup.cookieKey, true, { expires: date, path: '/' });
		};

		setTimeout(function(){

			var hasVisitedBefore = jQuery.cookie(modal_popup.cookieKey);

			// if(!hasVisitedBefore){ // ak nebola nastavena susienka otvorim modal 
				openColorBox();
			 //}
			
			}, modal_popup.timeoutOpen // Auto Open Timeout
		);
		
		// Stop browser scrolling when Popup is Open
		$(document).bind('cbox_open', function() {
			$('html').css({ overflow: 'hidden' });
		}).bind('cbox_closed', function() {
			$('html').css({ overflow: '' });
		});


});


//
//(function ($){
//
//	$(function (){
//
//		/* Disable Colorbox on mobile devices */
//
//		$mobile_colorbox();
//
//		$(window).resize(function () {
//			$mobile_colorbox()
//		});
//
//	});
//
//	$mobile_colorbox = function ()
//	{
//		if ( $(window).width() <= 767 ) {
//			$('.colorbox').colorbox.remove();
//		} else {
//			$('.colorbox').colorbox({rel:'colorbox'});
//		}            
//	}
//	
//	
//
//})(jQuery);

// console.debug('brekeke = ', jQuery.cookie('hasVisitedBefore')); // print "10"