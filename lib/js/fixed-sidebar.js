
var sidebar_fixed_id = '#' + fixed_sidebar.opt_sidebar_fixed_id;

jQuery( document ).ready( function( $ ) { 

	$(".show-hide-sidebar").click(function(e) {
		$(sidebar_fixed_id).toggleClass("toggle-sidebar");
        e.preventDefault(); // Cancel the default action (navigation) of the click.
	});
	
	$(".remove").click(function(e) {
		$(sidebar_fixed_id).removeClass("toggle-sidebar");
		e.preventDefault(); // Cancel the default action (navigation) of the click.
	});
	
	// stop scrolling page whlile scroll div ....
	$( sidebar_fixed_id ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
	    var e0 = e.originalEvent,
	        delta = e0.wheelDelta || -e0.detail;
	        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
	    e.preventDefault(); // Cancel the default action (navigation) of the click.
	});

});

// console.log( "sidebar_fixed_id", sidebar_fixed_id);
