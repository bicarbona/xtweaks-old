<?php
/**
Recipes
*/
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'icon'       => 'el-icon-website',
    'title'      => __( 'Recepty', 'redux-framework-demo' ),
    'submenu' => false,
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'opt-cpt-recipes',
            'type'     => 'switch', 
            'title'    => 'Recepty',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),
        
        array(
            'required' => array( 'opt-cpt-recipes', '=', true ),
            'id'       => 'opt-cpt-recipes-slug',
            'type'     => 'text',
            'title'    => __('Slug', 'redux-framework-demo'),
            'default'  => 'recipes',
            'ajax_save' => true
        ),

        array(
            'required' => array( 'opt-cpt-recipes', '=', true ),
            'id'       => 'opt-cpt-recipes-name-singular',
            'type'     => 'text',
            'title'    => __('Name Singular', 'redux-framework-demo'),
            'default'  => 'Recept',
            'ajax_save' => true
        ),

        array(
            'required' => array( 'opt-cpt-recipes', '=', true ),
            'id'       => 'opt-cpt-recipes-name-plural',
            'type'     => 'text',
            'title'    => __('Name Plural', 'redux-framework-demo'),
            'default'  => 'Recept',
            'ajax_save' => true
        ),

        array(
            'required' => array( 'opt-cpt-recipes', '=', true ),
            'id'               => 'opt-cpt-recipes-has-archive',
            'type'             => 'switch',
            'title'            => __('Has Archive', 'redux-framework-demo'), 
           // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
            'default'          => false,
        ),

        array(
            'required' => array( 'opt-cpt-recipes', '=', true ),
            'id'       => 'opt-cpt-recipes-menu-position',
            'type'     => 'spinner', 
            'title'    => 'Menu Position',
            'ajax_save' => true,
            'default'  => '10',
            'min'      => '1',
            'step'     => '1',
            'max'      => '30',
        ),
    )
)
);

?>