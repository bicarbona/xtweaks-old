<?php

/**
Skusenosti
 */
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Skúsenosti', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'opt-cpt-experiences',
                'type'     => 'switch', 
                'title'    => 'Skúsenosti',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),
            array(
                'required' => array( 'opt-cpt-experiences', '=', true ),
                'id'       => 'opt-cpt-experiences-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'skusenost'
            ),

            array(
                'required' => array( 'opt-cpt-experiences', '=', true ),
                'id'       => 'opt-cpt-experiences-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'Skúsenosť',
            ),

            array(
                'required' => array( 'opt-cpt-experiences', '=', true ),
                'id'       => 'opt-cpt-experiences-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'Skúsenosti',
            ),

            array(
                'required' => array( 'opt-cpt-experiences', '=', true ),
                'id'               => 'opt-cpt-experiences-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-experiences', '=', true ),
                'id'       => 'opt-cpt-experiences-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )
    )
);

?>