<?php

/**
    Subsection CPT
    Popup
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Popup', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
        
            array(
                'id'       => 'opt-cpt-popup',
                'type'     => 'switch', 
                'title'    => 'Popup',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),

            array(
                'required' => array( 'opt-cpt-popup', '=', true ),
                'id'       => 'opt-cpt-popup-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'default'  => 'popup',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-popup', '=', true ),              
                'id'       => 'opt-cpt-popup-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'default'  => 'Popup',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-popup', '=', true ),
                'id'       => 'opt-cpt-popup-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'default'  => 'Popup',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-popup', '=', true ),
                'id'               => 'opt-cpt-popup-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-popup', '=', true ),
                'id'       => 'opt-cpt-popup-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )
    )
);

?>