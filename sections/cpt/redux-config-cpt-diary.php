<?php
/**
Dennik
 */
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Denník', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'opt-cpt-diarys',
                'type'     => 'switch', 
                'title'    => 'Diary',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),

            array(
                'required' => array( 'opt-cpt-diarys', '=', true ),
                'id'       => 'opt-cpt-diarys-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'default'  => 'diarys',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-diarys', '=', true ),
                'id'       => 'opt-cpt-diarys-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'default'  => 'Denník',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-diarys', '=', true ),
                'id'       => 'opt-cpt-diarys-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'default'  => 'Denník',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-diarys', '=', true ),
                'id'               => 'opt-cpt-diarys-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-diarys', '=', true ),
                'id'       => 'opt-cpt-diarys-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )
    )
);

?>