<?php

/**
    Subsection CPT
    Portfolio
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Portfólio', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'opt-cpt-portfolio',
                'type'     => 'switch', 
                'title'    => 'Portfólio',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),

            array(
                'required' => array( 'opt-cpt-portfolio', '=', true ),
                'id'       => 'opt-cpt-portfolio-menu-name',
                'type'     => 'text',
                'title'    => __('Menu Name', 'redux-framework-demo'),
                'default'  => 'Portfólio',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-portfolio', '=', true ),
                'id'       => 'opt-cpt-portfolio-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'default'  => 'portfolio',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-portfolio', '=', true ),
                'id'       => 'opt-cpt-portfolio-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'Portfólio',
            ),

            array(
                'required' => array( 'opt-cpt-portfolio', '=', true ),
                'id'       => 'opt-cpt-portfolio-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'Portfólio',
            ),

            array(
                'required' => array( 'opt-cpt-portfolio', '=', true ),
                'id'               => 'opt-cpt-portfolio-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-portfolio', '=', true ),
                'id'       => 'opt-cpt-portfolio-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )  
    )
);

?>