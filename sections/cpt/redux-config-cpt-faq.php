<?php

/**
    Subsection CPT
    FAQ
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'FAQ', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'opt-cpt-faqs',
                'type'     => 'switch', 
                'title'    => 'Faq',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),
            array(
                'required' => array( 'opt-cpt-faqs', '=', true ),
                'id'       => 'opt-cpt-faqs-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'default'  => 'faq',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-faqs', '=', true ),
                'id'       => 'opt-cpt-faqs-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'default'  => 'FAQ',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-faqs', '=', true ),
                'id'       => 'opt-cpt-faqs-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'default'  => 'FAQ',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-faqs', '=', true ),
                'id'               => 'opt-cpt-faqs-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-faqs', '=', true ),
                'id'       => 'opt-cpt-faqs-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )
    )
);

?>