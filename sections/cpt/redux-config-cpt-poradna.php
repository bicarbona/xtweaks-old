<?php

/**
    Subsection CPT
    Poradna
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Poradňa', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'opt-cpt-talk-about',
                'type'     => 'switch', 
                'title'    => 'Poradňa',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),
            
            array(
                'required' => array( 'opt-cpt-talk-about', '=', true ),
                'id'       => 'opt-cpt-talk-about-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'default'  => 'online_poradna',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-talk-about', '=', true ),
                'id'       => 'opt-cpt-talk-about-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'default'  => 'Poradňa',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-talk-about', '=', true ),
                'id'       => 'opt-cpt-talk-about-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'default'  => 'Poradňa',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-talk-about', '=', true ),
                'id'               => 'opt-cpt-talk-about-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-talk-about', '=', true ),
                'id'       => 'opt-cpt-talk-about-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )
    )
);


?>