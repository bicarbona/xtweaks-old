<?php 

/**
    Subsection CPT
    Bloky
 */

define(RX_PREF, 'opt');

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Bloky', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => RX_PREF.'-cpt-blocks',
                'type'     => 'switch', 
                'title'    => 'Bloky',
                // 'subtitle' => 'subtitle',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'required'  => array( RX_PREF.'-cpt-blocks', '=', true ),
                'id'        => RX_PREF.'-cpt-blocks-slug',
                'type'      => 'text',
                'title'     => __('Slug', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'content_block'
            ),

            array(
                'required'  => array( RX_PREF.'-cpt-blocks', '=', true ),
                'id'        => RX_PREF.'-cpt-blocks-name-singular',
                'type'      => 'text',
                'title'     => __('Name Singular', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'Blok',
            ),

            array(
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'id'       => RX_PREF.'-cpt-blocks-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => 'Bloky',
            ),

            array(
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'id'        => RX_PREF.'-cpt-blocks-has-archive',
                'type'      => 'switch',
                'title'     => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'   => false,
            ),

            array(
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'id'       => RX_PREF.'-cpt-blocks-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),

             array(
                'id'   => RX_PREF.'-cpt-blocks-info-rest-api',
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'type' => 'info',
                'title'    => __('Rest API', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
              'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'id'       => RX_PREF.'-cpt-blocks-rest-api',
                'type'     => 'switch', 
                'title'    => __('Show in REST API', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'            => RX_PREF.'-cpt-blocks-rest-api-slug',
                'required'      => array( RX_PREF.'-cpt-blocks', '=', true ),
                'type'          => 'text',
                'title'         => __('REST API base slug', 'redux-framework-demo'),
                //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                //'desc'        => __('Desc', 'redux-framework-demo'),
                'default'       => '',
                'ajax_save'     => true
            ),

            array(
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'id'       => RX_PREF.'-cpt-blocks-in-nav-menu',
                'type'     => 'switch', 
                'title'    => __('Show in Nav Menus', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => RX_PREF.'-cpt-blocks-exclude-search',
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'type'     => 'switch', 
                'title'    => __('Search', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => RX_PREF.'-cpt-blocks-show-in-menu',
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'type'     => 'switch', 
                'title'    => __('Show in Menu', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => RX_PREF.'-cpt-blocks-show-in-submenu',
                'required' => array( RX_PREF.'-cpt-blocks', '=', true ),
                'type'     => 'text', 
                'title'    => __('Show in submenu', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'  => '',
                'ajax_save' => true,
            ),

        )
    )
);

?>