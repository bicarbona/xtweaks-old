<?php

/**
 Slider
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Slider', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'opt-cpt-slider',
                'type'     => 'switch', 
                'title'    => 'Slider',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),
            array(
                'required' => array( 'opt-cpt-slider', '=', true ),
                'id'       => 'opt-cpt-slider-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'  => 'xtw_slider',
            ),

            array(
                'required' => array( 'opt-cpt-slider', '=', true ),
                'id'       => 'opt-cpt-slider-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'default'  => 'Slider',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-slider', '=', true ),
                'id'       => 'opt-cpt-slider-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'default'  => 'Slider',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-slider', '=', true ),
                'id'               => 'opt-cpt-slider-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-slider', '=', true ),
                'id'       => 'opt-cpt-slider-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )
    )
);

?>