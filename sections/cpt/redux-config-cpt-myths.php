<?php

/**
    Subsection CPT
    Myths
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Mýty a pravdy', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
        
            array(
                'id'       => 'opt-cpt-myths',
                'type'     => 'switch', 
                'title'    => 'Mýty a pravdy',
                // 'subtitle' => 'subtitle',
                'default'  => false,
                'ajax_save' => true,
            ),

            array(
                'required' => array( 'opt-cpt-myths', '=', true ),
                'id'       => 'opt-cpt-myths-slug',
                'type'     => 'text',
                'title'    => __('Slug', 'redux-framework-demo'),
                'default'  => 'myth_truth',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-myths', '=', true ),              
                'id'       => 'opt-cpt-myths-name-singular',
                'type'     => 'text',
                'title'    => __('Name Singular', 'redux-framework-demo'),
                'default'  => 'Mýty a pravdy',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-myths', '=', true ),
                'id'       => 'opt-cpt-myths-name-plural',
                'type'     => 'text',
                'title'    => __('Name Plural', 'redux-framework-demo'),
                'default'  => 'Mýty a pravdy',
                'ajax_save' => true
            ),

            array(
                'required' => array( 'opt-cpt-myths', '=', true ),
                'id'               => 'opt-cpt-myths-has-archive',
                'type'             => 'switch',
                'title'            => __('Has Archive', 'redux-framework-demo'), 
               // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                'default'          => false,
            ),

            array(
                'required' => array( 'opt-cpt-myths', '=', true ),
                'id'       => 'opt-cpt-myths-menu-position',
                'type'     => 'spinner', 
                'title'    => 'Menu Position',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '1',
                'step'     => '1',
                'max'      => '30',
            ),
        )
    )
);

?>