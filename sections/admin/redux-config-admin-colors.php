<?php 

/**
    Subsection Admin
    Color
**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'title'      => __( 'Color', 'redux-framework-demo' ),
        'desc'       => '',
        'subsection' => true,
        'icon'       => 'el-icon-adjust',
        'fields'     => array(

            array(
                'id'       => 'opt-admin-base-color',
                'type'     => 'color_rgba',
                'title'    => __( 'Base', 'redux-framework-demo' ),
                'subtitle' => __( 'Menu Background', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#122a4e', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-admin-highlight-color',
                'type'     => 'color_rgba',
                'title'    => __( 'Highlight', 'redux-framework-demo' ),
                'subtitle' => __( 'Current menu / Hover', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#4074c4', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-admin-notification-color',
                'type'     => 'color_rgba',
                'title'    => __( 'Notification', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Notification Color', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#ff7e00', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-admin-body-background',
                'type'     => 'color_rgba',
                'title'    => __( 'Body', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Body Background', 'redux-framework-demo' ),
                'clickout_fires_change'     => true,
                'default'  => array( 'color' => '#f1f1f1', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-admin-link-color',
                'type'     => 'color_rgba',
                'title'    => __( 'Link', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Link Color', 'redux-framework-demo' ),
                'clickout_fires_change'     => true,
                'default'  => array( 'color' => '#0a54c6', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),
        )
    )// END ARRAYS
); // END SUBSECTION
?>