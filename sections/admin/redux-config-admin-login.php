<?php 

/**
    Subsection Admin
    Login
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'title'      => __( 'Login', 'redux-framework-demo' ),
        'desc'       => '',
        'section_id' => 'section_admin',
        'subsection' => true,
        'icon'       => 'el-icon-unlock',
        'fields'     => array(
    /**
    Admin logo
    **/

            array(
                'id'       => 'opt-admin-login-color-link-text',
                'type'     => 'link_color',
                'title'    => __('Links Color Option', 'redux-framework-demo'),
                'subtitle' => __('Only color validation can be done on this field type', 'redux-framework-demo'),
                'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                'default'  => array(
                    'regular'  => '#1e73be', // blue
                    'hover'    => '#dd3333', // red
                    'active'   => '#8224e3',  // purple
                    'visited'  => '#8224e3'  // purple
                )
            ),

            array(
                'id'       => 'opt-admin-login-logo',
                'type'     => 'media',
                'url'      => true, // 
                'title'    => __( 'Custom admin login logo', 'redux-framework-demo' ),
                'subtitle' => __( 'Upload a 254 x 95px image here to replace the admin login logo.', 'redux-framework-demo' ),
                'desc'     => '',
                'default'  => array(
                    'url'=>'https://www.dropbox.com/s/9scbdzippxifbon/proton.png?raw=1'
                ),
            ),

            array(         
                'id'       => 'opt-admin-login-body-background',
                'type'     => 'background',
                'title'    => __('Body Background', 'redux-framework-demo'),
                'subtitle' => __('Body background with image, color, etc.', 'redux-framework-demo'),
                'desc'     => __('', 'redux-framework-demo'),
                'default'  => array(
                    'background-color' => '#fff',
                )
            ),

            


        )
    )
);
?>