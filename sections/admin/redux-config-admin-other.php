<?php 

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'icon'       => 'el-icon-website',
    'title'      => __( 'Other', 'redux-framework-demo' ),
    'submenu' => false,
    'subsection' => true,
    'fields'     => array(

     array(
        'id'   => 'info-admin-posts-messages',
        // 'required' => array( 'opt-id', '=', true ),
        'type' => 'info',
        'title'    => __('Post Action Messages', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        'desc'     => __('Publish / Save draft', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info|success
    ),

        array(
            'id'       => 'opt-disable-messages-publish',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch', 
            'title'    => __('Publish', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-disable-messages-updated-draft',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch', 
            'title'    => __('Updated Draft', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-disable-messages-updated-published',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch', 
            'title'    => __('Updated Published', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-disable-messages-updated-shedule',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch', 
            'title'    => __('Shedule', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-disable-messages-restore-revision',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch', 
            'title'    => __('Restore Revision', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        
        array(
            'id'   => 'info-admin-post-autosave',
            // 'required' => array( 'opt-id', '=', true ),
            'type' => 'info',
            'title'    => __('Autosave', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info|success
        ),

        array(
            'id'       => 'opt-disable-autosave',
            'type'     => 'switch', 
            'title'    => 'Posts Auto Saving',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),
        
        array(
            'id'       => 'opt-autosave-interval',
            'type'     => 'text',
            'title'    => __('Autosave Interval', 'redux-framework-demo'),
            // 'subtitle' => __('subtitle', 'redux-framework-demo'),
            // 'desc'     => __('desc', 'redux-framework-demo'),
            'default'  => '120',
            'ajax_save' => true
        ),

        array(
            'id'       => 'opt-disable-html-in-commets',
            'type'     => 'switch', 
            'title'    => 'HTML in Comments',
            // 'subtitle' => 'subtitle',    
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'   => 'info-fix-acf-label',
            'type' => 'info',
            'title'    => __('Fix ACF', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info|success
        ),

         array(
             'id'       => 'opt-fix-acf-label',
             // 'required' => array( 'opt-id', '=', true ),
             'type'     => 'switch', 
             'title'    => __('Hide ACF label', 'redux-framework-demo'),
             //'subtitle' => __('Subtitle', 'redux-framework-demo'),
             'desc'     => __('Hide ACF label ', 'redux-framework-demo'),
             'default'  => true,
             'ajax_save' => true,
         ),


        array(
            'id'       => 'opt-rewrite-category-base',
            'type'     => 'switch', 
            'title'    => 'Rewrite Category Base',
            'desc' => 'Strip the category base <strong>(usually /category/)</strong> from the category URL.',
            'default'  => true,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-post-type-upload-dir',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch', 
            'title'    => __('Post Type Upload Dir', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        )// END FIELDS
    )
);
?>