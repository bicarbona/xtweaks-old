<?php 
   
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'title'      => __( 'Widgets', 'redux-framework-demo' ),
    'desc'       => '',
    'subsection' => true,
    'icon'       => 'el-icon-dashboard',

    'fields'     => array(


    array(
        'id'       => 'opt-admin-dashboard-custom-page',
        // 'required' => array( 'opt-id', '=', true ),
        'type'     => 'switch', 
        'title'    => __('Custom Dashboard', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'default'  => true,
        'ajax_save' => true,
    ),
                          
    /**
    Dashboard Hide Dashboard Widgets
    **/

    array(
        'id'      => 'opt-admin-dashboard-widget2',
        'type'    => 'sorter',
        'title'   => 'Dashboard Widgets Manager',
        'desc'    => 'Organize how you want the layout to appear on the homepage',
        'options' => array(
            'disabled'  => array(
                'dashboard_right_now' => __( 'Right Now' ),
                'network_dashboard_right_now' => __( 'Right Now' ),
                'dashboard_activity' => __( 'Activity' ),
                'dashboard_quick_press' => __( 'Quick Draft' ),
                'dashboard_primary' => __( 'WordPress News' ),   
                'woocommerce_dashboard_recent_reviews' => __( 'Woo Recent Reviews', 'woocommerce' ),
                'woocommerce_dashboard_status' => __( 'Woo Status', 'woocommerce' ),
                'pb_backupbuddy_stats' => __('BackupBuddy'),
                'redux_dashboard_widget' => 'Redux Dashboard Widget',
                ## Other
                'wpseo-dashboard-overview' => __( 'Yoast SEO' ),
            ),
            'enabled' => array(
            )
        ),
    ),

/**
Dashboard Hide Dashboard Widgets
**/

        array(
            'id'      => 'opt-admin-default-widget',
            'type'    => 'sorter',
            'title'   => 'Dashboard Widgets Manager',
            'desc'    => 'Organize how you want the layout to appear on the homepage',
            'options' => array(
                'disabled'  => array(
                    'WP_Widget_Pages' => __( 'Pages' ),
                    'WP_Widget_Calendar' => __( 'Calendar' ),
                    'WP_Widget_Archives' => __( 'Archives' ),
                    'WP_Widget_Links' => __( 'Links' ),
                    'WP_Widget_Meta' => __( 'Meta' ),
                    'WP_Widget_Search' => __( 'Search' ),
                    'WP_Widget_Text' => __( 'Text' ),
                    'WP_Widget_Categories' => __( 'Categories' ),
                    'WP_Widget_Recent_Posts' => __( 'Recent Posts' ),
                    'WP_Widget_Recent_Comments' => __( 'Recent Comments' ),
                    'WP_Widget_RSS' => __( 'RSS' ),
                    'WP_Widget_Tag_Cloud' => __( 'Tag Cloud' ),
                    'WP_Nav_Menu_Widget' => __( 'Nav Menu' ),

                    ## WooCommerce

                    'WC_Widget_Recent_Products' => __( 'Woo Recent' ),
                    'WC_Widget_Featured_Products' => __( 'Woo Featured' ),
                    'WC_Widget_Product_Categories' => __( 'Woo Categories' ),
                    'WC_Widget_Product_Tag_Cloud' => __( 'Woo Tag Cloud' ), 
                    'WC_Widget_Cart' => __( 'Woo Cart' ),
                    'WC_Widget_Layered_Nav' => __( 'Woo Layered Nav' ), 
                    'WC_Widget_Layered_Nav_Filters' => __( 'Woo Layered Nav Filters' ),
                    'WC_Widget_Price_Filter' => __( 'Woo Price Filter' ),
                    'WC_Widget_Product_Search' => __( 'Woo Search' ), 
                    'WC_Widget_Top_Rated_Products' => __( 'Woo Top Rated' ),
                    'WC_Widget_Recent_Reviews' => __( 'Woo Recent Reviews' ), 
                    'WC_Widget_Recently_Viewed' => __( 'Woo Recently Viewed' ),
                    'WC_Widget_Best_Sellers' => __( 'Woo Best Sellers' ),
                    'WC_Widget_Onsale' => __( 'Woo Onsale' ), 
                    'WC_Widget_Random_Products' => __( 'Woo Random Products' ),
                    'WC_Widget_Products' => __( 'Woo Products' ), 
                ),
                'enabled' => array(
                )
            ),
        ),

        array(
            'id'       => 'opt-shortcode-in-widget',
            'type'     => 'switch', 
            'title'    => 'Enable Shortcode in Widget',
            // 'subtitle' => 'subtitle',
            'default'  => true,
            'ajax_save' => true,
        ),

    ))// END ARRAYS

    ); // END SUBSECTION

?>