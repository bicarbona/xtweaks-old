<?php 

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'icon'       => 'el-icon-website',
    'title'      => __( 'Metaboxes', 'redux-framework-demo' ),
    'submenu' => false,
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'opt-post-page-metaboxes',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'button_set',
            'title'    => __('Post Page Metaboxes', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            // 'multi'    => false,
            'ajax_save' => true,
            //Must provide key => value pairs for options
            'options' => array(
                'post' => 'Post', 
                'page' => 'Page'
                ), 
            'default' => array('page'),
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-postexcerpt',
            'type'     => 'switch', 
            'title'    => 'Post Excerpt ',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-commentstatusdiv',
            'type'     => 'switch', 
            'title'    => 'Comment Status',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-commentsdiv',
            'type'     => 'switch', 
            'title'    => 'Comments',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),
        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-postcustom',
            'type'     => 'switch', 
            'title'    => 'Custom Fields',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-authordiv',
            'type'     => 'switch', 
            'title'    => 'Author',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-trackbacksdiv',
            'type'     => 'switch', 
            'title'    => 'Trackbacks',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-pageparentdiv',
            'type'     => 'switch', 
            'title'    => 'Page Parent',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-formatdiv',
            'type'     => 'switch', 
            'title'    => 'Format Post',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-postimagediv',
            'type'     => 'switch', 
            'title'    => 'Post Image',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-revisionsdiv',
            'type'     => 'switch', 
            'title'    => 'Revisions',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-slugdiv',
            'type'     => 'switch', 
            'title'    => 'Slug',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-tagsdiv-post_tag',
            'type'     => 'switch', 
            'title'    => 'Tags',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),
        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'id'       => 'opt-post-categorydiv',
            'type'     => 'switch', 
            'title'    => 'Category',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        // array(
        //     'id'       => 'opt-id',
        //     // 'required' => array( 'opt-id', '=', true ),
        //     'type'     => 'select',
        //     'multi'    => true,
        //     'title'    => __('Title', 'redux-framework-demo'),
        //     //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //     //'desc'     => __('Desc', 'redux-framework-demo'),
        //     //Must provide key => value pairs for radio options
        //     'ajax_save' => true,
        //     'options'  => array(
        //         '1' => 'slugdiv 1',
        //         '2' => 'Revisions 2',
        //         '3' => 'Opt 3'),
        //     'default'  => array('2','3')
        // ),

/**
Headway Metaboxes
**/

         array(
               
            'id'   => 'info-headway-metaboxes',
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'type' => 'info',
            'title'    => __('Headway Metaboxes', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => false,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info|success
        ),

        array(
            'id'       => 'opt-post-headway-admin-meta-box-template',
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'type'     => 'switch', 
            'title'    => __('Template', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-post-headway-admin-meta-box-alternate-title',
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'type'     => 'switch', 
            'title'    => __('Alternate Title', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-post-headway-admin-meta-box-post-thumbnail',
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'type'     => 'switch', 
            'title'    => __('Post Thumbnail', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-post-headway-admin-meta-box-display',
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'type'     => 'switch', 
            'title'    => __('Custom CSS Class', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-post-headway-admin-meta-box-seo',
            'required' => array( 'opt-post-page-metaboxes', '=', 'post' ),
            'type'     => 'switch', 
            'title'    => __('SEO', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),




        /**
        PAGE
        **/

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-postexcerpt',
            'type'     => 'switch', 
            'title'    => 'Post Excerpt ',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-commentstatusdiv',
            'type'     => 'switch', 
            'title'    => 'Comment Status',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-commentsdiv',
            'type'     => 'switch', 
            'title'    => 'Comments',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),
        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-postcustom',
            'type'     => 'switch', 
            'title'    => 'Custom Fields',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-authordiv',
            'type'     => 'switch', 
            'title'    => 'Author',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-trackbacksdiv',
            'type'     => 'switch', 
            'title'    => 'Trackbacks',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-pageparentdiv',
            'type'     => 'switch', 
            'title'    => 'Page Parent',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-formatdiv',
            'type'     => 'switch', 
            'title'    => 'Format Post',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-postimagediv',
            'type'     => 'switch', 
            'title'    => 'Post Image',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-revisionsdiv',
            'type'     => 'switch', 
            'title'    => 'Revisions',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-slugdiv',
            'type'     => 'switch', 
            'title'    => 'Slug',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-tagsdiv-post_tag',
            'type'     => 'switch', 
            'title'    => 'Tags',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),
        array(
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'id'       => 'opt-page-categorydiv',
            'type'     => 'switch', 
            'title'    => 'Category',
            // 'subtitle' => 'subtitle',
            'default'  => false,
            'ajax_save' => true,
        ),

        // array(
        //     'id'       => 'opt-id',
        //     // 'required' => array( 'opt-id', '=', true ),
        //     'type'     => 'select',
        //     'multi'    => true,
        //     'title'    => __('Title', 'redux-framework-demo'),
        //     //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //     //'desc'     => __('Desc', 'redux-framework-demo'),
        //     //Must provide key => value pairs for radio options
        //     'ajax_save' => true,
        //     'options'  => array(
        //         '1' => 'slugdiv 1',
        //         '2' => 'Revisions 2',
        //         '3' => 'Opt 3'),
        //     'default'  => array('2','3')
        // ),

/**
Headway Metaboxes
**/

         array(
               
            'id'   => 'info-headway-page-metaboxes',
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'type' => 'info',
            'title'    => __('Headway Metaboxes', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => false,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info|success
        ),


        array(
            'id'       => 'opt-page-headway-admin-meta-box-template',
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'type'     => 'switch', 
            'title'    => __('Template', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-page-headway-admin-meta-box-alternate-title',
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'type'     => 'switch', 
            'title'    => __('Alternate Title', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-page-headway-admin-meta-box-post-thumbnail',
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'type'     => 'switch', 
            'title'    => __('Post Thumbnail', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-page-headway-admin-meta-box-display',
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'type'     => 'switch', 
            'title'    => __('Custom CSS Class', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),

        array(
            'id'       => 'opt-page-headway-admin-meta-box-seo',
            'required' => array( 'opt-post-page-metaboxes', '=', 'page' ),
            'type'     => 'switch', 
            'title'    => __('SEO', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => false,
            'ajax_save' => true,
        ),
        )// END FIELDS
    )
);
?>