<?php 

/**
    Subsection Admin
    Dev
 */
 
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
   array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Dev', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
        
    /**
    Scroll Back to top
    **/
            array(
                'id'   => 'opt-admin-info',
                'title' =>  __( 'Dev', 'redux-framework-demo' ),
                'type' => 'info',
                'desc' => __( 'Footer', 'redux-framework-demo' ),
                'submenu' => false,
                'subsection' => true,
            ),
    /**
    Visibility Status
    **/
            array(
                'id'       => 'opt-admin-footer-visibility',
                'type'     => 'select',
                'title'    => __( 'Footer Visibility', 'redux-framework-demo' ),
                // 'subtitle' => __( '', 'redux-framework-demo' ),
                // 'desc'     => __( '', 'redux-framework-demo' ),
                //Must provide key => value pairs for radio options
                'options'  => array(
                    'publicly_visible' => 'Publicly Visible',
                    'preview_mode' => 'Preview mode',
                    'disabled' => 'Disabled'
                ),
                'default'  => 'disabled'
            ),

            array(
                'id'   => 'opt-admin-footer-text',
                'title'    => __( 'Footer text', 'redux-framework-demo' ),
                'type' => 'text',
                // 'desc' => __( '', 'redux-framework-demo' )
            ),

            array(
                'id'   => 'opt-admin-footer-version',
                'title'    => __( 'Footer version', 'redux-framework-demo' ),
                'type' => 'text',
                // 'desc' => __( '', 'redux-framework-demo' )
            ),

            array(
                'id'   => 'opt-admin-footer-version',
                'title'    => __( 'Hide Adminbar for non-admin', 'redux-framework-demo' ),
                'type' => 'radio',
                // 'desc' => __( '', 'redux-framework-demo' ),
                'options'  => array(
                    '1' => 'Opt 1', 
                    '2' => 'Opt 2', 
                    '3' => 'Opt 3'
                ),
            ),

            array(
                'id'       => 'opt-admin-hide-admin-logo',
                'type'     => 'switch', 
                'title'    => __('Hide Default Admin logo', 'redux-framework-demo'),
                // 'subtitle' => __('', 'redux-framework-demo'),
                'default'  => true,
            ),

            array(
                'id'   => 'opt-admin-footer-version',
                'title'    => __( 'Replace Howdy', 'redux-framework-demo' ),
                'type' => 'text',
                'default' => 'Welcome,',
                'desc' => __( 'Enter text to replace Howdy.', 'redux-framework-demo' )
            ),

            array(
                'id'       => 'opt-admin-hide-admin-logo',
                'type'     => 'switch', 
                'title'    => __('Hide Default Admin logo', 'redux-framework-demo'),
                // 'subtitle' => __('', 'redux-framework-demo'),
                'default'  => true,
            ),
        )   
    )
);

Redux::setField(
$opt_name, // This is your opt_name,
    array( // This is your arguments array
       'id'         => 'section_example_2',
       // Unique identifier for your panel. Must be set and must not contain spaces or special characters
       'type'       => 'color',
       'section_id' => 'section_admin',
       'title'      => 'Example Field',
       'subtitle'   => 'Field Subtitle',
       'desc'       => 'This is for descriptive text.',
    )
);

?>