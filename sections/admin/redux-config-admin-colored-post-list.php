<?php 

function get_post_statuses__($include = array(), $exclude = array()) {
    $post;
    $post_stati = get_post_stati($post_stati = array(), "objects");

    $custom_post_statuses = array();

    foreach ($post_stati as $post_status):

        if ($post_status->show_in_admin_status_list === false || (sizeof($include) > 0 && !in_array($post_status->name, $include)) || in_array($post_status->name, $exclude)):
            continue;
        endif;

        $handle = "capl-color-" . sanitize_key($post_status->name);
        $custom_post_statuses[$post_status->name] = array("option_handle" => $handle, "label" => $post_status->label, "name" => $post_status->name);

    endforeach;
    ksort($custom_post_statuses);
    return $custom_post_statuses;
    
}


/**

Admin  Color Post List

**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks

 array(
        'icon'       => 'el-icon-broom',
        'title'      => __( 'Colored Post List', 'redux-framework-demo' ),
        'subsection' => true,
        'id'        => 'opt-admin-color-post-list',
        'fields'     => array(


        ),
    ) 
);


// $custom_post_statuses = get_post_statuses__(array(), array("publish", "pending", "future", "private", "draft", "trash") );

$custom_post_statuses = get_post_statuses__( );

// $custom_post_statuses = get_post_statuses__( array("publish", "pending", "future", "private", "draft", "trash") );

        foreach ($custom_post_statuses as $custom_post_status):
        //$out .= $custom_post_status["option_handle"] .'<br>';
        //  echo  $custom_post_status["option_handle"] .'<br>';
       // echo  $custom_post_status["name"] .'<br>';
        // echo  $custom_post_status["label"] .'<br>';

        Redux::setField(
        $opt_name, // This is your opt_name,
            array( // This is your arguments array
               'id'         => $custom_post_status["name"],
               // Unique identifier for your panel. Must be set and must not contain spaces or special characters
               'type'       => 'color',
               'section_id' => 'opt-admin-color-post-list',
               'title'      => $custom_post_status["name"],
               // 'subtitle'   => 'Field Subtitle',
               // 'desc'       => 'This is for descriptive text.',
            )
        );
        endforeach;

        // Redux::setField(
        // $opt_name, // This is your opt_name,
        //     array( // This is your arguments array
        //        'id'         => 'breke',
        //        // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        //        'type'       => 'color',
        //        'section_id' => 'opt-admin-color-post-list',
        //        'title'      => 'Example Field',
        //        // 'subtitle'   => 'Field Subtitle',
        //        // 'desc'       => 'This is for descriptive text.',
        //     )
        // );

?>