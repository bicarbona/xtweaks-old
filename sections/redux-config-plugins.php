<?php

/** Avesome icons */
$icons = array(
// "fa-arrow-circle-o-up",
// "fa-arrow-circle-up",
// "fa-arrow-up",
// "fa-chevron-circle-up",
// "fa-chevron-up",
"icon-up-circle",
"icon-up-circle-two",
"icon-up-chevron-small",
"icon-up-bold",
"icon-up-chevron-thin",
"icon-up-chevron",
"icon-up-chevron-circle",
"icon-up-triangle",
"icon-up-align-top",
"icon-up-align-top-two",
"icon-up-chevron-mid",
"icon-up-chevron-capsh",
"icon-up-triangle-strong",
"icon-up-more-dots",
"icon-up-chevron-bold-round",
"icon-up-bold-circle",
"icon-up-triangle-round",
"icon-up-chevron-mid-round",
"icon-arrow-long",
"icon-arrow-round",

);

sort( $icons );
$iconArray = array();

foreach ( $icons as $icon ) {
    $name = ucwords( str_replace( '-', ' ', str_replace( array(
        'fa-',
        '-play',
        '-square',
        '-alt',
        '-circle',
        'icon'
    ), '', $icon ) ) );
    $iconArray[ 'fa ' . $icon ] = $name;
}
/** End awesome icons */

/**
Plugins
**/
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'icon'       => 'el-icon-puzzle',
    'title'      => __( 'Plugins', 'redux-framework-demo' ),
    // 'subsection' => true,
    'fields' => array(),
    ) // END ARRAYS

); // END SECTION

include_once 'plugins/rdx-conf-plug-scrollback-top.php';

include_once 'plugins/rdx-conf-plug-scrollup-header.php';

include_once 'plugins/rdx-conf-plug-shift-nav.php';

include_once 'plugins/rdx-conf-plug-notitle-tooltips.php';

include_once 'plugins/rdx-conf-plug-fixed-sidebar.php';

include_once 'plugins/rdx-conf-plug-woo.php';
    
include_once 'plugins/rdx-conf-plug-woo-cart.php';

include_once 'plugins/rdx-conf-plug-cf7.php';
include_once 'plugins/rdx-conf-plug-login-form.php';
// OLD DEPRECATED
// include_once 'plugins/rdx-conf-plug-sticky-menu.php';
?>