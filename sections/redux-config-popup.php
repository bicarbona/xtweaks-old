<?php

// Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
//     array(
//         'icon'       => 'el-icon-adjust',
//         'title'      => __( 'Popup', 'redux-framework-demo' ),
//         // 'subsection' => true,
//         'fields'     => array(

//             array(
//                 'id'       => 'opt-popup-pages',
//                 // 'required' => array( 'opt-id', '=', true ),
//                 'type'     => 'select',
//                 'multi'    => true,
//                 'title'    => __('Pages', 'redux-framework-demo'),
//                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
//                 //'desc'     => __('Desc', 'redux-framework-demo'),
//                 //Must provide key => value pairs for radio options
//                 'ajax_save' => true,
//                 'data' => 'pages',
//             ),


//             array(
//                 'id'       => 'opt-popup-posts',
//                 // 'required' => array( 'opt-id', '=', true ),
//                 'type'     => 'select',
//                 'multi'    => true,
//                 'title'    => __('Posts', 'redux-framework-demo'),
//                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
//                 //'desc'     => __('Desc', 'redux-framework-demo'),
//                 //Must provide key => value pairs for radio options
//                 'ajax_save' => true,
//                 'data' => 'posts',
//             ),
//             array(
//                 'id'       => 'opt-popup-categories',
//                 // 'required' => array( 'opt-id', '=', true ),
//                 'type'     => 'select',
//                 'multi'    => true,
//                 'title'    => __('Categories', 'redux-framework-demo'),
//                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
//                 //'desc'     => __('Desc', 'redux-framework-demo'),
//                 //Must provide key => value pairs for radio options
//                 'ajax_save' => true,
//                 'data' => 'categories',
//             ),

//             array(
//                 'id'       => 'opt-popup-roles',
//                 // 'required' => array( 'opt-id', '=', true ),
//                 'type'     => 'select',
//                 'multi'    => true,
//                 'title'    => __('Roles', 'redux-framework-demo'),
//                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
//                 //'desc'     => __('Desc', 'redux-framework-demo'),
//                 //Must provide key => value pairs for radio options
//                 'ajax_save' => true,
//                 'data' => 'roles',
//             ),

//             array(
//                 'id'       => 'opt-popup-post-type',
//                 // 'required' => array( 'opt-id', '=', true ),
//                 'type'     => 'select',
//                 'multi'    => true,
//                 'title'    => __('Post type', 'redux-framework-demo'),
//                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
//                 //'desc'     => __('Desc', 'redux-framework-demo'),
//                 //Must provide key => value pairs for radio options
//                 'ajax_save' => true,
//                 'data' => 'post_types',
//             ),

//             array(
//                 'id'       => 'opt-popup-post-tags',
//                 // 'required' => array( 'opt-id', '=', true ),
//                 'type'     => 'select',
//                 'multi'    => true,
//                 'title'    => __('tags', 'redux-framework-demo'),
//                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
//                 //'desc'     => __('Desc', 'redux-framework-demo'),
//                 //Must provide key => value pairs for radio options
//                 'ajax_save' => true,
//                 'data' => 'tags',
//             ),


//         )
//     )
// );


// BE SURE TO RENAME THE FUNCTION NAMES TO YOUR OWN NAME OR PREFIX
if ( !function_exists( "redux_add_metaboxes" ) ):
    function redux_add_metaboxes($metaboxes) {
    // Declare your sections

    $boxSections[ ] = array(
            'title'  => __('Settings', 'redux-framework-demo'),
            //'desc' => __('', 'redux-framework-demo'),
            'icon'   => 'el-icon-home',
            'fields' => array(
              array(
                'id'       => 'opt-popup-pages',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Pages', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'data' => 'pages',
            ),

            array(
                'id'       => 'opt-popup-posts',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Posts', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'data' => 'posts',
            ),

            array(
                'id'       => 'opt-popup-categories',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Categories', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'data' => 'categories',
            ),

            array(
                'id'       => 'opt-popup-roles',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Roles', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'data' => 'roles',
            ),

            array(
                'id'       => 'opt-popup-post-type',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Post type', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'data' => 'post_types',
            ),

            array(
                'id'       => 'opt-popup-post-tags',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Tags', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'data' => 'tags',
            ),





    array(
    'title' => __('Repeater', 'redux-framework-demo' ),
    'icon' => 'el-icon-thumbs-up',
    'fields' => array(
        array(
            'id'         => 'repeater-field-id',
            'type'       => 'repeater',
            'title'      => __( 'Title', 'redux-framework-demo' ),
            'subtitle'   => __( '', 'redux-framework-demo' ),
            'desc'       => __( '', 'redux-framework-demo' ),
            //'group_values' => true, // Group all fields below within the repeater ID
            //'item_name' => '', // Add a repeater block name to the Add and Delete buttons
            //'bind_title' => '', // Bind the repeater block title to this field ID
            //'static'     => 2, // Set the number of repeater blocks to be output
            //'limit' => 2, // Limit the number of repeater blocks a user can create
            //'sortable' => false, // Allow the users to sort the repeater blocks or not
            'fields'     => array(
                array(
                    'id'          => 'title_field',
                    'type'        => 'text',
                    'placeholder' => __( 'Title', 'redux-framework-demo' ),
                ),
                array(
                    'id'          => 'text_field',
                    'type'        => 'text',
                    'placeholder' => __( 'Text Field', 'redux-framework-demo' ),
                ),

            )
        )
    )
),
        )
    );

/**
 Section 2
**/
    $boxSections[ ] = array(
            'title'  => __('Layout', 'redux-framework-demo'),
            'desc'   => __('', 'redux-framework-demo'),
            'icon'   => 'el-icon-home',
            'fields' => array(

                    array(
                        'id'        => 'opt-modal-background',
                        // 'required' => array( 'opt-id', '=', true ),
                        'type'      => 'color_rgba',
                        'title'    => __('Background', 'redux-framework-demo'),
                        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                        //'desc'     => __('Desc', 'redux-framework-demo'),
                         'ajax_save' => true,
                        // See Notes below about these lines.
                        //'output'    => array('background-color' => '.site-header'),
                        //'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
                        'default'   => array(
                            'color'     => 'color',
                            'alpha'     => 1
                        ),

                        // These options display a fully functional color palette. Omit this argument
                        // for the minimal color picker, and change as desired.
                        'options'       => array(
                            'show_input'                => true,
                            'show_initial'              => true,
                            'show_alpha'                => true,
                            'show_palette'              => true,
                            'show_palette_only'         => false,
                            'show_selection_palette'    => true,
                            'max_palette_size'          => 10,
                            'allow_empty'               => true,
                            'clickout_fires_change'     => false,
                            'choose_text'               => 'Choose',
                            'cancel_text'               => 'Cancel',
                            'show_buttons'              => true,
                            'use_extended_classes'      => true,
                            'palette'                   => null,  // show default
                            'input_text'                => 'Select Color'
                        ),
                    ),


                    array(
                        'id'        => 'opt-modal-overlay-background',
                        // 'required' => array( 'opt-id', '=', true ),
                        'type'      => 'color_rgba',
                        'title'    => __('Overlay', 'redux-framework-demo'),
                        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                        //'desc'     => __('Desc', 'redux-framework-demo'),
                         'ajax_save' => true,
                        //'output'    => array('background-color' => '.site-header'),
                        'default'   => array(
                            'color'     => 'color',
                            'alpha'     => 1
                        ),

                        'options'       => array(
                            'show_input'                => true,
                            'show_initial'              => true,
                            'show_alpha'                => true,
                            'show_palette'              => true,
                            'show_palette_only'         => false,
                            'show_selection_palette'    => true,
                            'max_palette_size'          => 10,
                            'allow_empty'               => true,
                            'clickout_fires_change'     => false,
                            'choose_text'               => 'Choose',
                            'cancel_text'               => 'Cancel',
                            'show_buttons'              => true,
                            'use_extended_classes'      => true,
                            'palette'                   => null,  // show default
                            'input_text'                => 'Select Color'
                        ),
                    ),


                    array(
                        'id'            => 'opt-modal-border',
                        // 'required'      => array( 'opt-id', '=', true ),
                        'type'          => 'border',
                        'title'         => __('Border', 'redux-framework-demo'),
                        //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                        //'desc'        => __('Desc', 'redux-framework-demo'),
                        //'output'      => array('.site-header'),
                        'ajax_save'     => true,
                        'default'       => array(
                            'border-color'  => '#1e73be',
                            'border-style'  => 'solid',
                            'border-top'    => '1px',
                            'border-right'  => '1px',
                            'border-bottom' => '1px',
                            'border-left'   => '1px'
                        )
                    ),


                   array(
                        'id'            => 'opt-modal-margin',
                        // 'required'      => array( 'opt-id', '=', true ),
                        'type'          => 'spacing',
                        'title'         => __('Margin', 'redux-framework-demo'),
                        //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                        //'desc'        => __('Desc', 'redux-framework-demo'),
                        'mode'          => 'margin', // padding
                        'ajax_save'        =>     true,
                        'units'         => array('em', 'px'),
                        'units_extended' => 'false',
                        'default'       => array(
                            '*-top'     => '1px',
                            '*-right'   => '2px',
                            '*-bottom'  => '3px',
                            '*-left'    => '4px',
                            'units'     => 'px',
                        )
                    ),

                    array(
                        'id'            => 'opt-modal-padding',
                        // 'required'      => array( 'opt-id', '=', true ),
                        'type'          => 'spacing',
                        'title'         => __('Padding', 'redux-framework-demo'),
                        //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                        //'desc'        => __('Desc', 'redux-framework-demo'),
                        'mode'          => 'margin', // padding
                        'ajax_save'        =>     true,
                        'units'         => array('em', 'px'),
                        'units_extended' => 'false',
                        'default'       => array(
                            '*-top'     => '1px',
                            '*-right'   => '2px',
                            '*-bottom'  => '3px',
                            '*-left'    => '4px',
                            'units'     => 'px',
                        )
                    ),


                    array(
                        'id'       => 'opt-modal-auto-close',
                        // 'required' => array( 'opt-id', '=', true ),
                        'type'     => 'select',
                        'multi'    => false,
                        'title'    => __('Title', 'redux-framework-demo'),
                        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                        //'desc'     => __('Desc', 'redux-framework-demo'),
                        //Must provide key => value pairs for radio options
                        'ajax_save' => true,
                        'options'  => array(
                            '1' => 'No',
                            '5' => 'After 5 second',
                            '10' => 'After 10 second',
                            '15' => 'After 15 second',
                            '20' => 'After 20 second',
                            '25' => 'After 25 second',
                            '30' => 'After 30 second',
                            '35' => 'After 35 second',
                            '40' => 'After 40 second',
                            '45' => 'After 45 second',
                            '50' => 'After 50 second',
                            ),


                        'default'  => array('2','3')
                    ),


                    array(
                        'id'       => 'opt-modal-set-cookie',
                        // 'required' => array( 'opt-id', '=', true ),
                        'type'     => 'switch',
                        'title'    => __('Set Cookie', 'redux-framework-demo'),
                        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                        //'desc'     => __('Desc', 'redux-framework-demo'),
                        'default'  => true,
                        'ajax_save' => true,
                    ),

                    array(
                        'id'       => 'opt-modal-show-interval',
                        'required' => array( 'opt-modal-set-cookie', '=', true ),
                        'type'     => 'select',
                        'multi'    => false,
                        'title'    => __('Interval', 'redux-framework-demo'),
                        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                        //'desc'     => __('Desc', 'redux-framework-demo'),
                        //Must provide key => value pairs for radio options
                        'ajax_save' => true,
                        'options'  => array(
                            '1' => 'show once',
                            '2' => 'every 6 hours',
                            '3' => 'Opt 3'),
                        'default'  => array('2','3')
                    ),

            )
    );

    $metaboxes = array();

    $metaboxes[ ]   = array(
            'id'         => 'demo-layout',
            'title'      => __('Cool Options', 'redux-framework-demo'),
            'post_types' => array('popup'),
            'position'   => 'normal', // normal, advanced, side
            'priority'   => 'high', // high, core, default, low
            //'sidebar' => false, // enable/disable the sidebar in the normal/advanced positions
            'sections'   => $boxSections
    );

    $boxSections    = array();
        return $metaboxes;
    }

    $redux_opt_name = 'redux_tweaks';
    // Change {$redux_opt_name} to your opt_name
    add_action("redux/metaboxes/{$redux_opt_name}/boxes", "redux_add_metaboxes");
endif;


?>
