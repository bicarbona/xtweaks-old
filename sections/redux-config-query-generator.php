<?php
/**
 *  @todo redux sa bude zobrazovat pod CPT - xtw_query
 *  vytvorim  si vlastne query rovnako ako architect
 *  template - layout moze byt kludne aj staticke PHP
 *  @todo moznost hookovat query - napr - taxonomy ...
 *  (cize v buildery pripravim Query ale este ju mozem dodatocne zmenit pridat alebo prepisat)
 *  pridat podmienku (filter) if featured image
*   @link https://wordpress.org/plugins/easy-query/screenshots/
    @link https://connekthq.com/plugins/easy-query/, https://calderawp.com/downloads/caldera-easy-queries/#foobox-1/0/caeq-filter.png

    http://codecanyon.net/item/post-type-builder-wordpress-custom-post-types/11833291
 * pzarc_get_tags =>
* post per page
* post status
 */





$prefix = 'query-generator-';
// BE SURE TO RENAME THE FUNCTION NAMES TO YOUR OWN NAME OR PREFIX
if ( !function_exists( "redux_add_metaboxes_query" ) ):
    function redux_add_metaboxes_query($metaboxes) {
    // Declare your sections

    $boxSections[ ] = array(
            'title'  => __('Settings', 'redux-framework-demo'),
            //'desc' => __('', 'redux-framework-demo'),
            'icon'   => 'el-icon-home',
            'fields' => array(
              array(
                'id'       => $prefix . 'opt-popup-pages',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Pages', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'data' => 'pages',
            ),

            array(
              'title'   => __( 'xxxx', 'pzarchitect' ),
              'id'      => 'Include FIle',
              'type'    => 'select',
              'data'    => 'callback',
              'args'    => array( 'stf_get_files' ),
              'default' => 'all',
            ),

            array(
                'id'       => $prefix . 'opt-post-type',
                // 'required' => array( 'opt-id', '=', true ),
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('CPT Include', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'select2' => array( 'allowClear' => true ),

                'ajax_save' => true,
                'data' => 'post_type'

            ),
            //http://docs.reduxframework.com/core/fields/select/

            array(
                'title'    => __( 'Sort by', 'pzarchitect' ),
                'id'       => $prefix . 'orderby',
                'type'     => 'button_set',
                'default'  => 'date',
                'cols'     => 6,
                'options'  => array(
                  'date'       => 'Date',
                  'title'      => 'Title',
                  'menu_order' => 'Page order (pages only)',
                  'post__in'   => 'Specified',
                  'rand'       => 'Random',
                  'none'       => 'None',
                  'custom'     => 'Custom field'
                ),
                'desc'=>__('Specified only works when the if the source is using the specific posts/page/etc option.','pzarchitect'),
                'subtitle' => 'Some hosts disable random as it slows things down significantly on large sites. WPEngine does this. Look in WPE Dashboard, Advanced Configuration.'
              ),

              array(
                'title'   => __( 'Sort direction', 'pzarchitect' ),
                'id'      => $prefix . 'orderdir',
                'type'    => 'button_set',
                'default' => 'DESC',
                'options' => array(
                  'ASC'  => 'Ascending',
                  'DESC' => 'Descending',
                ),
              ),

              array(
                'title'   => __( 'Skip N posts', 'pzarchitect' ),
                'id'      => $prefix . 'skip',
                'type'    => 'spinner',
                'min'     => 0,
                'max'     => 9999,
                'step'    => 1,
                'default' => 0,
                'desc'    => '<strong style="color:tomato;">' . __( 'Note: Skipping breaks pagination. This is a known WordPress issue. Also, skipping does not work if no post limit set. Again a WP limitation. Use a high number of posts to show as a workaround', 'pzarchitect' ) . '</strong>',
                //                      'required'=>array(
                //                          array('_blueprints_pagination-per-page','=',false),
                ////                          array('_blueprints_section-0-panels-limited','=',false),
                //                      )
              ),

              array(
                'title'   => __( 'Sticky posts first', 'pzarchitect' ),
                'id'      => $prefix . 'sticky',
                'type'    => 'switch',
                'on'      => 'Yes',
                'off'     => 'No',
                'default' => false,
              ),

              //array(
              //'title'  => __('Pagination', 'pzarchitect'),
              //'id'     => $prefix . 'pagination-heading-start',
              //'type'   => 'section',
              //'indent' => false
              //),

              array(
                'title'  => __( 'Categories', 'pzarchitect' ),
                'id'     => $prefix . 'categories-heading-start',
                'type'   => 'section',
                'class'  => ' heading',
                'indent' => true
              ),

              array(
                'title'   => __( 'Include categories', 'pzarchitect' ),
                'id'      => $prefix . 'inc-cats',
                'type'    => 'select',
                'select2' => array( 'allowClear' => true ),
                //                'wpqv'    => 'category__in',
                'data'    => 'category',
                'multi'   => true
              ),

              array(
                'title'   => __( 'In ANY or ALL categories', 'pzarchitect' ),
                'id'      => $prefix . 'all-cats',
                'type'    => 'button_set',
                'options' => array( 'any' => 'Any', 'all' => 'All' ),
                //               'wpqv'    => 'category__and',
                'default' => 'any',
              ),

              array(
                'title'   => __( 'Exclude categories', 'pzarchitect' ),
                'id'      => $prefix . 'exc-cats',
                'type'    => 'select',
                'select2' => array( 'allowClear' => true ),
                //                'wpqv'  => 'category__not_in',
                'data'    => 'category',
                'multi'   => true
              ),

              array(
                'title'    => __( 'Include sub-categories on archives', 'pzarchitect' ),
                'id'       => $prefix . 'sub-cats',
                'type'     => 'switch',
                'on'       => 'Yes',
                'off'      => 'No',
                'default'  => false,
                'subtitle' => 'This requires a specified post type, not Defaults'
              ),

              array(
                'id'     => $prefix . 'categories-section-end',
                'type'   => 'section',
                'indent' => false
              ),

              array(
                'title'  => __( 'Tags', 'pzarchitect' ),
                'id'     => $prefix . 'tags-section-start',
                'type'   => 'section',
                'class'  => ' heading',
                'indent' => true
              ),

              array(
                'title' => __( 'Tags', 'pzarchitect' ),
                'id'    => $prefix . 'inc-tags',
                'type'  => 'select',
                //              'select2' => array('allowClear' => true),
                'data'  => 'tags',
                'multi' => true
              ),

              array(
                'title'   => __( 'Exclude tags', 'pzarchitect' ),
                'id'      => $prefix . 'exc-tags',
                'type'    => 'select',
                'select2' => array( 'allowClear' => true ),
                'data'    => 'tags',
                'multi'   => true
              ),

              array(
                'id'     => $prefix . 'tags-section-end',
                'type'   => 'section',
                'indent' => false
              ),

              array(
                'title'  => __( 'Custom taxonomies', 'pzarchitect' ),
                'id'     => $prefix . 'custom-taxonomies-section-start',
                'type'   => 'section',
                'class'  => ' heading',
                'indent' => true
              ),

              // TODO: Add a loop to display all custom taxonomies
              // foreach($taxonomies as $taxonomy ){}
              array(
                'title'   => __( 'Other taxonomies', 'pzarchitect' ),
                'id'      => $prefix . 'other-tax',
                'type'    => 'select',
                'select2' => array( 'allowClear' => true ),
                'data'    => 'taxonomies',
                'args'    => array( '_builtin' => false )
              ),

              array(
                'title'    => __( 'Other taxonomy terms', 'pzarchitect' ),
                'id'       => $prefix . 'other-tax-tags',
                'type'     => 'select',
                'select2'  => array( 'allowClear' => true ),
                'data'     => 'callback',
                'multi'    => true,

                'args'     => array( 'xtw_get_tags_2' ),

                'subtitle' => __( 'Select terms to filter by in the chosen custom taxonomy', 'pzarchitect' ),
                'desc'     => __( 'To populate this dropdown, select the Custom Taxonomy above, then Publish or Update this Blueprint', 'pzarchitect' )
              ),


              array(
                'title'    => __( 'Other taxonomy terms my way', 'pzarchitect' ),
                'id'       => $prefix . 'other-tax-tags-my-way',
                'type'     => 'select',
                'select2'  => array( 'allowClear' => true ),
                'data'     => 'terms',
                'multi'    => true,
                'args'     => array('taxonomies' => array('tragac')),
                // 'subtitle' => __( 'Select terms to filter by in the chosen custom taxonomy', 'pzarchitect' ),
                // 'desc'     => __( 'To populate this dropdown, select the Custom Taxonomy above, then Publish or Update this Blueprint', 'pzarchitect' )
              ),

              array(
                'title'   => __( 'Taxonomies operator', 'pzarchitect' ),
                'id'      => $prefix . 'tax-op',
                'type'    => 'button_set',
                'options' => array( 'AND' => 'All', 'IN' => 'Any', 'NOT IN' => 'None' ),
                'default' => 'IN',
                'hint'    => array( 'content' => __( 'Display posts containing all, any or none of the taxonomies', 'pzarchitect' ) ),
              ),

              //TODO: Add taxomonies to exclude
              //    array(
              //      'title' => __('Days to show', 'pzarchitect'),
              //      'id' => $prefix . 'days',
              //      'type' => 'text',
              //      'cols'=>6,
              //              //      'default' => 'All',
              //    ),
              //    array(
              //      'title' => __('Days to show until', 'pzarchitect'),
              //      'id' => $prefix . 'days-until',
              //      'type' => 'text',
              //      'cols'=>6,
              //
              //    ),

              array(
                'id'     => $prefix . 'custom-taxonomies-section-end',
                'type'   => 'section',
                'indent' => false
              ),

              array(
                'title'  => __( 'Others', 'pzarchitect' ),
                'id'     => $prefix . 'other-section-start',
                'type'   => 'section',
                'class'  => ' heading',
                'indent' => true
              ),

              array(
                'title'   => __( 'Authors', 'pzarchitect' ),
                'id'      => $prefix . 'authors',
                'type'    => 'select',
                'data'    => 'callback',
                'args'    => array( 'pzarc_get_authors' ),
                'default' => 'all',
              ),

              array(
                'id'     => $prefix . 'other-section-end',
                'type'   => 'section',
                'indent' => false
              ),
        )
    );

/**
 Section 2
**/
    $boxSections[ ] = array(
            'title'  => __('Layout', 'redux-framework-demo'),
            'desc'   => __('', 'redux-framework-demo'),
            'icon'   => 'el-icon-home',
            'fields' => array(
                    # CODE

            )
    );

    $metaboxes = array();

    $metaboxes[ ]   = array(
            'id'         => 'query-generator',
            'title'      => __('Cool Options', 'redux-framework-demo'),
            'post_types' => array('query-generator'),
            'position'   => 'normal', // normal, advanced, side
            'priority'   => 'high', // high, core, default, low
            //'sidebar' => false, // enable/disable the sidebar in the normal/advanced positions
            'sections'   => $boxSections
    );

    $boxSections    = array();
        return $metaboxes;

    }

    $redux_opt_name = 'redux_tweaks';

    // Change {$redux_opt_name} to your opt_name

    // add_action("redux/metaboxes/{$redux_opt_name}/boxes", "redux_add_metaboxes_query", 10, 1);
endif;


?>
