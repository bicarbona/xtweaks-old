<?php 

/**
LESS Include
**/
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'   => 'el-icon-check',
        'title'  => __( 'LESS Module', 'redux-framework-demo' ),
        // 'desc'   => __( '<p class="description">This is the Description. Again HTML is allowed</p>', 'redux-framework-demo' ),
        'fields' => array(
            array(
                'id'       => 'opt-main-less',
                'type'     => 'switch',
                'title'    => __( 'Main', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '1'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-arrow-icons',
                'type'     => 'switch',
                'title'    => __( 'Arrow Icons', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

           array(
                'id'       => 'opt-less-typography',
                'type'     => 'switch',
                'title'    => __( 'Typography', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-buttons',
                'type'     => 'switch',
                'title'    => __( 'Buttons', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),
            
            array(
                'id'       => 'opt-less-wrappers',
                'type'     => 'switch',
                'title'    => __( 'Wrappers', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-labels-badges',
                'type'     => 'switch',
                'title'    => __( 'Labels / Badges', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-alert-higlight',
                'type'     => 'switch',
                'title'    => __( 'Alert / Higlight', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-ss-gizmo',
                'type'     => 'switch',
                'title'    => __( 'Icons SS Gizmo', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-animations',
                'type'     => 'switch',
                'title'    => __( 'Animations', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),


            array(
                'id'       => 'opt-less-colorbox',
                'type'     => 'switch',
                'title'    => __( 'Colorbox', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                'desc'     => __( 'Colorbox - Jquery Lightbox - *popup photo', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),


            /**
            PRESUNUT
            **/
            
            array(
                'id'       => 'opt-less-dermoxen-icons',
                'type'     => 'switch',
                'title'    => __( 'Dermoxen Icons', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),
        )
    )
);

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Admin', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
    
            array(
                'id'       => 'opt-admin-tweaks',
                'type'     => 'switch',
                'title'    => __( 'Admin tweaks', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '1'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-admin-style',
                'type'     => 'switch',
                'title'    => __( 'Admin Style', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

        )
    )
);

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Plugins', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
    
           array(
                'id'       => 'opt-less-plugins',
                'type'     => 'switch',
                'title'    => __( 'Plugins', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '1'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-woocommerce',
                'type'     => 'switch',
                'title'    => __( 'Woocommerce', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-wp-pagenavi',
                'type'     => 'switch',
                'title'    => __( 'WP pagenavi', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-webflow-grid',
                'type'     => 'switch',
                'title'    => __( 'Webflow Grid', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '0'// 1 = on | 0 = off
            ),

        )
    )
);

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Headway', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(
    
            array(
                'id'       => 'opt-less-add-fix-headway',
                'type'     => 'switch',
                'title'    => __( 'Fix Headway', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '1'// 1 = on | 0 = off
            ),

            array(
                'id'       => 'opt-less-disable-min-height',
                'type'     => 'switch',
                'title'    => __( 'Disable Min Height', 'redux-framework-demo' ),
                // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
                // 'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
                'default'  => '1'// 1 = on | 0 = off
            ),

        )
    )
);


?>