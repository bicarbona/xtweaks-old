<?php 

/**

Readme

**/
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-adjust',
        'title'      => __( 'Buttons', 'redux-framework-demo' ),
        // 'subsection' => true,
        'fields'     => array(

            array(
                'id'       => 'opt-button-color-firm',
                'type'     => 'color_rgba',
                'title'    => __( 'Firm', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#34485e', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),
            
            array(
                'id'       => 'opt-button-color-base',
                'type'     => 'color_rgba',
                'title'    => __( 'Base', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#000000', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

        )
    )
);

?>