<?php 

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-adjust',
        'title'      => __( 'Color', 'redux-framework-demo' ),
        // 'subsection' => true,
        'fields'     => array(
        // array(
        //     'id'       => 'opt-select-stylesheet',
        //     'type'     => 'select',
        //     'title'    => __( 'Theme Stylesheet', 'redux-framework-demo' ),
        //     'subtitle' => __( 'Select your themes alternative color scheme.', 'redux-framework-demo' ),
        //     'options'  => array( 'default.css' => 'default.css', 'color1.css' => 'color1.css' ),
        //     'default'  => 'default.css',
        // ),

            array(
                'id'       => 'opt-color-firm',
                'type'     => 'color_rgba',
                'title'    => __( 'Firm', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#34485e', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),
            
            array(
                'id'       => 'opt-color-base',
                'type'     => 'color_rgba',
                'title'    => __( 'Base', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#000000', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-white',
                'type'     => 'color_rgba',
                'title'    => __( 'White', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#ffffff', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

           array(
                'id'       => 'opt-color-black',
                'type'     => 'color_rgba',
                'title'    => __( 'Black', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#000000', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-red',
                'type'     => 'color_rgba',
                'title'    => __( 'Red', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#fb4527', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-green',
                'type'     => 'color_rgba',
                'title'    => __( 'Green', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#37cda1', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-blue',
                'type'     => 'color_rgba',
                'title'    => __( 'Blue', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#59a9ee', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-yellow',
                'type'     => 'color_rgba',
                'title'    => __( 'Yellow', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#ffcb00', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-orange',
                'type'     => 'color_rgba',
                'title'    => __( 'Orange', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#fc8c00', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-info',
                'type'     => 'color_rgba',
                'title'    => __( 'Info', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#3498db', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-success',
                'type'     => 'color_rgba',
                'title'    => __( 'Success', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#2dcb70', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-warning',
                'type'     => 'color_rgba',
                'title'    => __( 'Warning', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#f0c42c', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-danger',
                'type'     => 'color_rgba',
                'title'    => __( 'Danger', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#e74c3c', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-inverse',
                'type'     => 'color_rgba',
                'title'    => __( 'Inverse', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#ffffff', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-gray',
                'type'     => 'color_rgba',
                'title'    => __( 'Gray', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#c3c3c3', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),

            array(
                'id'       => 'opt-color-violet',
                'type'     => 'color_rgba',
                'title'    => __( 'Violet', 'redux-framework-demo' ),
                // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
                'default'  => array( 'color' => '#9b82d7', 'alpha' => '1.0' ),
                'validate' => 'colorrgba',
            ),
        )
    )
);

?>