<?php 

/**

Readme

**/

if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
    Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
        array(
            'icon'   => 'el-icon-list-alt',
            'title'  => __( 'Documentation', 'redux-framework-demo' ),
            'submenu' => false,
            'customizer' => false,
            'fields' => array(
                array(
                    'id'       => '17',
                    'type'     => 'raw',
                    'markdown' => true,
                    'content'  => file_get_contents( dirname( __FILE__ ) . '/../README.md' )
                ),
            )
        )
    );
}
/**

TODO

**/
if ( file_exists( dirname( __FILE__ ) . '/../TODO.md' ) ) {
    Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
        array(
            'icon'   => 'el-icon-list-alt',
            'title'  => __( 'Todo', 'redux-framework-demo' ),
            'submenu' => false,
            'subsection' => true,
            'customizer' => 'false',
            'fields' => array(
                array(
                    'id'       => '18',
                    'type'     => 'raw',
                    'markdown' => true,
                    'content'  => file_get_contents( dirname( __FILE__ ) . '/../TODO.md' )
                ),
            )
        )    
    );
}

?>