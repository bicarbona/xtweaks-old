<?php 

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-braille',
        'title'      => __( 'CPT', 'redux-framework-demo' ),
        'submenu' => true,
        'subsection' => false,
        'fields'     => array(
        /*has_archive
        "labels" => $labels,
                "description" => "",
                "public" => true,
                "show_ui" => true,
                "has_archive" => false,
                "show_in_menu" => true,
                'menu_position' => 10,
                "exclude_from_search" => false,
                "capability_type" => "post",
                "map_meta_cap" => true,
                "hierarchical" => false,
                "rewrite" => array( "slug" => "recipes", "with_front" => true ),
           */
        )
    )
);


include_once 'cpt/redux-config-cpt-content-box.php';

include_once 'cpt/redux-config-cpt-popup.php';

include_once 'cpt/redux-config-cpt-slider.php';

include_once 'cpt/redux-config-cpt-portfolio.php';

include_once 'cpt/redux-config-cpt-skusenosti.php';

include_once 'cpt/redux-config-cpt-poradna.php';

include_once 'cpt/redux-config-cpt-diary.php';

include_once 'cpt/redux-config-cpt-recipes.php';

include_once 'cpt/redux-config-cpt-myths.php';

?>