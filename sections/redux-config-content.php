<?php

/**
  Section Content  
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
  array(
    'icon'       => 'el-icon-website',
    'title'      => __( 'Content', 'redux-framework-demo' ),
    // 'submenu' => false,
    // 'subsection' => true,
    'fields'     => array(
    array(
      'id'       => 'opt-wp-adminbar-zindex',
      'type'     => 'text',
      'title'    => __('WP Adminbar Z-index', 'redux-framework-demo'),
      // 'subtitle' => __('subtitle', 'redux-framework-demo'),
      // 'desc'     => __('desc', 'redux-framework-demo'),
      'default'  => '4000',
      'ajax_save' => true
    ),

    array(
      'id'       => 'opt-hw-fullwidth-wrapper',
      'type'     => 'switch',
      'title'    => __('Full Width Wrapper', 'redux-framework-demo'),
      // 'subtitle' => __('subtitle', 'redux-framework-demo'),
      // 'desc'     => __('desc', 'redux-framework-demo'),
          'default'  => 0,
      'ajax_save' => true
    ),   

    array(
          'id'       => 'opt-hw-fullwidth-wrapper-id',
          'required' => array( 'opt-hw-fullwidth-wrapper', '=', true ),
          'type'     => 'text',
          'title'    => __('Full Width Wrapper ID', 'redux-framework-demo'),
          // 'subtitle' => __('subtitle', 'redux-framework-demo'),
          // 'desc'     => __('desc', 'redux-framework-demo'),
            'default'  => 'some',

          'ajax_save' => true
      ),  

          // array(
          //       'id'       => 'opt-required-nested',
          //       'type'     => 'switch',
          //       'title'    => 'Nested Required Example',
          //       'subtitle' => 'Click <code>On</code> to see another set of options appear.',
          //       'default'  => false
          //   ),

            // array(
            //     'id'       => 'opt-required-nested-buttonset',
            //     'type'     => 'button_set',
            //     'title'    => 'Multiple Nested Required Examples',
            //     'subtitle' => 'Click any buton to show different fields based on their <code>required</code> statements.',
            //     'options'  => array(
            //         'button-text'     => 'Show Text Field',
            //         'button-textarea' => 'Show Textarea Field',
            //         'button-editor'   => 'Show WP Editor',
            //         'button-ace'      => 'Show ACE Editor'
            //     ),
            //     'required' => array( 'opt-required-nested', '=', true ),
            //     'default'  => 'button-text'
            // ),
            // array(
            //     'id'       => 'opt-required-nested-text',
            //     'type'     => 'text',
            //     'title'    => 'Nested Text Field',
            //     'required' => array( 'opt-required-nested-buttonset', '=', 'button-text' )
            // ),
            // array(
            //     'id'       => 'opt-required-nested-textarea',
            //     'type'     => 'textarea',
            //     'title'    => 'Nested Textarea Field',
            //     'required' => array( 'opt-required-nested-buttonset', '=', 'button-textarea' )
            // ),
            // array(
            //     'id'       => 'opt-required-nested-editor',
            //     'type'     => 'editor',
            //     'title'    => 'Nested Editor Field',
            //     'required' => array( 'opt-required-nested-buttonset', '=', 'button-editor' )
            // ),
            // array(
            //     'id'       => 'opt-required-nested-ace',
            //     'type'     => 'ace_editor',
            //     'title'    => 'Nested ACE Editor Field',
            //     'required' => array( 'opt-required-nested-buttonset', '=', 'button-ace' )
            // ),
            array(
                'id'   => 'opt-required-divide-2',
                'type' => 'divide'
            ),
      )
    )
);

?>