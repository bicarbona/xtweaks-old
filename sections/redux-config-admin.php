<?php

/**
 Admin Section
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'icon'       => 'el-icon-website',
    'title'      => __( 'Admin', 'redux-framework-demo' ),
    // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
    // 'subsection' => true,
    'id'      => 'section_admin',
    // 'fields' => array(
    //                   )
    )
);

/**
 Admin Subsections
*/
include_once 'admin/redux-config-admin-content.php';
include_once 'admin/redux-config-admin-login.php';
include_once 'admin/redux-config-admin-metaboxes.php';
include_once 'admin/redux-config-admin-widgets.php';
include_once 'admin/redux-config-admin-colors.php';
include_once 'admin/redux-config-admin-dev.php';
include_once 'admin/redux-config-admin-other.php';
include_once 'admin/redux-config-admin-colored-post-list.php';

?>