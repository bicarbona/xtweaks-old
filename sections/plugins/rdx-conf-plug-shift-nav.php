<?php

/**

Shift Nav

**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-broom',
        'title'      => __( 'Shift Nav', 'redux-framework-demo' ),
        'subsection' => true,
        'fields'     => array(

             array(
                'id'   => 'info-shiftnav-general',
                'type' => 'info',
                'title'    => __('General', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                'id'       => 'opt-shiftnav-plugin',
                'type'     => 'switch', 
                'title'    => __('Enable Shift Nav', 'redux-framework-demo'),
                // 'subtitle' => __('', 'redux-framework-demo'),
                'default'  => true,
            ),

            array(
                'id'       => 'opt-shiftnav-edge',
                'type'     => 'radio',
                'title'    => __('Edge', 'redux-framework-demo'), 
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'options'  => array(
                    'left' => 'Left', 
                    'right' => 'Right',
                ),
                'default' => 'left'
            ),

/**
BreakPoint
**/
            array(
                'id'       => 'opt-shiftnav-togglebar-breakpoint',
                'type'     => 'text',
                'title'    => __('Toggle Breakpoint', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Show the toggle bar only below this pixel width. Leave blank to show at all times. Do not include "px"', 'redux-framework-demo'),

                'default'  => '800',
                'ajax_save' => true
            ),


            array(
                'id'       => 'opt-shiftnav-swipe-open',
                'type'     => 'switch', 
                'title'    => 'Swipe',
                // 'subtitle' => 'subtitle',
                'desc' => ' Swipe to open the main ShiftNav Panel in iOS and Android. Not all themes will be compatible, as touch swipes can conflict with theme scripts. Make sure enabling this does not prevent other touch functionality on your site from working',
                'default'  => true,
                'ajax_save' => true,
            ),

            // array(
            //     'id'       => 'opt-shiftnav-skin',
            //     'type'     => 'select',
            //     'multi'    => false,
            //     'title'    => __('Skin', 'redux-framework-demo'), 
               
            //     'ajax_save' => true,
            //     'options'  => array(
            //         '1' => 'Opt 1',
            //         '2' => 'Opt 2',
            //         '3' => 'Opt 3'),
            //     'default'  => array('2')
            // ),

            array(
                'id'       => 'opt-shiftnav-submenu',
                'type'     => 'switch', 
                'title'    => 'Submenu Visibility',
                // 'subtitle' => '',
                'desc'      => 'Check this to indent submenu items of always-visible submenus',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-shiftnav-site-title',
                'type'     => 'switch', 
                'title'    => 'Display site title',
                // 'subtitle' => 'Display Site Title',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-shiftnav-disable-menu',
                'type'     => 'switch', 
                'title'    => 'Disable Menu',
                'subtitle' => '',
                'desc' => 'Check this to disable the menu entirely; the panel can be used for custom content',
                'default'  => true,
                'ajax_save' => true,
            ),

/**
Content Before / After
**/

            array(
                'id'   => 'info-custom-info-before-after',
                'type' => 'info',
                'title'    => __('Custom Info Before / After Menu', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                'id'               => 'opt-shiftnav-content-before',
                'type'             => 'editor',
                'title'            => __('Custom Content Before Menu', 'redux-framework-demo'), 
                // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                // 'default'          => 'default',
                'args'   => array(
                    'teeny'            => true, // minimal settings
                    'textarea_rows'    => 10,
                    'wpautop'            => true, //    Flags to set wpautop for adding paragraphs.
                )
            ),

            array(
                'id'               => 'opt-shiftnav-content-after',
                'type'             => 'editor',
                'title'            => __('Custom Content After Menu', 'redux-framework-demo'), 
                // 'subtitle'         => __('subtitle', 'redux-framework-demo'),
                // 'default'          => 'default',
                'args'   => array(
                    'teeny'            => true, // minimal settings
                    'textarea_rows'    => 10,
                    'wpautop'            => true, //    Flags to set wpautop for adding paragraphs.
                )
            ),

            array(
                'id'       => 'opt-shiftnav-togglebar',
                'type'     => 'switch', 
                'title'    => 'Display Toggle Bar',
                // 'subtitle' => 'subtitle',
                'default'  => true,
                'ajax_save' => true,
            ),

/**

Toggle Bar

**/

              array(
                 'id'   => 'info-shiftnav-toggle-bar',
                 'type' => 'info',
                 'title'    => __('Toggle Bar', 'redux-framework-demo'),
                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                 //'desc'     => __('Desc', 'redux-framework-demo'),
                 'notice' => true,
                 //'icon'  => 'el-icon-info-sign',
                 'style' => 'success', // warning|critical|info
             ),

             array(
                 'id'       => 'opt-shiftnav-toggle-content',
                 'type'     => 'ace_editor',
                 'full_width' => false,

                 'title'    => __('Toggle Content', 'redux-framework-demo'),
                 'subtitle' => __('[shift_toggle_title]', 'redux-framework-demo'),
                 'mode'     => 'plain_text', // css, html, javascript, json, less, markdown, mysql, php, plain_text, sass, scss, text, xml
                 'theme'    => 'monokai',
                 'desc'     => '',
                 'default' => '[shift_toggle_title]',
                 'ajax_save' => true,
             ),

             array(
                 'id'       => 'opt-shiftnav-close-icon',
                 'type'     => 'radio',
                 'title'    => __('Close Icon', 'redux-framework-demo'), 
                 'desc' => __('When the toggle is open, choose which icon to display.', 'redux-framework-demo'),
                 // 'desc'     => __('desc', 'redux-framework-demo'),
                 //Must provide key => value pairs for radio options
                 'ajax_save' => true,
                 'options'  => array(
                     'bars' => ' Hamburger Bars', 
                     'x' => ' Close button', 
                 ),
                 'default' => '1'
             ),

             array(
                 'id'       => 'opt-shiftnav-togglebar-position',
                 'type'     => 'select',
                 'multi'    => false,
                 'title'    => __('Toggle Bar Position', 'redux-framework-demo'), 
                 // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                 'desc'     => __('Choose Fixed if youd like the toggle bar to always be visible, or Absolute if youd like it only to be visible when scrolled to the very top of the page
    ', 'redux-framework-demo'),
                 //Must provide key => value pairs for radio options
                 'ajax_save' => true,
                 'options'  => array(
                     '1' => 'Fixed',
                     '2' => 'Absolute'),
                 'default'  => array('2','3')
             ),

             array(
                 'id'       => 'opt-shiftnav-align-text',
                 'type'     => 'select',
                 'multi'    => false,
                 'title'    => __('Align Text', 'redux-framework-demo'), 
                 'desc' => __('Align text left, right, or center. Applies to inline elements only.', 'redux-framework-demo'),
                 // 'desc'     => __('desc, again good for additional info.', 'redux-framework-demo'),
                 //Must provide key => value pairs for radio options
                 'ajax_save' => true,
                 'options'  => array(
                     '1' => 'Center',
                     '2' => 'Left',
                     '3' => 'Right'),
                 'default'  => array('1')
             ),

             array(
                 'id'        => 'opt-shiftnav-togglebar-background',
                 'type'      => 'color_rgba',
                 'title'     => 'Toggle Bar Background',
                 // 'subtitle'  => 'subtitle',
                 // 'desc'      => 'desc',
                  'ajax_save' => true,
                 // See Notes below about these lines.
                 //'output'    => array('background-color' => '.site-header'),
                 //'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
                 'default'   => array(
                     'color'     => 'true',
                     'alpha'     => 1
                 ),
              
                 // These options display a fully functional color palette. Omit this argument
                 // for the minimal color picker, and change as desired.
                 'options'       => array(
                     'show_input'                => true,
                     'show_initial'              => true,
                     'show_alpha'                => true,
                     'show_palette'              => false,
                     'show_palette_only'         => false,
                     'show_selection_palette'    => true,
                     'max_palette_size'          => 10,
                     'allow_empty'               => true,
                     'clickout_fires_change'     => false,
                     'choose_text'               => 'Choose',
                     'cancel_text'               => 'Cancel',
                     'show_buttons'              => true,
                     'use_extended_classes'      => true,
                     'palette'                   => null,  // show default
                     'input_text'                => 'Select Color'
                 ),                        
             ),

             array(
                 'id'        => 'opt-shiftnav-togglebar-text-color',
                 'type'      => 'color_rgba',
                 'title'     => 'Toggle Bar Text Color',
                 // 'subtitle'  => 'subtitle',
                 // 'desc'      => 'desc',
                  'ajax_save' => true,
                 // See Notes below about these lines.
                 //'output'    => array('background-color' => '.site-header'),
                 //'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
                 'default'   => array(
                     'color'     => 'true',
                     'alpha'     => 1
                 ),
              
                 // These options display a fully functional color palette. Omit this argument
                 // for the minimal color picker, and change as desired.
                 'options'       => array(
                     'show_input'                => true,
                     'show_initial'              => true,
                     'show_alpha'                => true,
                     'show_palette'              => false,
                     'show_palette_only'         => false,
                     'show_selection_palette'    => true,
                     'max_palette_size'          => 10,
                     'allow_empty'               => true,
                     'clickout_fires_change'     => false,
                     'choose_text'               => 'Choose',
                     'cancel_text'               => 'Cancel',
                     'show_buttons'              => true,
                     'use_extended_classes'      => true,
                     'palette'                   => null,  // show default
                     'input_text'                => 'Select Color'
                 ),                        
             ),


            array(
                'id'       => 'opt-shiftnav-togglebar-text-size',
                'type'     => 'spinner', 
                'title'    => 'Toggle Bar Text Size',
                // 'subtitle' => 'subtitle',
                'desc'     => 'Override the default font size of the toggle bar by setting a value here.',
                'ajax_save' => true,
                'default'  => '10',
                'min'      => '8',
                'step'     => '1',
                'max'      => '100',
            ),

/**
Toggle Content
**/
             array(
                'id'   => 'info-toggle-content',
                'type' => 'info',
                'title'    => __('Toggle Content', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                 'id'       => 'opt-shiftnav-toggle-content-left-edge',
                 'type'     => 'ace_editor',
                 'title'    => __('Toggle Content Let Edge', 'redux-framework-demo'),
                 // 'subtitle' => __('', 'redux-framework-demo'),
                 'mode'     => 'plain_text', // css, html, javascript, json, less, markdown, mysql, php, plain_text, sass, scss, text, xml
                 'theme'    => 'monokai',
                 'desc'     => '',
                 'default' => '',
                 'ajax_save' => true,
            ),

             array(
                 'id'       => 'opt-shiftnav-toggle-content-right-edge',
                 'type'     => 'ace_editor',
                 'title'    => __('Toggle Content Right Edge', 'redux-framework-demo'),
                 // 'subtitle' => __('', 'redux-framework-demo'),
                 'mode'     => 'plain_text', // css, html, javascript, json, less, markdown, mysql, php, plain_text, sass, scss, text, xml
                 'theme'    => 'monokai',
                 'desc'     => '',
                 'default' => '',
                 'ajax_save' => true,
             ),

              array(
                 'id'   => 'info-icons',
                 'type' => 'info',
                 'title'    => __('Button and Text size', 'redux-framework-demo'),
                 //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                 //'desc'     => __('Desc', 'redux-framework-demo'),
                 'notice' => true,
                 //'icon'  => 'el-icon-info-sign',
                 'style' => 'success', // warning|critical|info
             ),


    //General Settings

             array(
                 'id'       => 'opt-shiftnav-button-size',
                 'type'     => 'select',
                 'multi'    => false,
                 'title'    => __('Button Size', 'redux-framework-demo'), 
                 // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                 'desc'     => __('The size of the padding on the links in the menu. The larger the setting, the easier to click; but fewer menu items will appear on the screen at a time.', 'redux-framework-demo'),
                 //Must provide key => value pairs for radio options
                 'ajax_save' => true,
                 'options'  => array(
                     '1' => 'Default',
                     '2' => 'Medium',
                     '3' => 'Large',
                     '4' => 'Extra Large'
                     ),
                 'default'  => array('2','3')
             ),

            array(
                 'id'       => 'opt-shiftnav-text-size',
                 'type'     => 'select',
                 'multi'    => false,
                 'title'    => __('Text Size', 'redux-framework-demo'), 
                 // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                 'desc'     => __('The size of the font on the links in the menu (will override all levels).', 'redux-framework-demo'),
                 //Must provide key => value pairs for radio options
                 'ajax_save' => true,
                 'options'  => array(
                     '1' => 'Default',
                     '2' => 'Medium',
                     '3' => 'Large',
                     '4' => 'Extra Large'
                     ),
                 'default'  => array('2','3')
             ),

/**

Shiftbody

**/

             array(
                'id'   => 'info-shit-body',
                'type' => 'info',
                'title'    => __('Shift Body', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

             array(
                'id'   => 'info-shiftnav-body',
                'type' => 'text',
                'title'    => __('Shift Body Wrapper', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                'desc'     => __('Leave this blank to automatically create a ShiftNav Wrapper via javascript (this may have side effects). Set a selector here to turn a specific div (which must wrap all body contents) into the wrapper. Please note that if the wrapper you select is also styled by the theme, this may cause a conflict.', 'redux-framework-demo'),
                // 'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                'id'       => 'opt-shiftnav-shift-body',
                'type'     => 'switch', 
                'title'    => 'Shift Body',
                'desc' => ' Shift the body of the site when the menu is revealed. For some themes, this may negatively affect the site content, so this can be disabled.',
                'default'  => true,
                'ajax_save' => true,
            ),


             array(
                'id'   => 'opt-shiftnav-body-wrapper',
                'type' => 'text',
                'title'    => __('Shift Body Wrapper', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                'desc'     => __('Leave this blank to automatically create a ShiftNav Wrapper via javascript (this may have side effects). Set a selector here to turn a specific div (which must wrap all body contents) into the wrapper. Please note that if the wrapper you select is also styled by the theme, this may cause a conflict.', 'redux-framework-demo'),
                // 'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

             array(
                'id'   => 'info-scroll-offset',
                'type' => 'info',
                'title'    => __('Scroll Offset', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),
/**
Scroll Offset
**/

            array(
                'id'       => 'opt-shiftnav-scroll-offset',
                'type'     => 'text',
                'title'    => __('Scroll Offset', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                'desc'     => __('When using the ScrollTo functionality, this is the number of pixels to offset the scroll by, to account for the toggle bar and any spacing you want.', 'redux-framework-demo'),
                //'validate' => 'email',
                //'msg'      => 'msg',
                'default'  => '100',
                'ajax_save' => true
            ),



/**
Touch-off close
**/            
            array(
                'id'       => 'opt-shiftnav-touch-off-close',
                'type'     => 'switch', 
                'title'    => __('Touch-off close', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                'desc'     => __('Close the ShiftNav panel when touching any content not in the panel.', 'redux-framework-demo'),
                'default'  => true,
                'ajax_save' => true,
            ),


/**

Disable Transforms & Transitions    

**/
            
            array(
                'id'       => 'opt-shiftnav-disable-transform',
                'type'     => 'switch', 
                'title'    => __('Disable Transforms', 'redux-framework-demo'),
                // 'subtitle' => __('', 'redux-framework-demo'),
                'desc'     => __('<strong>Disable CSS3 transformations and transitions</strong> This will disable smooth animations, but may work better on browsers that don\'t properly implement CSS3 transforms, especially old non-standard Android browsers. 
<br><br>
                    Note that ShiftNav attempts to detect these browsers and fall back automatically, but some browsers have incomplete implementations of CSS3 transforms, which produce false positives when testing.', 'redux-framework-demo'),
                'default'  => false,
                'ajax_save' => true,
            ),


/**
Swipe
**/
             array(
                'id'   => 'info-shiftnav-swipe',
                'type' => 'info',
                'title'    => __('Swipe', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                'id'       => 'opt-shiftnav-swipe-close',
                'type'     => 'switch', 
                'title'    => 'Swipe Close',
                'desc' => ' Enable swiping to close the ShiftNav panel on Android and iOS. Touch events may not interact well with all themes.',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-shiftnav-swipe-close-tolerance-horizontal',
                'type'     => 'spinner', 
                'title'    => 'Swipe Tolerance Horizontal',
                // 'subtitle' => 'subtitle',
                'desc'     => 'The minimum horizontal pixel distance before the swipe is triggered. Do not include px',
                'ajax_save' => true,
                'default'  => '150',
                'min'      => '20',
                'step'     => '1',
                'max'      => '300',
            ),

            array(
                'id'       => 'opt-shiftnav-swipe-close-tolerance-vertical',
                'type'     => 'spinner', 
                'title'    => 'Swipe Tolerance Vertical',
                // 'subtitle' => 'subtitle',
                'desc'     => 'The minimum vertical pixel distance before the swipe is triggered. Do not include px',
                'ajax_save' => true,
                'default'  => '60',
                'min'      => '20',
                'step'     => '1',
                'max'      => '300',
            ),


            array(
                'id'       => 'opt-shiftnav-swipe-edge-proximity',
                'type'     => 'spinner', 
                'title'    => 'Swipe Edge Proximity',
                // 'subtitle' => 'subtitle',
                'desc'     => 'The distance from the edge, within which the first touch event must occur for the swipe to be triggered. Do not include px',
                'ajax_save' => true,
                'default'  => '80',
                'min'      => '20',
                'step'     => '1',
                'max'      => '200',
            ),

/**
Menu
**/


             array(
                'id'   => 'info-shiftnav-meni',
                'type' => 'info',
                'title'    => __('Menu', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),
            array(
                'id'       => 'opt-shiftnav-open-curent-submenu',
                'type'     => 'switch', 
                'title'    => 'Open Current Accordion Submenu',
                'desc' => ' Open the submenu of the current menu item on page load (accordion submenus only).',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => ' ',
                'type'     => 'switch', 
                'title'    => 'Collapse Accordions',
                'desc' => 'When an accordion menu is opened, collapse any other accordions on that level.',
                'default'  => true,
                'ajax_save' => true,
            ),



            array(
                'id'       => 'opt-shiftnav-scroll-to-top',
                'type'     => 'switch', 
                'title'    => 'Scroll Shift Submenus to Top',
                'desc' => ' When a Shift submenu is activated, scroll that item to the top to maximize submenu visibility.',
                'default'  => true,
                'ajax_save' => true,
            ),

/**

Hover / Active

**/

             array(
                'id'   => 'info-shiftnav-highlight',
                'type' => 'info',
                'title'    => __('Higlight - Hover / Active', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                'id'       => 'opt-shiftnav-highlight-hover',
                'type'     => 'switch', 
                'title'    => 'Highlight Targets on Hover',
                'desc' => 'With this setting enabled, the links will be highlighted when hovered or touched.',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-shiftnav-highlight-active',
                'type'     => 'switch', 
                'title'    => 'Highlight Targets on active',
                'desc' => 'With this setting enabled, the links will be highlighted when hovered or touched.',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'   => 'info-shiftnav-show-tips',
                'type' => 'info',
                'title'    => __('Admin Tips', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),
/**
Show Tips
**/
            array(
                'id'       => 'opt-shiftnav-show-tips-admin',
                'type'     => 'switch', 
                'title'    => 'Show Tips to Admins',
                'desc' => 'Display tips to admin user',
                'default'  => true,
                'ajax_save' => true,
            ),
/**
Lock - Horizontal
**/
             array(
                'id'   => 'info-shiftnav-lock',
                'type' => 'info',
                'title'    => __('Lock', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                'id'       => 'opt-shiftnav-lock-horizontal-scroll',
                'type'     => 'switch', 
                'title'    => 'Lock Horizontal Scroll',
                'desc' => 'Attempt to prevent the content from scrolling horizontally when the menu is active. On some themes, may also prevent vertical scrolling. May not prevent touch scrolling in Chrome. No effect if Shift Body is disabled.',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-shiftnav-lock-horizontal-scroll-x',
                'type'     => 'switch', 
                'title'    => 'Lock Horizontal Scroll',
                'desc' => 'Lock both vertical and horizontal scrolling on site content when menu is active. No effect if Shift Body is disabled.',
                'default'  => true,
                'ajax_save' => true,
            ),

/**

Menu Hacks

**/
             array(
                'id'   => 'info-shiftnav-menus-hacks',
                'type' => 'info',
                'title'    => __('Menu Hacks', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),
            array(
                'id'       => 'opt-shiftnav-inherit-ubermenu-conditionals',
                'type'     => 'switch', 
                'title'    => 'Inherit UberMenu Conditionals',
                'desc' => 'Display menu items based on UberMenu Conditionals settings',
                'default'  => true,
                'ajax_save' => true,
            ),
            
            array(
                'id'       => 'opt-shiftnav-filter-menu-args',
                'type'     => 'switch', 
                'title'    => 'Force Filter Menu Args',
                'desc' => ' Some themes will filter the menu arguments on all menus on the site, which can break things. This will re-filter those arguments for ShiftNav menus only.',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-shiftnav-kill-menu-class',
                'type'     => 'switch', 
                'title'    => 'Kill Menu Class Filter',
                'desc' => 'Some themes filter the menu item classes and strip out core WordPress functionality. This will change the structure of ShiftNav and prevent styles from being applies. This will prevent any actions on the nav_menu_css_class filter.',
                'default'  => true,
                'ajax_save' => true,
            ),

/**
Hide Menu
**/

             array(
                'id'   => 'info-hide-menus',
                'type' => 'info',
                'title'    => __('Hide Menus', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

            array(
                'id'       => 'opt-shiftnav-hide-menu',
                'type'     => 'text',
                'title'    => __('Hide Theme Menu', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Enter the selector of the themes menu if you wish to hide it below the breakpoint above. For example, #primary-nav or .topnav.', 'redux-framework-demo'),
                'ajax_save' => true
            ),

            array(
                'id'       => 'opt-shiftnav-hide-uber-menu',
                'type'     => 'text',
                'title'    => __('Hide UberMenu', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Hide all UberMenus when ShiftNav is displayed. If you would like to only hide a specific UberMenu, use the setting above with a specific UberMenu ID. '),
                'ajax_save' => true
            ),

/**
Image
*/
             array(
                'id'   => 'info-shiftnav-image',
                'type' => 'info',
                'title'    => __('TEMPORARY', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'warning', // warning|critical|info
            ),

            array(
                'id'       => 'opt-shiftnav-top-image',
                'type'     => 'media', 
                'url'      => true,
                'title'    => __('Top Image', 'redux-framework-demo'),
                'ajax_save' => true,
                'default'   => array(
                    'url'   =>'http://s.wordpress.org/style/images/codeispoetry.png',
                    'mode'=> '', //String specifying either the file type or mime type of files to accept from the media library.
                ),
            ),

            array(
                'id'       => 'opt-shiftnav-pad-image',
                'type'     => 'switch', 
                'title'    => 'Pad Image',
                'desc' => 'Add padding to align image with menu item text. Uncheck to expand to the edges of the panel.',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-shiftnav-image-url',
                'type'     => 'text',
                'title'    => __('Image Link (URL)', 'redux-framework-demo'),
                'desc' => __('Make the image a link to this URL.', 'redux-framework-demo'),
                // 'default'  => 'default',
                'ajax_save' => true
            ),


            array(
                'id'   => 'opt-',
                'type' => 'info',
                    'style' => 'warning',

                'desc' => __('desc', 'redux-framework-demo')
            ),
        )
    )
);

?>