<?php 


/**

My sticky menu

**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-minus',
        'title'      => __( 'My sticky menu', 'redux-framework-demo' ),

        'subsection' => true,
        'fields'     => array(

        array(
            'id'       => 'opt-sticky-menu',
            'type'     => 'switch', 
            'title'    => __('Sticky menu', 'redux-framework-demo'),
            'subtitle' => __('*Potrebne main.less', 'redux-framework-demo'),
            'default'  => '0',
        ),

        array(
            'id'       => 'opt-sticky-menu-id-class',
            'type'     => 'text', 
            'title'    => __('Menu identificator', 'redux-framework-demo'),
            'subtitle' => __('Menu or Header div class or id', 'redux-framework-demo'),
           // 'default'  => true,
        ),

        array(
            'id'       => 'opt-sticky-zindex',
            'type'     => 'text', 
            'title'    => __('Menu z-index', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo'),
            'default'  => '9000',
        ),
        array(
            'id'       => 'opt-sticky-background',
            'type'     => 'color_rgba', 
            'title'    => __('Sticky Background Color', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo'),
            'default'  => array( 'color' => '#fff', 'alpha' => '1.0' ),
            'validate' => 'colorrgba',
           // 'default'  => '9000',
        ),

        array(
            'id'       => 'opt-sticky-transition-time',
            'type'     => 'spinner', 
            'title'    => __('Sticky Transition Time', 'redux-framework-demo'),
            'subtitle' => __(' in seconds', 'redux-framework-demo'),
            'default'       => 0.3,
            'min'           => 0,
            'step'          => 0.1,
            'resolution'    => 0.1,
            'max'           => 3,
            'float_mark'    => '.',
            'display_value' => 'text'
            //'default'  => '9000',
        ),

        array(
            'id'       => 'opt-sticky-disable-small-screen',
            'type'     => 'text', 
            'title'    => __('Disable at Small Screen Sizes', 'redux-framework-demo'),
            'subtitle' => __('less than   px width, 0 to disable. in seconds', 'redux-framework-demo'),
            'default'  => '359',
        ),

        array(
            'id'       => 'opt-sticky-active-on-height',
            'type'     => 'text', 
            'title'    => __('Make visible when scroled', 'redux-framework-demo'),
            'subtitle' => __('after .... px scroll', 'redux-framework-demo'),
            'default'  => '320',
        ),

        array(
            'id'       => 'opt-sticky-make-visible-homepage',
            'type'     => 'text', 
            'title'    => __('Make visible when scroled on homepage', 'redux-framework-demo'),
            'subtitle' => __('after .... px scroll', 'redux-framework-demo'),
            'default'  => '320',
        ),

        array(
            'id'       => 'opt-sticky-fade-slide',
            'type'     => 'button_set', 
            'title'    => __('Fade or slide effect', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo'),
             'options' => array(
                'fade' => 'Fade', 
                'slide' => 'Slide', 
             ), 
            'default' => 'fade'
        ),

        array(
            'id'       => 'opt-sticky-custom-style',
            'type'     => 'textarea', 
            'title'    => __('Add / Edit CSS', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo'),
            'description' => '.myfixed { margin:0 auto!important; float:none!important; border:0px!important; background:none!important; max-width:100%!important; }',
            'default'  => '.myfixed { margin:0 auto!important; float:none!important; border:0px!important; background:none!important; max-width:100%!important; }',
            ),
        )
    )
);
 ?>