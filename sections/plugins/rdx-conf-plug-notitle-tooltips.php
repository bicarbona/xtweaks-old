<?php 

/**

No title tooltips

**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks

 array(
        'icon'       => 'el-icon-broom',
        'title'      => __( 'No title tooltips', 'redux-framework-demo' ),
        'subsection' => true,
        'fields'     => array(

        array(
            'id'       => 'opt-no-tttips',
            'type'     => 'switch', 
            'title'    => __('No title tooltips', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo'),
            'default'  => true,
        ),
    ),
    ) 
);

?>