<?php 

/**
* Woocommerce Plugin
**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Woocommerce', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(

            //Products Archives
            //Product Columns  The number of columns that products are arranged in to on archives
/**
Products Columns Loop
 */            array(
                'id'       => 'opt-woo-product-columns',
                'type'     => 'select',
                'title'    => __('Columns', 'redux-framework-demo'),
               'subtitle' => __('Loop Archive', 'redux-framework-demo'),
                'desc'     => __('Product Columns  The number of columns that products are arranged in to on archives', 'redux-framework-demo'),
                'ajax_save' => true,
                'options' => array(
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                ),
                'default'  => '4',
            ),
/**
Products per page
*/

            array(
                'id'       => 'opt-woo-products-per-page',
                'type'     => 'text',
                'title'    => __('Per page', 'redux-framework-demo'),
                'subtitle' => __('Loop Archive', 'redux-framework-demo'),
                'desc'     => __(' The number of products displayed per page', 'redux-framework-demo'),
                'default'  => '10',
                'ajax_save' => true
            ),

/**
Catalog mode
 */
            array(
                'id'       => 'opt-woo-catalog-mode',
                'type'     => 'switch', 
                'title'    => 'Catalog Mode',
                'desc' => 'Enable this setting to set the products into catalog mode, with no cart or checkout process.',
                'default'  => false,
                'ajax_save' => true,
            ),

            array(
                'id'   => 'info-woo-products-sorting',
                'type' => 'info',
                'title' => __('Sorting - Result Count', 'redux-framework-demo'),
                // 'desc' => __('desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info
            ),

/**
Display Products Sorting

    moznost pridat odstranenie konkretnych poloziek v sortingu
    xtw_woo_catalog_orderby
*/
            array(
                'id'       => 'opt-woo-products-sorting',
                'type'     => 'switch', 
                'title'    => 'Sorting',
                'subtitle' => 'Loop Archive',
                'default'  => false,
                'ajax_save' => true,
            ),

/**
Result Count Loop
 */
            array(
                'id'       => 'opt-woo-product-result-count-loop',
                'type'     => 'switch', 
                'title'    => 'Results Count',
                'subtitle' => 'Loop Archive',
                'default'  => false,
                'ajax_save' => true,
            ),

/**
Temp
*/

array(
    'id'      => 'opt-woo-sortable',
    'type'    => 'sorter',
    'title'   => 'Single Product Layout',
    // 'desc'    => 'Organize how you want the layout to appear on the homepage',
    'options' => array(
        'kokot'  => array(
            'single_title'      =>  'single_title',
            'add_to_cart'       =>  'add_to_cart',
            'single_price'      =>  'single_price',
            'single_excerpt'    =>  'single_excerpt',
            'single_rating'     =>  'single_rating',
            'single_meta'       =>  'single_meta',
            // 'services'   => 'Services'
        ),
        'pica' => array(
        )
    ),
),

/**
 Add To Cart
*/

    array(
        'id'   => 'info-woo-add-to-cart',
        'type' => 'info',
        'title' => __('Add to cart', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),
/**
 Add To Cart Loop
*/
        array(
            'id'       => 'opt-woo-loop-add-to-cart',
            'type'     => 'switch', 
            'title'    => 'Add to cart',
            'subtitle' => 'Loop Archive',
            'default'  => true,
            'ajax_save' => true,
        ),

/**
 Add To Cart Single
 */
        array(
            'id'       => 'opt-woo-single-add-to-cart',
            'type'     => 'switch', 
            'title'    => 'Add to cart',
            'subtitle' => 'Single Page',
            'default'  => true,
            'ajax_save' => true,
        ),

/**
Sale
*/
array(
    'id'   => 'info-woo-sale',
    'type' => 'info',
    'title' => __('Sale', 'redux-framework-demo'),
    // 'desc' => __('desc', 'redux-framework-demo'),
    'notice' => true,
    //'icon'  => 'el-icon-info-sign',
    'style' => 'success', // warning|critical|info
),

/**
 Sale Flashes Single
 */

            array(
                'id'       => 'opt-woo-sale-single',
                'type'     => 'switch', 
                'title'    => 'Sale flash',
                'subtitle' => 'Single Page',
                'default'  => true,
                'ajax_save' => true,
            ),
/**
 Sale Flashes Loop
 */
            array(
                'id'       => 'opt-woo-sale-loop',
                'type'     => 'switch', 
                'title'    => 'Sale flash',
                'subtitle' => 'Loop Archive',
                'default'  => true,
                'ajax_save' => true,
            ),

/**
Review
*/

 array(
        'id'   => 'info-woo-review',
        'type' => 'info',
        'title' => __('Review', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

/**
 Review Single Page
 */
            array(
                'id'       => 'opt-woo-single-review',
                'type'     => 'switch', 
                'title'    => 'Review',
                'subtitle' => 'Single Page',
                'default'  => true,
                'ajax_save' => true,
            ),

/**
New badge
 */


 array(
        'id'   => 'info-woo-new-badge',
        'type' => 'info',
        'title' => __('New Badge', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

/**
New badge Loop
 */
            array(
                'id'       => 'opt-woo-new-badge',
                'type'     => 'switch', 
                'title'    => 'New Badge',
                'subtitle' => 'Loop Archive',
                'default'  => true,
                'ajax_save' => true,
            ),
/**
New badge Days
 */

            array(
                'id'       => 'opt-woo-new-badge-days',
                'type'     => 'text',
                'title'    => __('New Badge Days', 'redux-framework-demo'),
                'subtitle' => __('for how many days?', 'redux-framework-demo'),
                'default'  => '30',
                'ajax_save' => true
            ),

/**
New badge Text
 */

            array(
                'id'       => 'opt-woo-new-badge-text',
                'type'     => 'text',
                'title'    => __('New Badge Text', 'redux-framework-demo'),
                'default'  => 'New',
                'ajax_save' => true
            ),

/**
Product Images
 */


 array(
        'id'   => 'info-woo-product-images',
        'type' => 'info',
        'title' => __('Images', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),
/**
Images Single
 */
            array(
                'id'       => 'opt-woo-product-image-single',
                'type'     => 'switch', 
                'title'    => 'Image',
                'subtitle' => 'Single Page',
                'default'  => true,
                'ajax_save' => true,
            ),

/**
Images Loop
 */
            array(
                'id'       => 'opt-woo-product-image-loop',
                'type'     => 'switch', 
                'title'    => 'Images',
                'subtitle' => 'Loop Archive',
                'default'  => true,
                'ajax_save' => true,
            ),

/**
Price
*/

 array(
        'id'   => 'info-woo-product-price',
        'type' => 'info',
        'title' => __('Price', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),
/**
Price Single
 */
            array(
                'id'       => 'opt-woo-product-price-single',
                'type'     => 'switch', 
                'title'    => 'Price',
                'subtitle' => 'Single Page',
                'default'  => true,
                'ajax_save' => true,
            ),

/**
Price Loop
 */
            array(
                'id'       => 'opt-woo-product-price-loop',
                'type'     => 'switch', 
                'title'    => 'Price',
                'subtitle' => 'Loop Archive',
                'default'  => true,
                'ajax_save' => true,
            ),


 array(
        'id'   => 'info-woo-product-rating',
        'type' => 'info',
        'title' => __('Rating', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),
/**
Rating Single
 */
            array(
                'id'       => 'opt-woo-product-rating-single',
                'type'     => 'switch', 
                'title'    => 'Rating',
                'subtitle' => 'Single Page',
                'default'  => true,
                'ajax_save' => true,
            ),


/**
Rating Loop
 */
            array(
                'id'       => 'opt-woo-product-rating-loop',
                'type'     => 'switch', 
                'title'    => 'Rating',
                'subtitle' => 'Loop Archive',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-woo-product-sumary-single',
                'type'     => 'switch',
                'title'    => __('Product Sumary', 'redux-framework-demo'),
                'subtitle' => __('Single Page', 'redux-framework-demo'),
                'desc'     => __('Category etc', 'redux-framework-demo'),

                'default'  => false,
                'ajax_save' => true
            ),

            array(
                'id'       => 'id',
                'type'     => 'divide',
            ),


/**
Related Products
 */

     array(
        'id'   => 'info-woo-remove-related-products',
        'type' => 'info',
        'title' => __('Related Products', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

            array(
                'id'       => 'opt-woo-remove-related-products',
                'type'     => 'switch', 
                'title'    => 'Display related products ',
                'desc' => 'Zobrazit related products  // Single page ',
                'default'  => false,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-woo-external-link',
                'type'     => 'switch', 
                'title'    => 'External link',
                'subtitle' => 'Link na externy produkt / frontend',
                'default'  => false,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'id',
                'type'     => 'divide',
            ),


     array(
        'id'   => 'info-woo-custom-tabs',
        'type' => 'info',
        'title' => __('Custom Tabs', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

            array(
                'id'       => 'opt-woo-custom-tabs',
                'type'     => 'switch',
                'title'    => __('Custom Tabs', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc', 'redux-framework-demo'),
                'default'  => '',
                'ajax_save' => true
            ),

            array(
              'required' => array( 'opt-woo-custom-tabs', '=', true ),
                'id'       => 'opt-woo-custom-tab1',
                'type'     => 'text',
                'title'    => __('Custom tab 1', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc', 'redux-framework-demo'),
                'default'  => 'Použitie',
                'ajax_save' => true
            ),

            array(
                  'required' => array( 'opt-woo-custom-tabs', '=', true ),
                'id'       => 'opt-woo-custom-tab2',
                'type'     => 'text',
                'title'    => __('Custom tab 2', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc', 'redux-framework-demo'),
                'default'  => 'Zloženie',
                'ajax_save' => true
            ),

            array(
                  'required' => array( 'opt-woo-custom-tabs', '=', true ),
                'id'       => 'opt-woo-custom-tab3',
                'type'     => 'text',
                'title'    => __('Custom tab 3', 'redux-framework-demo'),
                //'subtitle' => __('subtitle', 'redux-framework-demo'),
              //  'desc'     => __('desc', 'redux-framework-demo'),
                'default'  => 'Odporúčaný',
                'ajax_save' => true
            ),
       
            array(
                'id'       => 'id',
                'type'     => 'divide',
                'desc'     => __('FUTURE', 'redux-framework-demo'),
            ),
            //http://docs.reduxframework.com/core/fields/color-gradient/
            array(
                'id'       => 'opt-shop-page-title',
                'type'     => 'text',
                'title'    => __('Shop page title', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc', 'redux-framework-demo'),
                // 'validate' => 'email',
                // 'msg'      => 'msg',
                // 'default'  => 'default',
                'ajax_save' => true
            ),

/**
  FUTURE - theme flatsome
 */

 array(
        'id'   => 'info-woo-product-future',
        'type' => 'info',
        'title' => __('FUTURE***', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

            array(
                'id'       => 'opt-woo-related-productsxx',
                'type'     => 'select',
                'multi'    => false,
                'title'    => __('Display related products ', 'redux-framework-demo'), 
                'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Zobrazit related products  // Single page ', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'options'  => array(
                    '1' => 'Slider',
                    '2' => 'Grid',
                    '3' => 'Remove'),
                'default'  => array('2','3')
            ),

            array(
                'id'       => 'opt-woo-related-products-per-row',
                'type'     => 'select',
                'multi'    => false,
                'title'    => __('Related products per row', 'redux-framework-demo'), 
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc, again good for additional info.', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'options'  => array(
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6'),
                    'default'  => '2'
            ),

            array(
                'id'       => 'opt-woo-loop-display-rating',
                'type'     => 'switch', 
                'title'    => 'Show rating',
                'subtitle' => 'Show rating',
                'desc' =>'Enable this if you would like to show the product rating below the product image/details (standard display type only).',
                'default'  => true,
                'ajax_save' => true,
            ),

// Cart Notification Animation  -- Atelier swift
            array(
                'id'       => 'opt-woo-cart-notification-animation',
                'type'     => 'select',
                'multi'    => false,
                'title'    => __('Cart Notification Animation', 'redux-framework-demo'), 
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Choose the animation style for the cart/wishlist menu item when adding a product.', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'options'  => array(
                    '1' => 'None',
                    '2' => 'Tada',
                    '3' => 'Bounce',
                    '4' => 'Flash',
                    '5' => 'Shake',
                    '6' => 'Pulse'
                    ),
                'default'  => 'pulse'
            ),

            array(
                'id'       => 'opt-woo-related-heading',
                'type'     => 'text',
                'title'    => __('Related Heading Text', 'redux-framework-demo'),
               // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Heading text for the related products on the product page.', 'redux-framework-demo'),

                'ajax_save' => true
            ),

// MAX NUMBER OF RELATED PRODUCTS

            array(
                'id'       => 'opt-woo-related-products-max-display',
                'type'     => 'text',
                'title'    => __('Related product max', 'redux-framework-demo'),
                'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('desc', 'redux-framework-demo'),
                'default'  => '12',
                'ajax_save' => true
            ),


// HTML BEFORE ADD TO CART BUTTON (GLOBAL)
            array( 
                'id'       => 'opt-woo-html-before-add-to-cart',
                'type'     => 'ace_editor',
                'theme'    => 'monokai',
                'title'    => __('HTML BEFORE ADD TO CART', 'redux-framework-demo'),
                //'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Enter HTML and shortcodes that will show before Add to cart selections.',
                 'redux-framework-demo'),
                'ajax_save' => true,
            ),

//HTML AFTER ADD TO CART BUTTON (GLOBAL)
            array( 
                'id'       => 'opt-woo-html-after-add-to-cart',
                'type'     => 'ace_editor',
                'theme'    => 'monokai',
                'title'    => __('HTML after ADD TO CART', 'redux-framework-demo'),
                //'subtitle' => __('subtitle', 'redux-framework-demo'),
                'desc'     => __('Enter HTML and shortcodes that will show before Add to cart selections.
            ', 'redux-framework-demo'),
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-woo-info-style',
                'type'     => 'select',
                'multi'    => false,
                'title'    => __('PRODUCT INFO STYLE ', 'redux-framework-demo'), 
                'subtitle' => __('- z temy flatsome - pethsop', 'redux-framework-demo'),
                'desc'     => __('Select how you want to display product info...', 'redux-framework-demo'),
                //Must provide key => value pairs for radio options
                'ajax_save' => true,
                'options'  => array(
                    '1' => 'Tabs',
                    '2' => 'Tabs center',
                    '3' => 'Tabs Pills',
                    '4' => 'Sections',
                     '5' => 'Accordion',
                     '6' => 'Vertical Tabs'),

                'default'  => '1'
            ),

            array(
                'id'       => 'opt-woo-loop-short-desc-grid',
                'type'     => 'switch', 
                'title'    => 'PRODUCT SHORT DESCRIPTION',
                'subtitle' => 'Show product short description in grid   ',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-woo-sale-display-mode',
                'type'     => 'switch', 
                'title'    => 'Sale display mode',
                'subtitle' => 'DISPLAY % INSTEAD OF SALE! IN SALE BUBBLE',
                'default'  => true,
                'ajax_save' => true,
            ),

//Toggle the display of various components on product archives and change product layout.

/* from plugin woocommerce archive customizer

 Display Display Product Count

 Per Page Dropdown

 Product Sorting

 Sale Flashes

 Thumbnails

 Prices


 Ratings

 Product categories

 Stock

 "New" badges
 Display the "New" badge for how many days?

*/
        )
    )
);
?>