<?php 

/**
*
*   Fixed Sidebar
*
**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Sidebar', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(

            array( 
                'id'       => 'opt-raw',
                'type'     => 'raw',
                // 'title'    => __('Raw output', 'redux-framework-demo'),
                // 'subtitle' => __('Subtitle text goes here.', 'redux-framework-demo'),
                // 'desc'     => __('This is the description field for additional info.', 'redux-framework-demo'),
                'content'  => '<strong style="background: red">Zatial len Options viac info v TODO</strong>',
            ),
             array(
                 'id'       => 'opt-sidebar-fixed',
                 'type'     => 'switch', 
                 'title'    => 'Enable Fixed Sidebar',
                 // 'subtitle' => 'subtitle',
                 'default'  => '0',
                 'ajax_save' => true,
             ),

            array(
                'id'       => 'opt-sidebar-fixed-id',
                'type'     => 'text',
                'title'    => __('Wrapper ID', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc', 'redux-framework-demo'),
                'default'  => 'some',
                'ajax_save' => true
            ),

            array(
                'id'        => 'opt-sidebar-fixed-background',
                'type'      => 'color_rgba',
                'title'     => 'Background',

                'default'   => array(
                    'color'     => 'true',
                    'alpha'     => 1
                ),
             
                // These options display a fully functional color palette. Omit this argument
                // for the minimal color picker, and change as desired.
                'options'       => array(
                    'show_input'                => true,
                    'show_initial'              => true,
                    'show_alpha'                => true,
                    'show_palette'              => true,
                    'show_palette_only'         => false,
                    'show_selection_palette'    => true,
                    'max_palette_size'          => 10,
                    'allow_empty'               => true,
                    'clickout_fires_change'     => true,
                    'choose_text'               => 'Choose',
                    'cancel_text'               => 'Cancel',
                    'show_buttons'              => true,
                    'use_extended_classes'      => true,
                    'palette'                   => null,  // show default
                    'input_text'                => 'Select Color'
                ),   
            ),

            array(
                'id'       => 'opt-sidebar-fixed-zindex',
                'type'     => 'text',
                'title'    => __('Z index', 'redux-framework-demo'),
                // 'subtitle' => __('subtitle', 'redux-framework-demo'),
                // 'desc'     => __('desc', 'redux-framework-demo'),
                'default'  => '4000',
                'ajax_save' => true
            ),

            array(
                'id'       => 'opt-sidebar-fixed-width',
                'type'     => 'dimensions',
                'units'    => array('em','px','%'),
                'height'     => false,
                'title'    => __('Width', 'redux-framework-demo'),
                'default'  => array(
                    'width'   => '360', 
                ),
            ),

            array(
                'id'       => 'opt-sidebar-fixed-height',
                'type'     => 'dimensions',
                'units'    => array('em','px','%'),
                'width'     => false,
                'title'    => __('Height', 'redux-framework-demo'),
                'default'  => array(
                    'height'   => '100', 
                ),
            ),

            array(
                'id'       => 'opt-sidebar-fixed-transition',
                'type'     => 'spinner',
                
                // 'hint'     => array(
                // 'content' => 'This is a <b>hint</b> tool-tip for the text field.<br/><br/>Add any HTML based text you like here.',
                //     ),

                'title'    => 'Transition',
                'subtitle' => 'in seconds',
                // 'desc'     => 'desc',
                'ajax_save' => true,
                'default'  => '0.3',
                'min'      => '0.1',
                'step'     => '0.1',
                'max'      => '3',
            ),

            array(
                'id'             => 'opt-sidebar-fixed-transform',
                'type'           => 'dimensions',
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => __( 'Transform x / y', 'redux-framework-demo' ),
                // 'subtitle'       => __( 'Allow your users to choose width, height, and/or unit.', 'redux-framework-demo' ),
                // 'desc'           => __( '', 'redux-framework-demo' ),
                'height'         => true,
                'default'        => array(
                    'width'  => 360,
                    'height' => 0,
                )
            ),
        )           
    )
);
 ?>