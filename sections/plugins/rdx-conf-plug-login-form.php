<?php 

/**
    Subsection Plugin
    Login Form
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'title'      => __( 'Login Form', 'redux-framework-demo' ),
        'desc'       => '',
        'section_id' => 'section_login',
        'subsection' => true,
        'icon'       => 'el-icon-unlock',
        'fields'     => array(

            array(
                'id'   => 'info-login-form',
                // 'required' => array( 'opt-id', '=', true ),
                'type' => 'info',
                'title'    => __('Redirect Pages', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
                 'id'       => 'opt-login-redirect-page',
                 // 'required' => array( 'opt-id', '=', true ),
                 'type'     => 'select',
                 // 'multi'    => true,
                 'title'    => __('Login Redirect', 'redux-framework-demo'),
                 'ajax_save' => true,
                 'data' => 'page'
            ),

            array(
                 'id'       => 'opt-logout-redirect-page',
                 // 'required' => array( 'opt-id', '=', true ),
                 'type'     => 'select',
                 // 'multi'    => true,
                 'title'    => __('Logout Redirect', 'redux-framework-demo'),
                 'ajax_save' => true,
                 'data' => 'page'
            ),

            array(
                 'id'       => 'opt-login-failed-redirect-page',
                 // 'required' => array( 'opt-id', '=', true ),
                 'type'     => 'select',
                 // 'multi'    => true,
                 'title'    => __('Login Failed Redirect', 'redux-framework-demo'),
                 'ajax_save' => true,
                 'data' => 'page'
            ),

            array(
                 'id'       => 'opt-login-empty-redirect-page',
                 // 'required' => array( 'opt-id', '=', true ),
                 'type'     => 'select',
                 // 'multi'    => true,
                 'title'    => __('Login Empty Redirect', 'redux-framework-demo'),
                 'ajax_save' => true,
                 'data' => 'page'
            ),

            array(
                'id'   => 'info-login-form-label',
                'type' => 'info',
                'title'    => __('Login Form Label', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
                 'id'            => 'opt-login-form-username-label',
                 // 'required'      => array( 'opt-id', '=', true ),
                 'type'          => 'text',
                 'title'         => __('Meno', 'redux-framework-demo'),
                 //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                 'default'       => 'Meno',
                 'ajax_save'     => true
            ),

            array(
                'id'            => 'opt-login-form-password-label',
                 // 'required'      => array( 'opt-id', '=', true ),
                 'type'          => 'text',
                 'title'         => __('Heslo', 'redux-framework-demo'),
                 //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                 'default'       => 'Heslo',
                 'ajax_save'     => true
            ),

            array(
                'id'            => 'opt-login-form-logout-link',
                 // 'required'      => array( 'opt-id', '=', true ),
                 'type'          => 'text',
                 'title'         => __('Odhlásiť', 'redux-framework-demo'),
                 //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                 'default'       => 'Odhlásiť',
                 'ajax_save'     => true
            ),

             array(
                'id'   => 'info-login-form-rememeber',
                'type' => 'info',
                'title'    => __('Remember me', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
                'id'       => 'opt-login-form-remember',
                'type'     => 'switch', 
                'title'    => __('Remember', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'            => 'opt-login-form-remember-label',
                 'required'      => array( 'opt-login-form-remember', '=', true ),
                 'type'          => 'text',
                 'title'         => __('Zapamätaj si ma', 'redux-framework-demo'),
                 //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                 'default'       => 'Zapamätaj si ma',
                 'ajax_save'     => true
            ),

            array(
                'id'       => 'opt-login-form-remember-value',
                'required' => array( 'opt-admin-login-remember', '=', true ),
                'type'     => 'switch', 
                'title'    => __('Remember Default', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'   => 'info-login-form-text',
                // 'required' => array( 'opt-id', '=', true ),
                'type' => 'info',
                'title'    => __('Text Before / After', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
                'id'               => 'opt-login-form-text-before',
                // 'required' => array( 'opt-id', '=', true ),
                'type'             => 'editor',
                'title'             => __('Text Before', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'          => '',
                'args'   => array(
                    'teeny'             => true, // minimal settings
                    'textarea_rows'     => 10,
                    'wpautop'           => true, //    Flags to set wpautop for adding paragraphs.
                )
            ),

            array(
                'id'               => 'opt-login-form-text-after',
                // 'required' => array( 'opt-id', '=', true ),
                'type'             => 'editor',
                'title'             => __('Text After', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'default'          => '',
                'args'   => array(
                    'teeny'             => true, // minimal settings
                    'textarea_rows'     => 10,
                    'wpautop'           => true, //    Flags to set wpautop for adding paragraphs.
                )
            ),

            array(
                'id'   => 'info-login-form-errors',
                // 'required' => array( 'opt-id', '=', true ),
                'type' => 'info',
                'title'    => __('Login Form Errors', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
                'id'               => 'opt-login-form-error-empty',
                // 'required' => array( 'opt-id', '=', true ),
                'type'             => 'editor',
                'title'             => __('Username / Password Empty', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                'desc'     => __('Error Username and/or Password is empty', 'redux-framework-demo'),
                'default'          => 'Užívateľské meno a/alebo heslo je prázdne',
                'args'   => array(
                    'teeny'             => true, // minimal settings
                    'textarea_rows'     => 10,
                    'wpautop'           => true, //    Flags to set wpautop for adding paragraphs.
                )
            ),

            array(
                'id'               => 'opt-login-form-error-failed',
                // 'required' => array( 'opt-id', '=', true ),
                'type'             => 'editor',
                'title'             => __('Username / Password Failed', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                'desc'     => __('Error Invalid username and/or password', 'redux-framework-demo'),
                'default'          => 'Neplatné užívateľské meno a/alebo heslo',
                'args'   => array(
                    'teeny'             => true, // minimal settings
                    'textarea_rows'     => 10,
                    'wpautop'           => true, //    Flags to set wpautop for adding paragraphs.
                )
            ),
        )
    )
);
?>