<?php 

/**

    Woocommerece Cart

**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Woo Cart', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'fields'     => array(

/**

    Cart

**/
     array(
        'id'   => 'info-woo-cart-cart',
        'type' => 'info',
        'title' => __('Cart', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'            => 'opt-woo-cart-typo',
        'type'          => 'typography', 
         'title'    => __('Title', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'google'        => false,
        'color'         => false,
        'font-backup'   => false,
        'font-family'   => false,
        'font-style'    => false,
        'font-weight'   => false,
        'subset'        => false,
        'text-align'    => false,
        'preview'       => false,
        'units'       =>'px',
        'ajax_save' => true,
        // 'subtitle'    => __('false', 'redux-framework-demo'),
        'default'     => array(
            // 'color'          => '#333', 
            // 'font-style'  => '700', 
            // 'font-family' => 'Abel', 
            // 'google'      => true,
            'font-size'   => '13px', 
            'line-height' => '13px'
        ),
    ),

    array(
        'id'       => 'opt-woo-cart-background',
        'type'     => 'link_color',
        'title'    => __('Background', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Description', 'redux-framework-demo'),
        'hover'     => true,
        'active'    => false,
        'visited'   => false,
        'ajax_save' => true,
        'default'   => array(
            'regular'  => '#fff', // blue
            'hover'    => '#fff', // red
        //    'active'   => '#8224e3',  // purple
        //    'visited'  => '#8224e3',  // purple
        )
    ),
    
    array(
        'id'       => 'opt-woo-cart-color',
        'type'     => 'link_color',
        'title'    => __('Title', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Description', 'redux-framework-demo'),
        'hover'     => true,
        'active'    => false,
        'visited'   => false,
        'default'   => array(
            'regular'  => '#1e73be', // blue
            'hover'    => '#dd3333', // red
        //    'active'   => '#8224e3',  // purple
        //    'visited'  => '#8224e3',  // purple
        )
    ),

//https://docs.reduxframework.com/core/fields/link-color/

    array(
        'id'             => 'opt-woo-cart-padding',
        'type'           => 'spacing',
        // 'output'         => array('.site-header'),
        'mode'           => 'padding',
        'units'          => array('em', 'px'),
        'units_extended' => 'false',
        'title'          => __('Padding', 'redux-framework-demo'),
        // 'subtitle'       => __('Allow your users to choose the spacing or margin they want.', 'redux-framework-demo'),
        // 'desc'           => __('You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'redux-framework-demo'),
        'default'            => array(
            'padding-top'     => '10px', 
            'padding-right'   => '10px', 
            'padding-bottom'  => '10px', 
            'padding-left'    => '10px',
            'units'     => 'px', 
        )
    ),

/**

    Fixed Position
    
**/

    array(
        'id'   => 'info-woo-cart-fixed-position',
        'type' => 'info',
        'title'    => __('Fixed Position', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'       => 'opt-woo-cart-fixed-horizontal',
        'type'     => 'select',
        'multi'    => false,
        'title'    => __('Fixed Position', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        //Must provide key => value pairs for radio options
        'ajax_save' => true,
        'options'  => array(
            'right' => 'Right',
            'left' => 'Left',
            'center' => 'Center'),
        'default'  => array('right')
    ),

    array(
        'id'            => 'opt-woo-cart-fixed-margin',
        'type'          => 'spacing',
        'title'         => __('Margin', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'mode'          => 'margin',
        'ajax_save'     =>   true,
        'units'         => array('em', 'px'),
        'units_extended' => 'false',
        'default'       => array(
            'margin-top'    => '10px', 
            'margin-right'   => '10px', 
            'margin-bottom'  => '10px', 
            'margin-left'    => '10px',
            'units'     => 'px', 
        )
    ),

/**

    Text after item

**/
    array(
        'id'       => 'opt-woo-cart-after-item-text',
        'type'     => 'text',
        'title'    => __('Text Before Quantity', 'redux-framework-demo'),
         // 'subtitle' => __('Item after text', 'redux-framework-demo'),
        'desc'     => __('Item after text', 'redux-framework-demo'),
        'default'  => 'položka',
        'ajax_save' => true
    ),

    array(
        'id'       => 'opt-woo-cart-after-items-text',
        'type'     => 'text',
        'title'    => __('Items', 'redux-framework-demo'),
         'subtitle' => __('Items after text', 'redux-framework-demo'),
        // 'desc'     => __('desc', 'redux-framework-demo'),
        'default'  => 'položky',
        'ajax_save' => true
    ),

/**

    Cart Content - List

**/

    array(
        'id'   => 'info-woo-cart-content',
        'type' => 'info',
        'title' => __('Content', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'       => 'opt-woo-cart-content-background',
        'type'     => 'color',
        'title'    => __('Background', 'redux-framework-demo'), 
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        'ajax_save' => true,
        'default'  => '#FFFFFF',
        'validate' => 'color',
    ),

    array(
        'id'          => 'opt-woo-cart-content-typo',
        'type'        => 'typography', 
        'title'       => __('Content', 'redux-framework-demo'),
        'color'        => false,
        'google'      => false, 
        'font-backup' => false,
        'font-family' => false,
        'font-style' => false,
        'font-weight' => false,
        'subset' => true,
        'preview' => true,
        'text-align' => true,
        'units'       =>'px',
        // 'subtitle'    => __('Typography option with each property can be called individually.', 'redux-framework-demo'),
        'default'     => array(
            // 'color'       => '#333', 
            // 'font-style'  => '700', 
            // 'font-family' => 'Abel', 
            // 'google'      => true,
            'font-size'   => '13px', 
            'line-height' => '13px'
        ),
    ),

    array(
        'id'             => 'opt-woo-cart-content-padding',
        'type'           => 'spacing',
        // 'output'         => array('.site-header'),
        'mode'           => 'padding',
        'units'          => array('em', 'px'),
        'units_extended' => 'false',
        'title'          => __('Padding', 'redux-framework-demo'),
        // 'subtitle'       => __('Allow your users to choose the spacing or margin they want.', 'redux-framework-demo'),
        // 'desc'           => __('You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'redux-framework-demo'),
        'default'            => array(
            'padding-top'     => '10px', 
            'padding-right'   => '10px', 
            'padding-bottom'  => '10px', 
            'padding-left'    => '10px',
            'units'     => 'px', 
        )
    ),

     array(
        'id'   => 'info-woo-cart-item-title',
        'type' => 'info',
        'title'    => __('Item Title', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'            => 'opt-woo-cart-item-title-typo',
        'type'          => 'typography', 
         'title'    => __('Typography', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'google'        => false,
        'color'         => false,
        'font-backup'   => false,
        'font-family'   => false,
        'font-style'    => false,
        'font-weight'   => false,
        'subset'        => false,
        'text-align'    => false,
        'preview'       => false,
        'units'       =>'px',
        'ajax_save' => true,
        // 'subtitle'    => __('false', 'redux-framework-demo'),
        'default'     => array(
           // 'color'          => '#333', 
            // 'font-style'  => '700', 
            // 'font-family' => 'Abel', 
            // 'google'      => true,
            'font-size'   => '13px', 
            'line-height' => '13px'
        ),
    ),

    array(
        'id'       => 'opt-woo-cart-item-title-color',
        'type'     => 'link_color',
        'title'    => __('Color', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Description', 'redux-framework-demo'),
        'hover'     => true,
        'active'    => false,
        'visited'   => false,
        'default'   => array(
            'regular'  => '#373737', // blue
            'hover'    => '#191919', // red
        //    'active'   => '#8224e3',  // purple
        //    'visited'  => '#8224e3',  // purple
        )
    ),

/**

    Remove

**/

     array(
        'id'   => 'info-opt-remove',
        'type' => 'info',
        'title' => __('Remove', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'          => 'opt-woo-cart-remove-typo',
        'type'        => 'typography', 
        'title'       => __('Typography', 'redux-framework-demo'),
        'google'      => false,
        'color' => false,
        'text-align' => false,
        'font-backup' => false,
        'font-family' => false,
        'font-style' => false,
        'font-weight' => false,
        'subset' => false,
        'preview' => false,
        // 'text-align' => true,
        'units'       =>'px',
        // 'subtitle'    => __('Typography option with each property can be called individually.', 'redux-framework-demo'),
        'default'     => array(
            'color'       => '#333', 
            // 'font-style'  => '700', 
            // 'font-family' => 'Abel', 
            // 'google'      => true,
            'font-size'   => '13px', 
            'line-height' => '13px'
        ),
    ),
    
    array(
      'id'       => 'opt-woo-cart-remove-color',
      'type'     => 'link_color',
      'title'    => __('Color', 'redux-framework-demo'),
      //'subtitle' => __('Subtitle', 'redux-framework-demo'),
      //'desc'     => __('Description', 'redux-framework-demo'),
      'hover' => true,
      'active' => false,
      'visited' => false,
      'default'  => array(
          'regular'  => '#373737', // blue
          'hover'    => '#191919', // red
          // 'active'   => '#8224e3',  // purple
          // 'visited'  => '#8224e3',  // purple
      )
    ),


/**

    Image

**/

     array(
        'id'   => 'info-woo-cart-image',
        'type' => 'info',
        'title' => __('Image', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'       => 'opt-woo-cart-image-size',
        'type'     => 'spinner', 
        'title'    => 'Image Size',
        // 'subtitle' => 'subtitle',
        // 'desc'     => 'desc',
        'ajax_save' => true,
        'default'  => '40',
        'min'      => '10',
        'step'     => '10',
        'max'      => '400',
    ),

// Row Color

    array(
        'id'       => 'opt-woo-cart-row-border-color',
        'type'     => 'color',
        'title'    => __('Border Color', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'ajax_save' => true,
        'default'  => '##eaeaea',
        'validate' => 'color',
    ),



/**

    View Cart Button

**/

     array(
        'id'   => 'info-woo-cart-view',
        'type' => 'info',
        'title' => __('Buttons', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'            => 'opt-woo-cart-btn-padding',
        'type'          => 'spacing',
        'title'         => __('Padding', 'redux-framework-demo'),
        //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
        //'desc'        => __('Desc', 'redux-framework-demo'),
        'mode'          => 'padding', // padding
        'ajax_save'        =>     true,
        'units'         => array('em', 'px'),
        'units_extended' => 'false',
        'default'       => array(
            'padding-top'     => '10px', 
            'padding-right'   => '10px', 
            'padding-bottom'  => '10px', 
            'padding-left'    => '10px',
            'units'     => 'px', 
        )
    ),

    array(
        'id'            => 'opt-woo-cart-btn-typo',
        'type'          => 'typography', 
         'title'    => __('Typo', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'google'        => false,
        'color'         => false,
        'font-backup'   => false,
        'font-family'   => false,
        'font-style'    => false,
        'font-weight'   => false,
        'subset'        => false,
        'text-align'    => false,
        'preview'       => false,
        'units'       =>'px',
        'ajax_save' => true,
        // 'subtitle'    => __('false', 'redux-framework-demo'),
        'default'     => array(
            'color'          => '#333', 
            // 'font-style'  => '700', 
            // 'font-family' => 'Abel', 
            // 'google'      => true,
             'font-size'   => '13px', 
             'line-height' => '13px'
        ),
    ),
    

    array(
        'id'       => 'opt-woo-cart-btn-width',
        'type'     => 'select',
        'multi'    => false,
        'title'    => __('Width', 'redux-framework-demo'), 
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        // 'desc'     => __('desc, again good for additional info.', 'redux-framework-demo'),
        //Must provide key => value pairs for radio options
        'ajax_save' => true,
        'options'  => array(
            '100%' => 'Full Widht',
            'auto' => 'Auto',
            'custom' => 'Custom'),
        'default'  => 'auto'
    ),

/**
Btn View
**/
     array(
        'id'   => 'info-woo-cart-btn-view',
        'type' => 'info',
        'title'    => __('View Cart Button', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),

    array(
        'id'       => 'opt-woo-cart-btn-view',
        'type'     => 'switch', 
        'title'    => 'View Cart',
        // 'subtitle' => 'subtitle',
        'default'  => true,
        'ajax_save' => true,
    ),

    array(
        'id'       => 'opt-woo-cart-btn-view-background',
        'type'     => 'link_color',
        'title'    => __('Background', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Description', 'redux-framework-demo'),
        'hover'     => true,
        'active'    => false,
        'visited'   => false,
        'default'   => array(
            'regular'  => '#c02a41',
            'hover'    => '#962133', // red
        //    'active'   => '#8224e3',  // purple
        //    'visited'  => '#8224e3',  // purple
        )
    ),

    array(
        'id'       => 'opt-woo-cart-btn-view-color',
        'type'     => 'link_color',
        'title'    => __('Color', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Description', 'redux-framework-demo'),
        'hover'     => true,
        'active'    => false,
        'visited'   => false,
        'default'   => array(
            'regular'  => '#fff',
            'hover'    => '#fff',
        //    'active'   => '#8224e3',  // purple
        //    'visited'  => '#8224e3',  // purple
        )
    ),

    array(
        'id'       => 'opt-woo-cart-btn-view-text',
        'type'     => 'text',
        'title'    => __('Text', 'redux-framework-demo'),
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        'default'  => 'Košík',
        'ajax_save' => true
    ),


    array(
        'id'            => 'opt-woo-cart-btn-view-margin',
        'type'          => 'spacing',
        'title'         => __('Margin', 'redux-framework-demo'),
        //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
        //'desc'        => __('Desc', 'redux-framework-demo'),
        'mode'          => 'padding', // padding
        'ajax_save'     =>  true,
        'units'         => array('em', 'px'),
        'units_extended' => 'false',
        'default'       => array(
            // 'padding-top'     => '10px', 
            // 'padding-right'   => '10px', 
            'padding-bottom'  => '20px', 
            // 'padding-left'    => '10px',
            'units'     => 'px', 
        )
    ),

    array(
        'id'       => 'opt-woo-cart-btn-view-width',
        'type'     => 'select',
        'multi'    => false,
        'title'    => __('Width', 'redux-framework-demo'), 
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        // 'desc'     => __('desc, again good for additional info.', 'redux-framework-demo'),
        //Must provide key => value pairs for radio options
        'ajax_save' => true,
        'options'  => array(
            'full' => 'Full Widht',
            'auto' => 'Auto',
            'custom' => 'Custom'),
        'default'  => 'auto'
    ),

/**

    Checkout Button

**/
    array(
        'id'   => 'id',
        'type' => 'info',
        // 'desc' => __('desc', 'redux-framework-demo'),
        'title' => __('Checkout Button', 'redux-framework-demo'),
        'notice' => true,
        // 'icon'  => 'el-icon-info-sign',
        'style' => 'success',
    ),

    array(
        'id'       => 'opt-woo-cart-btn-checkout',
        'type'     => 'switch', 
        'title'    => 'Checkout',
        // 'subtitle' => 'subtitle',
        'default'  => true,
        'ajax_save' => true,
    ),

    array(
        'id'       => 'opt-woo-cart-btn-checkout-background',
        'type'     => 'link_color',
        'title'    => __('Background', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Description', 'redux-framework-demo'),
        'hover'     => true,
        'active'    => false,
        'visited'   => false,
        'default'   => array(
            'regular'  => '#3d4565', 
            'hover'    => '#2a2f45', 
        //    'active'   => '#8224e3',  // purple
        //    'visited'  => '#8224e3',  // purple
        )
    ),

    array(
        'id'       => 'opt-woo-cart-btn-checkout-color',
        'type'     => 'link_color',
        'title'    => __('Color', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Description', 'redux-framework-demo'),
        'hover'     => true,
        'active'    => false,
        'visited'   => false,
        'default'   => array(
            'regular'  => '#fff', // blue
            'hover'    => '#fff', // red
        //    'active'   => '#8224e3',  // purple
        //    'visited'  => '#8224e3',  // purple
        )
    ),


    array(
        'id'       => 'opt-woo-cart-btn-checkout-text',
        'type'     => 'text',
        'title'    => __('Text', 'redux-framework-demo'),
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        'default'  => 'Pokladňa',
        'ajax_save' => true
    ),

    array(
        'id'       => 'opt-woo-cart-btn-checkout-width',
        'type'     => 'select',
        'multi'    => false,
        'title'    => __('Width', 'redux-framework-demo'), 
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        // 'desc'     => __('desc, again good for additional info.', 'redux-framework-demo'),
        //Must provide key => value pairs for radio options
        'ajax_save' => true,
        'options'  => array(
            'full' => 'Full Widht',
            'auto' => 'Auto',
            'custom' => 'Custom'),
        'default'  => 'auto'
    ),

/**
  END VIEW CART & CHECKOUT BUTTONS
**/

/**

    Subtotal

**/
     array(
        'id'   => 'info-woo-cart-subtotal',
        'type' => 'info',
        'title' => __('Subtotal', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        // 'icon'  => 'el-icon-info-sign',
        'style' => 'success',
    ),

     array(
         'id'       => 'opt-woo-cart-subtotal-background',
         'type'     => 'color',
         'title'    => __('Background', 'redux-framework-demo'),
         //'subtitle' => __('Subtitle', 'redux-framework-demo'),
         //'desc'     => __('Desc', 'redux-framework-demo'),
         'ajax_save' => true,
         'default'  => 'transparent',
         'validate' => 'color',
     ),
     //http://docs.reduxframework.com/core/fields/color/

    array(
        'id'       => 'opt-woo-cart-subtotal-text',
        'type'     => 'text',
        'title'    => __('Subtotal', 'redux-framework-demo'),
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        'default'  => 'Spolu',
        'ajax_save' => true
    ),

    array(
        'id'            => 'opt-woo-cart-subtotal-typo',
        'type'          => 'typography', 
         'title'    => __('Title', 'redux-framework-demo'),
        //'subtitle' => __('Subtitle', 'redux-framework-demo'),
        //'desc'     => __('Desc', 'redux-framework-demo'),
        'google'        => false,
        'color'         => false,
        'font-backup'   => false,
        'font-family'   => false,
        'font-style'    => false,
        'font-weight'   => false,
        'subset'        => false,
        'text-align'    => true,
        'preview'       => false,
        'units'       =>'px',
        'ajax_save' => true,
        // 'subtitle'    => __('false', 'redux-framework-demo'),
        'default'     => array(
           'text-align' => 'center',
            'color'          => '#333', 
            // 'font-style'  => '700', 
            // 'font-family' => 'Abel', 
            // 'google'      => true,
            'font-size'   => '13px', 
            'line-height' => '13px'
        ),
    ),
    
    array(
        'id'       => 'opt-woo-cart-subtotal-color',
        'type'     => 'color',
        'title'    => __('Color', 'redux-framework-demo'), 
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        'ajax_save' => true,
        'default'  => '#000',
        'validate' => 'color',
    ),

/**

    Empty Cart

**/
     array(
        'id'   => 'info-woo-cart-empty-list',
        'type' => 'info',
        'title' => __('Empty', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        // 'icon'  => 'el-icon-info-sign',
        'style' => 'success',
    ),

    array(
        'id'       => 'opt-woo-cart-empty-list',
        'type'     => 'text',
        'title'    => __('Empty Cart', 'redux-framework-demo'),
        // 'subtitle' => __('subtitle', 'redux-framework-demo'),
        // 'desc'     => __('desc', 'redux-framework-demo'),
        'default'  => 'Košík je prázdny',
        'ajax_save' => true
    ),

/**

    Popup

**/

    array(
        'id'   => 'info-woo-cart-popup',
        'type' => 'info',
        'title' => __('Popup', 'redux-framework-demo'),
        // 'desc' => __('desc', 'redux-framework-demo'),
        'notice' => true,
        //'icon'  => 'el-icon-info-sign',
        'style' => 'success', // warning|critical|info
    ),
    array(
        'id'       => 'opt-woo-cart-popup',
        'type'     => 'switch', 
        'title'    => 'Popup',
        // 'subtitle' => 'subtitle',
        'default'  => true,
        'ajax_save' => true,
    ),

    )
)
);
?>