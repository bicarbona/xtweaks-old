<?php 

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks

 array(
        'icon'       => 'el-icon-broom',
        'title'      => __( 'CF7', 'redux-framework-demo' ),
        'subsection' => true,
        'fields'     => array(

/**

CF 7 to CPT - Contact form to Custom Post Type

**/
        array(
            'id'       => 'opt-select-post-type-cf7',
            'type'     => 'select',
            'data'     => 'post_type',
            'title'    => __( 'Contact post type', 'redux-framework-demo' ),
            'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
            'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
        ),
/**
Post status
**/
        array(
            'id'       => 'opt-select-post-status-cf7',
            'type'     => 'select',
            'title'    => __( 'Contact post status', 'redux-framework-demo' ),
            'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
            'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
            //Must provide key => value pairs for select options
            'options'  => array(
                'publish' => 'publish', // - A published post or page
                'pending' => 'pending', // - post is pending review
                'draft' => 'draft', // - a post in draft status
                'auto-draft' => 'auto-draft', //  - a newly created post, with no content
                'future' => 'future', // - a post to publish in the future
                'private' =>'private', // - not visible to users who are not logged in
                'inherit' => 'inherit', //- a revision. see get_children.
                'trash' => 'trash', // - post is in trashbin. added with Version 2.9.*/
            ),
            'default'  => 'pending'
        )

    ),
    ) 
);


?>