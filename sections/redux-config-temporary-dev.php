<?php

/**
 Admin Section
 */

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'icon'       => 'el-icon-website',
    'title'      => __( 'DEV', 'redux-framework-demo' ),
    // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
    // 'subsection' => true,
    'id'      => 'section_admin',
    'fields' => array(
          array(
            'id'       => 'opt-experimental-ajax-delete',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch',
            'title'    => __('Experimental Ajax delete', 'redux-framework-demo'),
              'default'  => false,
              'ajax_save' => true,
          ),
        )
    )
);

/**
 Admin Subsections
*/
// include_once '/redux-config-.php';

?>
