<?php 

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
array(
    'icon'       => 'el-icon-adjust',
    'title'      => __( 'Ui Semantic', 'redux-framework-demo' ),
    // 'subsection' => true,
    'fields'     => array(

array(
  'id'=>'switch-custom',
    'type' => 'switch', 
    'title' => __('Switch - Custom Titles', 'redux-framework'),
    'subtitle'=> __('Look, it\'s on! Also hidden child elements!', 'redux-framework'),
    "default"       => 0,
    'on' => 'Enabled',
    'off' => 'Disabled',
),  

array(
    'id'=>'switch-fold',
    'type' => 'switch', 
    'fold' => array('switch-custom'),                       
    'title' => __('Switch - With Hidden Items (NESTED!)', 'redux-framework'),
    'subtitle'=> __('Also called a "fold" parent.', 'redux-framework'),
    'desc' => __('Items set with a fold to this ID will hide unless this is set to the appropriate value.', 'redux-framework'),
    'default' => 0,
    ),  
array(
    'id'=>'patterns',
    'type' => 'image_select', 
    'tiles' => true,
    'fold' => array('switch-fold'=>0),
    'title' => __('Images Option (with pattern=>true)', 'redux-framework'),
    'subtitle'=> __('Select a background pattern.', 'redux-framework'),
    'default'       => 0,
    'options' => array(
                    // '1' => array('alt' => '1 Column', 'img' => REDUX_URL.'assets/img/1col.png'),
                    // '2' => array('alt' => '2 Column Left', 'img' => REDUX_URL.'assets/img/2cl.png'),
                    // '3' => array('alt' => '2 Column Right', 'img' => REDUX_URL.'assets/img/2cr.png'),
                    // '4' => array('alt' => '3 Column Middle', 'img' => REDUX_URL.'assets/img/3cm.png'),
                    // '5' => array('alt' => '3 Column Left', 'img' => REDUX_URL.'assets/img/3cl.png'),
                    // '6' => array('alt' => '3 Column Right', 'img' => REDUX_URL.'assets/img/3cr.png')
                        ),
    ),  


// CUSTOM SIDEBARS
            array(
                'id' => 'custom_sidebars',
                'type' => 'repeater',
                'group_values' => true,
                'bind_title' => 'sidebar_title',
                'title' => __( 'Custom Sidebars', 'lsvrtheme' ),
                'subtitle' => __( 'Each sidebar must have an <strong>unique simple ID</strong> which contains just letters and digits (for example "sidebar1").', 'lsvrtheme' ),
                'limit' => 50,
                'fields' => array(
                    array(
                        'id' => 'sidebar_id',
                        'type' => 'text',
                        'title' => __( 'Sidebar ID', 'lsvrtheme' ),
                    ),
                    array(
                        'id' => 'sidebar_title',
                        'type' => 'text',
                        'title' => __( 'Sidebar Title', 'lsvrtheme' ),
                    ),

                    array(
                        'id'            => 'opt-colorek',
                        'required'      => array( 'opt-id', '=', true ),
                        'type'          => 'color',
                        'title'         => __('Title', 'redux-framework-demo'),
                        //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
                        //'desc'        => __('Desc', 'redux-framework-demo'),
                        'ajax_save'     => true,
                        'default'       => '#FFFFFF',
                        'validate'      => 'color',
                    ),
                    //http://docs.reduxframework.com/core/fields/color/
                )
            ),


        array( 
            'id'       =>'opt-semantic-reset',
            'type'     => 'switch', 
            'title'    => __('Reset', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-site',
            'type'     => 'switch',
            'title'    => __('Site', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),
/**
 Elements 
**/
         array(
            'id'   => 'info-semantic-elements',
            'type' => 'info',
            'title'    => __('Elements', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),
        //http://docs.reduxframework.com/core/fields/desc/

        array( 
            'id'       =>'opt-semantic-button',
            'type'     => 'switch',
            'title'    => __('Button', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-container',
            'type'     => 'switch', 
            'title'    => __('Container', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-divider',
            'type'     => 'switch',
            'title'    => __('Divider', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-flag', 
            'type'     => 'switch',
            'title'    => __('Flag', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-header',  
            'type'     => 'switch',
            'title'    => __('Header', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-icon', 
            'type'     => 'switch',
            'title'    => __('Icon', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-image',
            'type'     => 'switch', 
            'title'    => __('Image', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-input',
            'type'     => 'switch', 
            'title'    => __('Input', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-label',
            'type'     => 'switch', 
            'title'    => __('Label', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-list', 
            'type'     => 'switch',
            'title'    => __('List', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-loader',
            'type'     => 'switch',
            'title'    => __('Loader', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-rail',
            'type'     => 'switch',
            'title'    => __('Rail', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),
        array( 
            'id'       =>'opt-semantic-reveal',
            'type'     => 'switch',
            'title'    => __('Reveal', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-segment',  
            'type'     => 'switch',
            'title'    => __('Segment', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-step',
            'type'     => 'switch',
            'title'    => __('Step', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

/** 
 Collections
 */

        array(
            'id'   => 'info-semantic-collections',
            'type' => 'info',
            'title'    => __('Collections', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),

        array( 
            'id'       =>'opt-semantic-breadcrumb',  
            'type'     => 'switch',
            'title'    => __('Breadcrumb', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-form',
            'type'     => 'switch',
            'title'    => __('Form', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-grid', 
            'type'     => 'switch',
            'title'    => __('Grid', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-menu', 
            'type'     => 'switch',
            'title'    => __('Menu', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-message',  
            'type'     => 'switch',
            'title'    => __('Message', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-table',
            'type'     => 'switch', 
            'title'    => __('Table', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),


/** 
 Views 
 */
         array(
            'id'   => 'info-semantic-views',
            'type' => 'info',
            'title'    => __('Views', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),

        array( 
            'id'       =>'opt-semantic-ad',
            'type'     => 'switch',
            'title'    => __('Ad', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-card',
            'type'     => 'switch',
            'title'    => __('Card', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-comment', 
            'type'     => 'switch',
            'title'    => __('Comment', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-feed', 
            'type'     => 'switch',
            'title'    => __('Feed', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-item',
            'type'     => 'switch',
            'title'    => __('Item', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-statistic',
            'type'     => 'switch', 
            'title'    => __('Statistic', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

/** 
 Modules 
 */
         array(
            'id'   => 'info-semantic-modules',
            'type' => 'info',
            'title'    => __('Modules', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),

        array( 
            'id'       =>'opt-semantic-accordion',
            'type'     => 'switch', 
            'title'    => __('Accordion', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-checkbox',
            'type'     => 'switch',
            'title'    => __('Checkbox', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-dimmer',  
            'type'     => 'switch',
            'title'    => __('Dimmer', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-dropdown',
            'type'     => 'switch',
            'title'    => __('Dropdown', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-embed',
            'type'     => 'switch', 
            'title'    => __('Embed', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-modal',
            'type'     => 'switch', 
            'title'    => __('Modal', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-nag',  
            'type'     => 'switch',
            'title'    => __('Nag', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-popup',
            'type'     => 'switch', 
            'title'    => __('Popup', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-progress', 
            'type'     => 'switch',
            'title'    => __('Progress', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-rating',  
            'type'     => 'switch',
            'title'    => __('Rating', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-search',  
            'type'     => 'switch',
            'title'    => __('Search', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-shape',
            'type'     => 'switch', 
            'title'    => __('Shape', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-sidebar', 
            'type'     => 'switch',
            'title'    => __('Sidebar', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-sticky',  
            'type'     => 'switch',
            'title'    => __('Sticky', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-tab',  
            'type'     => 'switch',
            'title'    => __('Tab', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),

        array( 
            'id'       =>'opt-semantic-transition',   
            'type'     => 'switch',
            'title'    => __('Transition', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => '0', // 1 = on | 0 = off
            'ajax_save' => true,
        ),


        )
    )
);

function helo(){


}
?>