
## Architect builder

- rich snippets
- podpora acf - repeatable ?
	- typ pola - napr (select (moznost zobrazit field name)
	- typ pola - gallery - tak da iny vystup

~~~php
# ACF
	
	$field = get_field_object( 'acf-name' );
	$value = get_field( 'acf-name' );
	
	if( $field['type'] == 'gallery' ){
 
            // featured image
            if( $value ):

            // $image['sizes']['thumbnail']
            print'<div class="featured">'.$special.'<img src="'.$value[0]['url'].'" alt="" width="" height="" border="0" /></div';
            
            # Gallery
            if($show_gallery){
                foreach( $value as $image ):
                    print'<img src="'.$image['url'].'" alt="" width="" height="" border="0" />';
                  // echo  $image['url'];
                endforeach;
            }

            # End gallery    
            endif; 

    }
~~~	
- podpora taxonomie
- Tabs - content -> Architect
- Override taxonomy
	- shortcode - send parameter -> filtrovanie taxonomie atd
	(vytvoris jeden blueprint ale mozes ho pouzit aj na inu taxonmiu cpt...)


## Redux - > 
link to post - customize
jednoducha uprava link to post modulov v customizery

*  @TODO flush rewrite
*  add_action( 'init', 'flush_rewrite_rules' );


## Login form

text after 
text before pridat check box show if loged in ...

nastavenie redirect podla uzivatela
+ moznost vybrat - redirect podla stranky kde je formular vrati na aktualnu stranku
+ widget
+ (wp admin show hide?)

https://wordpress.org/plugins/sidebar-login/

## Delete Post Link

~~~php
/**
*
*   Architect Delete Post after 
*
**/

global $post;

add_action('arc_after_panel_close' ,'xtw_delete_link' );

function xtw_delete_link(){
    if ( !current_user_can( 'edit_post', $post->ID ) )
            return;

    if( is_user_logged_in() ){
        echo '<a href="'.get_delete_post_link( ).'">delete</a>';
    }
}

~~~

## Sidebar Fixed

vysuvaci sidebar alebo akykolvek wrapper (napr pre nejake skryte menu login etc)

~~~js
jQuery( document ).ready( function( $ ) { 

    $(".show-hide-sidebar").click(function(e) {
        $(".wrapper-mirroring-wd855dc62a99782a").toggleClass("toggle-sidebar");
        e.preventDefault(); // Cancel the default action (navigation) of the click.
    });
    
    $(".remove").click(function(e) {
        $(".wrapper-mirroring-wd855dc62a99782a").removeClass("toggle-sidebar");
        e.preventDefault(); // Cancel the default action (navigation) of the click.
    });
    
// stop scrolling page whlile scroll div ....
$( ".wrapper-mirroring-wd855dc62a99782a" ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
    var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    e.preventDefault(); // Cancel the default action (navigation) of the click.
});
~~~


## Redux custom page to admin menu

- moznost cez redux vybrat existujucu stranku alebo nejaky post ... a pridat do admin menu / alebo admin bar menu idealne


## DISABLE CORE BLOG / vypnutie klasickych clankov
+ pouzvivam CPT blog

~~~php
<?php
add_action( 'admin_menu', 'remove_admin_menus' );
add_action( 'wp_before_admin_bar_render', 'remove_toolbar_menus' );
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

function remove_admin_menus() {
    remove_menu_page( 'edit.php' );
}

function remove_toolbar_menus() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'new-post' );
}
?>
~~~

flush rewrite rule

## Big changes

kompletne zmenit strukturu  
** nieco ako pagelines a ultimate tweaks **

/modul/modul-name.php
/modul/modul-name.less
/modul/modul-name.js

modul-name.php 

+ redux options
+ funkcia _do() vykona vystup modulu
+ less prida options z reduxu  
  + dolezite prave preto ze ak fyzicky 
  odstranim modul nebude samotny less kompilovat premenne)  
  otazka je ci dokazem separatne ssubory less skompilovat do jedneho css
  + klasickym sposobom to asi nepojde
  + treba preskumat moznost - ze samotny base.less generujem dynamicky v PHP
  a v php sa podla nacitanych modulov rozhodnem ze donho zakompilujem aj less 
  z nacitaneho modulu
  

## Wrapper ID
# HW wrapper ID - do select boxu

zobrazenie id headway wrapperu
headway-hwr-toolkit/admin/classes/block-search.php

## Admin bar prisposobit

+ v admin bare skryt
+ presunut pod redux

## UI/UX Component Framework

[Semantic UI Git](https://github.com/Semantic-Org/Semantic-UI)
[Compare frameworks](http://usablica.github.io/front-end-frameworks/compare.html)

Nachystat / samozrejme pridat do text editoru
lists
	simple
	cislovany
	...

## bug after whitewrap / includes/headway.php

~~~css
.wrapper:after {
    content: "\0000A0";
    font-size: 1px;
}

.block{
    overflow: visible !important;
}
~~~

plugin - Enable Media Replace 3.0.3

## theme HW content bug image link / post link

+ problem s less / vc / all import / woocommerce/ 
+ scroll top - velkost ikony! / zarovnanie na stred - line height

### https://wordpress.org/plugins/image-cleanup/

# ak sa nudis

hw options

+ wp pagenavi
+ woocommerce
+ icons

zisti preco robi problem border box


# HW - pridat moznost globalne vypnut / zapnut [edit post]
cize spravit vlastnu funkciu na vkladanie tohto linku a osefovat tak aby sa dala vypnut a zapnut cez redux
a skrit v HW editore

Pripravene option v admin section
opt-content-edit-button
staci dat podmienku v hw moduloch s

### ASAP

+ vsetky funkcie osetrit 
 
~~~php
<?php
if(!function_exist(xxxx)){
	function xxxx(){
	
	}
}
?>
~~~

# DEV sources

[iconmoon.io](https://icomoon.io/app/)

# Display a Single Post via admin-ajax.php

~~~php
<?php
/**
 * Show a single post via admin-ajax.php
 *
 * Use a url like this to call this function:
 * /wp-admin/admin-ajax.php?action=mfields_show_post&p=514
 *
 * @since     2010-12-04
 */
 
function mfields_ajax_show_post() {
    $id = ( isset( $_GET['p'] ) ) ? (int) $_GET['p'] : false;
    query_posts( array( 'p' => $id ) );
    if ( have_posts() ) {
        while( have_posts() ) {
            the_post();
            the_title();
            the_content();
            comments_template( '', true );
        }
    }
    else {
        print '<p>No posts</p>';
    }
    exit;
}
add_action( 'wp_ajax_mfields_show_post', 'mfields_ajax_show_post' );
add_action( 'wp_ajax_nopriv_mfields_show_post', 'mfields_ajax_show_post' );
?>
~~~

# Makaj na tomto

+ customize dashboard / remove widgets
+ logout time
+ Change posts autosave interval
+ Posts revisions control
+ Disable posts autosave
+ Change Empty Trash days period
+ Enable excerpt for Pages
+ Change excerpt length
+ Remove WordPress Version
+ Remove RSD Link
+ Remove Feed links
+ Remove WLW Manifest Link
+ Remove extra Feed links
+ Disable login form Shake
+ Disable WordPress update notice
+ Authentication cookie expiration
+ Redirect single result search
+ Shortcodes in widgets
+ Change default editor type
+ Disable self pings
+ Disable comments auto links
+ Disable dashboard Welcome widget
+ Disable dashboard widgets
+ Hide WP version from admin footer
+ Disable default widgets
+ Disable all RSS feeds
+ Disable XML-RPC
+ Admin Bar front end visibility
+ Change JPEG compression quality
+ Remove gallery shortcode inline style If you u 
+ Replace image caption code
+ Disable frontend Adminbar items
+ Disable admin Adminbar items

http://pressing-matters.io/the-definitive-guide-to-adding-javascript-css-to-wordpress/

#Heartbeat Control plugin

optimize
http://www.inmotionhosting.com/support/website/wordpress/how-to-optimize-wordpress#disable-wp-cron

# Base 64 icon fonts arrow

+ dat ako include LESS, takze ho bude moct pouzivat cela tema nezavysle, zaroven osetrit ze ak > 

# Admin post list icon (action)

pridat do admin in style a osefovat miesto dlhych  textov  ikonky

edit
hw-visual-editor
clone
edit
view
inline

# Ajax

[sticky star](https://wordpress.org/plugins/sm-sticky-clicky-star/)

# Symlink 
[symlink](https://github.com/liggitt/wordpress-plugin-symlink/blob/master/plugin-symlink.php)
[a totok](http://pressing-matters.io/fixing-plugins-that-break-on-symlinked-wp-content/)

# Tutorial: Writing a simple WordPress plugin from scratch

[toto] (https://catn.com/2014/10/06/tutorial-writing-a-simple-wordpress-plugin-from-scratch/)

# Owl Carousel
[carousel](http://www.owlcarousel.owlgraphic.com/demos/responsive.html)

# Color Posts + Nested Pages
~~~css
	.nestedpages .published .row {
		height: auto;
		background-color: #e7f3e2;
	}

	.nestedpages .draft .row {
		height: auto;
		background-color: #f3ef9b;
	}
~~~

### ADD - > Fav icons [Cheat Sheet] (https://github.com/audreyr/favicon-cheat-sheet#id10)
[template] (https://github.com/jonrandahl/H5BP-Multi-Layer-FavIcons/)

#### ADD -> Post Status Menu Items --- plugin

## Debug

Control verbosity level of WP DEBUG? ylluminate
I've been at a loss so far and so I thought I'd pose the question: Is there a way to modify the verbosity level of the WP debug.log via wp-config.php or elsewhere?
Just an fyi, here is what I have in my wp-config.php to enable logging:

~~~php
<?php
// DEBUG

 // Enable WP_DEBUG mode
define('WP_DEBUG', true);

// Enable Debug logging to the /wp-content/debug.log file
define('WP_DEBUG_LOG', true);

// Disable display of errors and warnings 
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors',0);

// Use dev versions of core JS and CSS files 
// (only needed if you are modifying these core files)
define('SCRIPT_DEBUG', true);

// END DEBUG
?>
~~~

When WP_DEBUG is set, WordPress sets (via wp_debug_mode() call early in core load process) the error reporting level to E_ALL & ~E_DEPRECATED & ~E_STRICT. This means all warnings and errors except strict errors and PHP deprecated functions (not WordPress ones).

You can define your own level in a custom mu-plugin (the override needs to be called as soon as possible, but after WordPress core load). Use this page for information about the error levels you can use. An example:
error_reporting(E_ERROR | E_WARNING | E_PARSE);

# LESS

http://www.sitepoint.com/a-comprehensive-introduction-to-less-mixins/

## WooCommerce 
	
+ vypinanie / zapinanie funkcii
+ woocommerce-poor-guys-swiss-knife

## Pridat zoznam alebo automaticku instalaciu najpouzivanejsich pluginov - swiss theme to ma

[TGM Plugin Activation](http://tgmpluginactivation.com/#screenshots)



###[Best collection of code for your functions](http://http://wordpress.stackexchange.com/questions/1567/best-collection-of-code-for-your-functions-php-file)


# Pre-populating post types

Here is one for this collection.

This auto populates post types and posts.

~~~php
	add_filter( 'default_content', 'my_editor_content' );
	
	function my_editor_content( $content ) {
	
	    global $post_type;
	
	    switch( $post_type ) {
	        case 'your_post_type_here': //auto populate
	            $content = 'The content you want to pre-populate the post type with.';
	break;
	     }
	
	    return $content;
	}
~~~

This can come in handy for when you need to post the same info over and over again with slight differences.

# List all SubCategories

~~~php
	$echo = '<ul>' . "\n";
	$childcats = get_categories('child_of=' . $cat . '&hide_empty=1');
	foreach ($childcats as $childcat) {
	    if (1 == $childcat->category_parent) {
	        $echo .= "\t" . '<li><a href="' . get_category_link($childcat->cat_ID).'" title="' . $childcat->category_description . '">';
	        $echo .= $childcat->cat_name . '</a>';
	        $echo .= '</li>' . "\n";
	    }
	}
	$echo .= '</ul>' . "\n";
	echo $echo;
~~~	

also here, more informations and functins in the post http://wpengineer.com/2025/list-all-subcategories/

# Get Attributes of Given Thumbnail

~~~php
	/**
	* GET THUMBNAIL ATTRIBUTES
	*
	* Fetches width, heigth and URI of a thumbnail.
	*
	* @author Philip Downer <philip@manifestbozeman.com>
	* @license http://opensource.org/licenses/gpl-license.php GNU Public License
	* @version v1.0
	*
	* @param string $return Accepts 'path', 'width', or 'height'.
	* @param string $size The thumbnail size corresponding to {@link add_image_size() WP core function}.
	* @return mixed Returns the requested info, or if no 'Featured Image' assigned, returns 'false'.
	*/
	
	function get_thumb_attr($return,$size='thumbnail') {
	    global $post;
	
	    if (has_post_thumbnail($post->ID)) {
	      $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'intro');
	      if ( $return == 'path' ) { return $thumb[0]; }
	      if ( $return == 'width' ) { return $thumb[1]; }
	      if ( $return == 'height' ) { return $thumb[2]; }
	    } else {
	        return false;
	    }
	}//end function
~~~

# Adds a custom dropdown option to WP_NAV_MENUS where the user can select a predefined css class for each menu item

~~~php
<?php
function menu_item_class_select(){
    global $pagenow;
    if ($pagenow == "nav-menus.php"){
    ?>
    <script>
    jQuery(document).ready(function(){
        function create_dd(v){
            //create dropdown
            var dd = jQuery('<select class="my_class"></select>');
            //create dropdown options
            //array with the options you want
            var classes = ["","class1","class2","class3"];
            jQuery.each(classes, function(i,val) {
                if (v == val){
                    dd.append('<option value="'+val+'" selected="selected">'+val+'</option>');
                }else{
                    dd.append('<option value="'+val+'">'+val+'</option>');
                }
            });
            return dd;
        }

        jQuery(".edit-menu-item-classes").each(function() {
            //add dropdown
            var t = create_dd(jQuery(this).val());
            jQuery(this).before(t);
            //hide all inputs
            jQuery(this).css("display","none");

        });
        //update input on selection
        jQuery(".my_class").bind("change", function() {
            var v = jQuery(this).val();
            var inp = jQuery(this).next();
            inp.attr("value",v);
        });
    });


    </script>
    <?php
    }
}
add_action('admin_footer','menu_item_class_select');
?>
~~~

http://wordpress.stackexchange.com/a/33816/479

# Remove WordPress 3.3 Admin Bar Menu Items

~~~php
<?php
	function dashboard_tweaks() {
	    global $wp_admin_bar;
	    $wp_admin_bar->remove_menu('wp-logo');
	    $wp_admin_bar->remove_menu('about');
	    $wp_admin_bar->remove_menu('wporg');
	    $wp_admin_bar->remove_menu('documentation');
	    $wp_admin_bar->remove_menu('support-forums');
	    $wp_admin_bar->remove_menu('feedback');
	    $wp_admin_bar->remove_menu('view-site');
	    $wp_admin_bar->remove_menu( 'comments' ); // Remove "Comments" link
	}
	add_action( 'wp_before_admin_bar_render', 'dashboard_tweaks' );
?>
~~~

# Automatically create a new page upon activating
@ theme - pozri WOOcommerce

~~~php
<?php
	if (isset($_GET['activated']) && is_admin()){
		    $new_page_title = 'This is the page title';
		    $new_page_content = 'This is the page content';
		    $new_page_template = ''; //ex. template-custom.php. Leave blank if you don't want a custom page template.
		
		//don't edit under this line
		$page_check = get_page_by_title($new_page_title);
		$new_page = array(
		    'post_type' => 'page',
		    'post_title' => $new_page_title,
		    'post_content' => $new_page_content,
		    'post_status' => 'publish',
		    'post_author' => 1,
		);
		if(!isset($page_check->ID)){
		    $new_page_id = wp_insert_post($new_page);
		    if(!empty($new_page_template)){
		        update_post_meta($new_page_id,'_wp_page_template', $new_page_template);
		    }
		}
?>
~~~
 
# Resize on upload to the largest size in media setting

~~~php
<?php
	function replace_uploaded_image($image_data) {
	
	// if there is no large image : return
	if (!isset($image_data['sizes']['large'])) return $image_data;
	
	// path to the uploaded image and the large image
		$upload_dir = wp_upload_dir();
		$uploaded_image_location = $upload_dir['basedir'] . '/' .$image_data['file'];
		$large_image_location = $upload_dir['path'] . '/'.$image_data['sizes']['large']['file'];
	
	// delete the uploaded image
		unlink($uploaded_image_location);
	
	// rename the large image
		rename($large_image_location,$uploaded_image_location);
	
	// update image metadata and return them
		$image_data['width'] = $image_data['sizes']['large']['width'];
		$image_data['height'] = $image_data['sizes']['large']['height'];
		unset($image_data['sizes']['large']);
	
		return $image_data;
	}
	add_filter('wp_generate_attachment_metadata','replace_uploaded_image');
?>
~~~

# List all constants for information and debugging

Tested on: Wordpress 3.0.1

Will only display the information if you are a logged in user

~~~php
<?php
	if ( is_user_logged_in() ) {
	    print('<pre>');
	    print_r( @get_defined_constants() );
	    print('</pre>');
	}
?>
~~~

## Here is version with optional filter that will partially match constant names and values:

~~~php
<?php
	function constants($filter = false) {

        $constants = get_defined_constants();

        if( $filter ) {

            $temp = array();

            foreach ( $constants as $key => $constant )
                if( false !== stripos( $key, $filter ) || false !== stripos( $constant, $filter ) )
                    $temp[$key] = $constant;

            $constants = $temp;
        }

        ksort( $constants );
        var_dump( $constants );
    }
?>
~~~

# Easy WordPress Security Fixes

## Remove version string from header

~~~php
remove_action('wp_head', 'wp_generator');
~~~

## Hide login error messages (Wrong Password, No Such User etc.)

~~~php
add_filter('login_errors',create_function('$a', "return null;"));
~~~

## Remove admin name in comments class

[Source](http://www.wprecipes.com/wordpress-hack-remove-admin-name-in-comments-class)

~~~php

function remove_comment_author_class( $classes ) {
    foreach( $classes as $key => $class ) {
        if(strstr($class, "comment-author-")) {
            unset( $classes[$key] );
        }
    }
    return $classes;
}
add_filter( 'comment_class' , 'remove_comment_author_class' );
~~~

# Get a custom field value through shortcodes

~~~php
	add_shortcode('field', 'shortcode_field');
	
	function shortcode_field($atts){
	  extract(shortcode_atts(array(
	   'post_id' => NULL,
	  ), $atts));
	
	  if(!isset($atts[0])) return;
	  $field = esc_attr($atts[0]);
	
	  global $post;
	  $post_id = (NULL === $post_id) ? $post->ID : $post_id;
	
	  return get_post_meta($post_id, $field, true);
	}
~~~
Usage:

>[field "my_key"]
>
>[field "my_key" post_id=1]

# Remove XML-RPC when not in use for performance boost

### Prevents WordPress from testing ssl capability on domain.com/xmlrpc.php?rsd
	
~~~php
	remove_filter('atom_service_url','atom_service_url_filter');
~~~

# Custom Favicon WP-Admin

~~~php
	function admin_favicon() {
	 echo '<link rel="shortcut icon" type="image/x-icon" href="' . get_bloginfo('template_directory') . '/images/favicon.ico" />';
	}
	add_action( 'admin_head', 'admin_favicon' );
~~~

# Extending Auto Logout Period


~~~php
	function keep_me_logged_in_for_1_year( $expirein ) {
	   return 31556926; // 1 year in seconds
	}
	add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_year' );
~~~

# Remove superfluous info and HTML within the <head> tag

~~~php
#remove unnecessary header info
	function remove_header_info() {
	    remove_action('wp_head', 'rsd_link');
	    remove_action('wp_head', 'wlwmanifest_link');
	    remove_action('wp_head', 'wp_generator');
	    remove_action('wp_head', 'start_post_rel_link');
	    remove_action('wp_head', 'index_rel_link');
	    remove_action('wp_head', 'adjacent_posts_rel_link');         // for WordPress <  3.0
	    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head'); // for WordPress >= 3.0
	}
	add_action('init', 'remove_header_info');
~~~

# Remove extra CSS that 'Recent Comments' widget injects

~~~php
	function remove_recent_comments_style() {
	    global $wp_widget_factory;
	    remove_action('wp_head', array(
	        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
	        'recent_comments_style'
	    ));
	}
	add_action('widgets_init', 'remove_recent_comments_style');
~~~
# Function to Disable RSS Feeds

~~~php
	function fb_disable_feed() {
	wp_die( __('No feed available, please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
	}
	
	add_action('do_feed', 'fb_disable_feed', 1);
	add_action('do_feed_rdf', 'fb_disable_feed', 1);
	add_action('do_feed_rss', 'fb_disable_feed', 1);
	add_action('do_feed_rss2', 'fb_disable_feed', 1);
	add_action('do_feed_atom', 'fb_disable_feed', 1);
~~~


# Enable shortcodes in widgets

~~~php
	if ( !is_admin() ){
	    add_filter('widget_text', 'do_shortcode', 11);
	}
~~~

# Remove unwanted dashboard items
~~~php
	add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
	
	function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	
	//Right Now - Comments, Posts, Pages at a glance
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	
	//Recent Comments
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	
	//Incoming Links
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	
	//Plugins - Popular, New and Recently updated Wordpress Plugins
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	
	//Wordpress Development Blog Feed
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	
	//Other Wordpress News Feed
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	
	//Quick Press Form
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	
	//Recent Drafts List
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	}
~~~

# Display DB Queries, Time Spent and Memory Consumption

~~~php
	function performance( $visible = false ) {
	
	    $stat = sprintf(  '%d queries in %.3f seconds, using %.2fMB memory',
	        get_num_queries(),
	        timer_stop( 0, 3 ),
	        memory_get_peak_usage() / 1024 / 1024
	        );
	
	    echo $visible ? $stat : "<!-- {$stat} -->" ;
	}
	Then this code below the code above which will automatically insert the code above into the footer of your public website (make sure your theme is calling wp_footer):
	
	add_action( 'wp_footer', 'performance', 20 );

~~~


# Remove meta boxes from default posts screen

~~~php
function remove_default_post_screen_metaboxes() {
	 remove_meta_box( 'postcustom','post','normal' ); // Custom Fields Metabox
	 remove_meta_box( 'postexcerpt','post','normal' ); // Excerpt Metabox
	 remove_meta_box( 'commentstatusdiv','post','normal' ); // Comments Metabox
	 remove_meta_box( 'trackbacksdiv','post','normal' ); // Talkback Metabox
	 remove_meta_box( 'slugdiv','post','normal' ); // Slug Metabox
	 remove_meta_box( 'authordiv','post','normal' ); // Author Metabox
}
add_action('admin_menu','remove_default_post_screen_metaboxes');
~~~

# Modify the Login Logo & Image URL Link

~~~php
	add_filter( 'login_headerurl', 'namespace_login_headerurl' );
	/**
	 * Replaces the login header logo URL
	 *
	 * @param $url
	 */
	function namespace_login_headerurl( $url ) {
	    $url = home_url( '/' );
	    return $url;
	}
	
	add_filter( 'login_headertitle', 'namespace_login_headertitle' );
	/**
	 * Replaces the login header logo title
	 *
	 * @param $title
	 */
	function namespace_login_headertitle( $title ) {
	    $title = get_bloginfo( 'name' );
	    return $title;
	}
	
	add_action( 'login_head', 'namespace_login_style' );
	/**
	 * Replaces the login header logo
	 */
	function namespace_login_style() {
	    echo '<style>.login h1 a { background-image: url( ' . get_template_directory_uri() . '/images/logo.png ) !important; }</style>';
	}
~~~

# Disable Posts Auto Saving

~~~php
	function disableAutoSave(){  
	wp_deregister_script('autosave');  
	}  

	add_action( 'wp_print_scripts', 'disableAutoSave' );
~~~

# Change Excerpt Length Depending Of The Category

~~~php
	add_filter('excerpt_length', 'my_excerpt_length');  
	function my_excerpt_length($length) {  
	    if(in_category(14)) {  
	        return 13;  
	    } else {  
	    return 60;  
	    }  
	} 
~~~

# Adding A Custom Field Automatically On Post/Page Publish

A code snippet for installing a custom field automatically to a page or post when they are published. You can just add the code below into your functions.php file, located inside your theme’s folder. Of course, don’t forget to change the custom field name.

~~~php
	add_action('publish_page', 'add_custom_field_automatically');  
	add_action('publish_post', 'add_custom_field_automatically');  
	  
	function add_custom_field_automatically($post_ID) {  
	    global $wpdb;  
	    if(!wp_is_post_revision($post_ID)) {  
	        add_post_meta($post_ID, 'field-name', 'custom value', true);  
	    }  
	} 
~~~

#Redirect To Post When Search Query Returns Single Result

~~~php
	add_action('template_redirect', 'single_result');  
	function single_result() {  
	    if (is_search()) {  
	        global $wp_query;  
	        if ($wp_query->post_count == 1) {  
	            wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );  
	        }  
	    }  
	} 
~~~


# Set Default Editor

## This sets the Visual Editor as default  

~~~php
	add_filter( 'wp_default_editor', create_function('', 'return "tinymce";') );  
~~~	  

## This sets the HTML Editor as default  

~~~php
	add_filter( 'wp_default_editor', create_function('', 'return "html";') );  
~~~

# Remove unnecessary meta-data from your WordPress site

~~~php
	remove_action( 'wp_head', 'wp_generator' ) ; 
	remove_action( 'wp_head', 'wlwmanifest_link' ) ; 
	remove_action( 'wp_head', 'rsd_link' ) ;
~~~


# Disable HTML in WordPress comments

~~~php

	add_filter( 'pre_comment_content', 'wp_specialchars' );

~~~

# Hide Errors on the WordPress Login screen

~~~php

	function no_errors_please(){
	  return 'GET OFF MY LAWN !! RIGHT NOW !!';
	}
	add_filter( 'login_errors', 'no_errors_please' );

~~~


# Change posts autosave interval


~~~php

	define( 'AUTOSAVE_INTERVAL', 120 );

~~~


# Add the missing favicon and touch icons
Put a 16×16 favicon.ico and a 144×144 apple-touch.png file in the home directory of your blog. Then add this line to your .htaccess to redirect all apple touch icon requests to that particular file.
.htaccess

RedirectMatch 301 /apple-touch-icon(.*)?.png http://example.com/apple-touch.png


# Log 404 Errors in Google Analytics

404 errors are a missed opportunity. You can use events in Google Analytics to log your 404 errors including details about the referring site that is pointing to that 404 page of your site.

	Add this block inside your Google Analytics tracking code after the _gaq.push function.

~~~php

<? if (is_404()) { ?>
 _gaq.push(['_trackEvent', '404', document.location.pathname + document.location.search, document.referrer, 0, true]);
<? }

~~~

# plugin - Limit Login Attempts


# Set Different Editor Stylesheets For Different Post Types


~~~php

	function my_editor_style() {  
		global $current_screen;  
       		switch ($current_screen->post_type) {  
       
        	case 'post':  
        	add_editor_style('editor-style-post.css');  
        	break;  
        
        	case 'page':  
        	add_editor_style('editor-style-page.css');  
        	break;
        
        case 'portfolio':  
        add_editor_style('editor-style-portfolio.css');  
        break;  
    }  
}  
add_action( 'admin_head', 'my_editor_style' );  
~~~

# Enable TinyMCE Editor For Post The_excerpt

~~~php
<?php
	function tinymce_excerpt_js(){ ?>  
	<script type="text/javascript">  
	    jQuery(document).ready( tinymce_excerpt );  
	    function tinymce_excerpt() {  
	    jQuery("#excerpt").addClass("mceEditor");  
	    tinyMCE.execCommand("mceAddControl", false, "excerpt");  
	    }  
	</script>  
	<?php }  
	add_action( 'admin_head-post.php', 'tinymce_excerpt_js');  
	add_action( 'admin_head-post-new.php', 'tinymce_excerpt_js');  
	  
	function tinymce_css(){ ?>  
	<style type='text/css'>  
	    #postexcerpt .inside{margin:0;padding:0;background:#fff;}  
	    #postexcerpt .inside p{padding:0px 0px 5px 10px;}  
	    #postexcerpt #excerpteditorcontainer { border-style: solid; padding: 0; }  
	</style>  
	<?php }  
	add_action( 'admin_head-post.php', 'tinymce_css');  
	add_action( 'admin_head-post-new.php', 'tinymce_css');
	?>
~~~

# Display Post Thumbnail Also In Edit Post And Page Overview
	
~~~php
	if ( !function_exists('fb_AddThumbColumn') && function_exists('add_theme_support') ) {  
	// for post and page  
	add_theme_support('post-thumbnails', array( 'post', 'page' ) );  
	function fb_AddThumbColumn($cols) {  
	    $cols['thumbnail'] = __('Thumbnail');  
	    return $cols;  
	}  
	  
	function fb_AddThumbValue($column_name, $post_id) {  
	    $width = (int) 35;  
	    $height = (int) 35;  
	    if ( 'thumbnail' == $column_name ) {  
	        // thumbnail of WP 2.9  
	        $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );  
	          
	        // image from gallery  
	        $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );  
	          
	        if ($thumbnail_id)  
	            $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );  
	        elseif ($attachments) {  
	            foreach ( $attachments as $attachment_id => $attachment ) {  
	            $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );  
	        }  
	    }  
	    if ( isset($thumb) && $thumb ) { echo $thumb; }  
	    else { echo __('None'); }  
	    }  
	}  
	  
	// for posts  
	add_filter( 'manage_posts_columns', 'fb_AddThumbColumn' );  
	add_action( 'manage_posts_custom_column', 'fb_AddThumbValue', 10, 2 );  
	  
	// for pages  
	add_filter( 'manage_pages_columns', 'fb_AddThumbColumn' );  
	add_action( 'manage_pages_custom_column', 'fb_AddThumbValue', 10, 2 );  
}  
~~~

 
## List of Available Tweaks

PANEL: TWEAKS

+ Disable TinyMCE4 autoresize (v3.8)
+ Redirect search with one result (v1.5)
+ Change default editor type (v1.5)
+ Disable capital P filters (v1.4)
+ Remove website field from comment form (v1.3)
+ Refuse comments with no referrer (v1.3)
+ Disable XML-RPC
+ Enable shortcodes in widgets
+ Prevent self pingbacks
+ Disable all RSS feeds
+ Disable comments auto links
+ Disable default widgets
+ PANEL: ADMINBAR
 
+ Disable selected frontend adminbar items (v2.0)
+ Disable selected admin adminbar items (v2.0)
+ Control Admin Bar front end visibility (v1.6)
+ PANEL: ADMIN

+ Bring back Dashboard columns selection (v3.6)
+ Add order column for Post Types panels (v3.5)
+ Add extra columns for the Users panel (v3.0)
+ Add post thumbnail column for Post Types panels (v3.0)
+ Add comment ID column for the Comments panel (v3.0)
+ Add post ID column for Post Types panels (v3.0)
+ Add term ID column for Taxonomies panels (v3.0)
+ Disable welcome dashboard widget (v1.2)
+ Disable browser upgrade nag (v1.1)
+ Disable file editing
+ Disable dashboard widgets
+ Hide WP version in admin footer
+ PANEL: REWRITER
 
+ Simple posts short URL (v3.5)
+ Change author archive URL slug (v2.0)
+ Change search results URL slug (v2.0)
+ PANEL: MEDIA
 
+ Remove default gallery shortcode inline styles (v2.0)
+ Replace default image caption code (v1.4)
+ Change JPEG compression quality
+ PANEL: POSTS
 
+ Allow use of drafts for parents (v3.0)
+ Enable excerpt for Pages (v2.0)
+ Disable auto draft deletion (v1.2)
+ Change Excerpt length (v1.1)
+ Disable posts auto save
+ Disable auto trash deletion
+ Posts revisions control
+ Change posts auto save interval
+ Change empty trash days
+ PANEL: HEADER

+ Remove Open Sans font loading (v3.6)
+ Prevent S kype from marking phones (v3.5)
+ Remove canonical link (v1.2)
+ Remove parent/start links
+ Remove WordPress version
+ Remove RSD link
+ Remove WLW manifest link
+ Remove feed links
+ Remove extra feed links
+ Remove l10n JavaScript
+ Remove adjacent posts links
+ Remove home link
+ PANEL: PLUGINS
 
+ Hide WP-Stats smiley
+ PANEL: HTTP
 
+ HTTP Request timeout (v3.9)
+ HTTP Request redirections count (v3.9)
+ HTTP Request reject unsafe URL’s (v3.9)
+ HTTP Request user agent (v3.9)
+ HTTP Request version (v3.9)
+ PANEL: UPDATES

+ Control updates notifications emails (v3.2)
+ Change email for update notifications (v3.2)
+ Disable automatic updates (v3.1)
+ Control WordPress automatic updates (v3.1)
+ WordPress core auto update control (v3.1)
+ Disable WordPress update notice (v1.2)
+ PANEL: GLOBAL
 
+ Remove color scheme changer (v3.5)
+ Default color scheme for new users (v3.5)
+ Change default theme (v3.0)
+ Allow login with email address (v2.0)
+ Disable doing-it-wrong display (v2.0)
+ Disable login form Shake (v1.6)
+ Authentication cookie expiration
+ Disable user profile contact methods
+ Add user profile contact methods


# Posts revisions control

~~~php
	define( 'WP_POST_REVISIONS', 3);
	
	if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 5);
~~~

# Remove pings to your own blog

~~~php
	function no_self_ping( &$links ) {
	    $home = get_option( 'home' );
	    foreach ( $links as $l => $link )
	        if ( 0 === strpos( $link, $home ) )
	            unset($links[$l]);
	}
	add_action( 'pre_ping', 'no_self_ping' );
~~~
